# General description

This repository contains source code files of my homebrew computer codenamed *Cobalt-68*. 
This project was started in 2017 as nostalgic attempt to create my own computer system loosely styled after good-old PCs of 1980s era.
It is built around two main components: MC68008 - 8-bit bus version of MC68000 CPU and Atmega640 - MCU which plays role of main system perepherial controller. 

As for now computer is pretty much fully functional: it contains CPU, RAM, ROM, storage device, display controller and PS/2 keyboard, UART. In terms of software there is still much to do, but there is basic operating system and applications.

# Project goals
This project is aimed at creation of fully autonomus computer system, which is capable of operation and and further development without support from Big Computer (my home PC).

Met criterias:
  - Has mass storage device.
  - Has physical console for user IO (video display, keyboard)
  - Has operating system and basic applications.

Criterias to meet:
  - Has onboard application software development tools
  - Has onboard system software tools (C compiler and associated softare)

# Further goals:
- System optimization using custom-build perepherials devices (TTL or CLPD).
- Adding perepherials (Floppy, IDE hard drive, etc).
- Improving software ecosystem.
- New iteration of the project based on MC68030.

# Brief description of system:
* CPU: Motorola MC68008 (48 pin DIP package), 8 MHz clock speed.
* RAM: 512 KB static RAM
* ROM: 32 KB EEPROM 
* PS/2 Keyboard suppport
* UART
* Storage: 2xSD slots.
* 4x Expansion bus slots. 
    * Monochrome 80x25 characters Video Display Controller (VDC)

* Software:
    * ROM-based machine code monitor.
    * CP/M-like operation system.
    * Basic OS commands (ls, mkdir, etc.)
    * Text editor
    * Lisp interpreter

# Toolchain
This project makes use of:
* Prebuilt GCC Toolchain for 68K for target system software development [link](https://gnutoolchains.com/m68k-elf/).
* Atmel Studio for MCU programming [link](https://www.microchip.com/en-us/development-tools-tools-and-software/microchip-studio-for-avr-and-sam-devices).
* WinCUPL for PLD programming [link](https://www.microchip.com/en-us/products/fpgas-and-plds/spld-cplds/pld-design-resources)
* PuTTY for terminal communication.

Development is Windows-based, so scripts and path's may not be ready to unix-based development environment.

# Third-party source code
To save time and effort I've used source code from several other projects. Many thanks to their respective authors!
* [ulibSD](https://github.com/1nv1/ulibSD) - SD card library by Nelson 1nv1.
* [FatFs - Generic FAT Filesystem Module](http://elm-chan.org/fsw/ff/00index_e.html) by ELM Chan.
* [Public Domain C Library (PDCLib)](https://github.com/DevSolar/pdclib).
* [Kilo text editor](https://github.com/antirez/kilo) by Salvatore Sanfilippo.
* [MiniLisp](https://github.com/rui314/minilisp) by Rui Ueyama.
* [PicoC interpreter](https://gitlab.com/zsaleeba/picoc) by Zik Saleeba.
