

#include "plib.h"

#define BYTE_OFFSET(value, offset) *(((char*)&value) + offset)

unsigned int sd_timeout = 0xFF;

void sd_waitNotBusy()
{
	while(spi_sendByte(0xFF) != 0xFF);
}

sd_result sd_sendCommand(unsigned char number, unsigned long address, char crc)
{
	sd_waitNotBusy();
	
	for(int i = 0; i < 16; i++) spi_sendByte(0xFF);	 // Dummy bytes.
	
	spi_sendByte(number | 0x40);
	spi_sendByte(BYTE_OFFSET(address, 0));
	spi_sendByte(BYTE_OFFSET(address, 1));
	spi_sendByte(BYTE_OFFSET(address, 2));
	spi_sendByte(BYTE_OFFSET(address, 3));
	spi_sendByte(crc);
	
	return sd_waitByte();
}

sd_result sd_waitByte()
{
	unsigned int counter = sd_timeout;
loop:;
	unsigned char c; 
	
	if(counter-- == 0) 
	{
		return (sd_result){SD_TIMEOUT, 0, 0};
	}
	
	c = spi_sendByte(0xff);
	
	if(c & 0x80) goto loop;
	
	return (sd_result){SD_NOERROR, c, 0};
}

sd_result sd_getStatus()
{
	sd_result result;
	
	result = sd_sendCommand(13, 0, 0);
	
	if(result.code != SD_NOERROR)
	{
		return result;	
	}
	
	result.r2 = spi_sendByte(0xFF);
	return result;
}

void sd_spinup(char spiIndex)
{
	spi_unselectSlave();
	
	for(char i = 0; i < 10;i ++)
	{
		spi_sendByte(0xFF);
	}
	
	spi_selectSlave(spiIndex);
}	

unsigned int sd_writeBlock(char spiIndex, unsigned long address, void *pdata, unsigned int size)
{
	if(size > 512)
	{
		return 1;
	}
	
	unsigned char *data = pdata;
	
	spi_selectSlave(spiIndex);
	
	sd_result result = sd_sendCommand(24, address, 0xFF);
	
	if(result.code != SD_NOERROR || result.r1 != 0) 
	{
		spi_unselectSlave();
		return 1;
	}
	
	spi_sendByte(0xFF); // Dummy byte
	spi_sendByte(0xFE); // Data packet start marker
	
	// send bytes
	for (int i = 0; i < size; i++) spi_sendByte(*data++);
	
	// send padding
	for (int i = 0; i < (512 - size); i++) spi_sendByte(0);
	
	// Dummy CRC
	spi_sendByte(0);
	spi_sendByte(0);
	
	while ((spi_sendByte(0xFF) & 0x1F) != 0x05); // cmd ack waiting
    while (spi_sendByte(0xFF) != 0x00); // data write complete waiting
		
	spi_unselectSlave();
	return SD_NOERROR;
}

unsigned int sd_readBlock(char spiIndex, unsigned long address, void *pdata, unsigned int size)
{
	if(size > 512)
	{
		return 1;
	}

	unsigned char *data = pdata;
	
	spi_selectSlave(spiIndex);
	
	
	spi_selectSlave(spiIndex);
	
	sd_result result = sd_sendCommand(17, address, 0xFF);
	
	if(result.code != SD_NOERROR || result.r1 != 0) 
	{
		spi_unselectSlave();
		return 1;
	}

	// wait for data block
	// TODO : implement data error token handling
	while(spi_sendByte(0xFF) != 0xFE);
	
	// Read bytes
	for (int i = 0; i < size; i++) *data++ = spi_sendByte(0xFF);
	
	// Skip bytes
	for (int i = 0; i < (512 - size); i++) spi_sendByte(0xFF);
	
	// Dummy CRC read
	spi_sendByte(0xFF);
	spi_sendByte(0xFF);

	spi_unselectSlave();
	return SD_NOERROR;
}

// Result: 
// R0 == 0 - success
// R1 == 1 - CMD0 fail
// R1 == 2 - CMD1 fail
unsigned int sd_initialize(char spiIndex)
{
	sd_spinup(spiIndex);

	sd_result result = sd_sendCommand(0, 0, 0x95);
	if(result.code != SD_NOERROR || result.r1 != 1)
	{
		return 1;
	}		
	
	for(unsigned char i = 0; i < 100; i++)
	{
		result = sd_sendCommand(1, 0, 0xFF);
		
		if(result.code == SD_NOERROR && result.r1 == 0)
		{
			spi_unselectSlave();
			return 0;
		}
	} 
	
	spi_unselectSlave();
	return 2;
}