
#ifndef spi_c
#define spi_c

#define SPI_DATA *((volatile char*)0xFF805)
#define SPI_SS   *((volatile char*)0xFF806)

#include "plib.h"
 
char spi_trace = 0;

void spi_selectSlave(char v)
{
	SPI_SS = v;
	
	if(spi_trace)
	{
		kprint("SS "); kprintHex(v); kprint("\n");
	}
}

void spi_unselectSlave()
{
	SPI_SS = 0xff;
	
	if(spi_trace)
	{
		kprint("USS\n"); 
	}
}

unsigned char spi_sendByte(unsigned char v)
{
	if(spi_trace)
	{
		kprintHex(v); 
	}
	SPI_DATA = v;
	unsigned char c = SPI_DATA;
	
	if(spi_trace)
	{
		kprint(":"); 
		kprintHex(c); 
		kprint("   ");
	}
	return c;
}

#endif