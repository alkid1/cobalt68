

#include "plib.h"

#define UART_PORT (0xFF800)

void kputchar(char c)
{
	*((char*)UART_PORT) = c;
}

void kprint(const char *msg)
{
	while(*msg) kputchar(*(msg++));
}

void kprintHexNibble(char data)
{
	kputchar(digitsTable[data & 0xF]);
}

void kprintHex(char data)
{
	kprintHexNibble(data >> 4);
	kprintHexNibble(data);
}

void kprintHex16(unsigned int data)
{
	kprintHex(data >> 8);
	kprintHex(data & 0xFF);
}

void kprintHex32(unsigned long data)
{
	kprintHex16(data >> 16);
	kprintHex16(data & 0xFFFF);
}