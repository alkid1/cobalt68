#include "hlib.h"

unsigned char lookupDigit(char digit)
{
	unsigned char i;
	
	if(digit >= 'a') digit &= ~0x20;
	
	for(i = 0; i < 16; i++)
	{
		if(digitsTable[i] == digit) 
		{
			return i;
		}
	}
	
	return 16;
}
