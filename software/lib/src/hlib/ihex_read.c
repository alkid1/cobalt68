#include "hlib.h"

char isBreakDigit(char c)
{
	return c == 27 || c == 3; 
}

struct UIntParseResult readBytes(int length, char (*readChar)())
{
	unsigned long result = 0;
	
	while(length-- > 0)
	{
		char c = readChar();
		if(isBreakDigit(c)) return (struct UIntParseResult){0, RHL_ABORT};
		
		unsigned long value = lookupDigit(c);
		if(value == 16) return (struct UIntParseResult){0, RHL_BADFORMAT};
		
		result <<= 4;
		result |= value;
	}
	
	return (struct UIntParseResult){result, 1};
}

int readHexLine(void* pointer, int maxSize, char (*readChar)())
{
	struct UIntParseResult parseResult;
	unsigned char checksumCounter = 0;
	
	// 1. Wait for start-symbol ':'
	while(1)
	{
		char c = readChar();
		if(isBreakDigit(c)) return RHL_ABORT;
		if(c == ':') break;
	}
	
	// 2. Read Length 2-chars
	parseResult = readBytes(2, readChar);
	if(parseResult.success != 1) return parseResult.success;
	unsigned char length = parseResult.value;
	
	checksumCounter += length;
	

	// 3. Read Address 4-chars
	parseResult = readBytes(4, readChar);
	if(parseResult.success != 1) return parseResult.success;
	unsigned short address = parseResult.value;	
	
	checksumCounter += (address & 0xFF00) >> 8;
	checksumCounter += (address & 0xFF);


	// 4. Read Type 
	/*  00 запись содержит данные двоичного файла.
		01 запись является концом файла.
		02 запись адреса сегмента (подробнее см. ниже).
		03 Start Segment Address Record.
		04 запись расширенного адреса (подробнее см. ниже).
		05 Start Linear Address Record.*/
	parseResult = readBytes(2, readChar);
	if(parseResult.success != 1) return parseResult.success;
	unsigned char type = parseResult.value;
	
	checksumCounter += type;
	
	// 5. Read data, 
	unsigned short length2 = length;
	unsigned char  counter = 0;
	while(length2-- >0)
	{
		parseResult = readBytes(2, readChar);
		if(parseResult.success != 1) return parseResult.success;
		unsigned char data = parseResult.value;	
		
		// detect buffer overflow
		if((address + counter) >= maxSize) return RHL_OVERFLOW;
		
		// place data
		*((char*)(pointer + address + counter)) = data;
		
		checksumCounter += data;
		counter++;
	}
	 
	// 6. Read control sum, compare.
	parseResult = readBytes(2, readChar);
	if(parseResult.success != 1) return parseResult.success;
	unsigned char checksum = parseResult.value;	
	
	checksumCounter += checksum;
	
	if(checksumCounter != 0) return RHL_BADCHECKSUM;
	
	if(type == 01) return RHL_EOF;
	
	return length;
}