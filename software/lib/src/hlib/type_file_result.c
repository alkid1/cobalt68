#include "lib_fileio.h"
#include "lib_stdio.h"

#include "type_file_result.h"

void type_file_result(FILE_RESULT code)
{
	const char * message = 0;
	switch(code)
	{
		case FSYS_OK:
			message = "OK\n";
			break;
		case FSYS_BADINIT    :
			message = "Filesystem not initialized\n";
			break;
		case FSYS_NOTFOUND   :
			message = "File not found\n";
			break;
		case FSYS_NOFS       :
			message = "No filesystem present\n";
			break;
		case FSYS_IOERROR    :
			message = "IO error\n";
			break;
		case FSYS_FILELIMIT  :
			message = "File limit\n";
			break;
		case FSYS_TABLELIMIT :
			message = "Table limit\n";
			break;
		case FSYS_ARGUMENT   :
			message = "Invalid argument\n";
			break;
		case FSYS_FILEINUSE  :
			message = "File in use\n";
			break;
		case FSYS_FSYSLIMIT  :		
			message = "Filesystems limit\n";
			break;
		default:
			message = "Unspecified error\n";
			break;			
	}
	
	stdio_print(message);
}