#include "hlib.h"

struct UIntParseResult parseInt(const char *ptr)
{
	unsigned long result = 0;
	char digit;
	unsigned char value;
	
	while(1)
	{
		digit = *ptr;
		
		if((digit) == 0) break;
	
		value = lookupDigit(digit);
		
		if(value == 16)
		{
			return (struct UIntParseResult){0, 0};
		}
		
		result = result << 4;
		result |= value;

		ptr++;
	}

	return (struct UIntParseResult){result, 1};
}
