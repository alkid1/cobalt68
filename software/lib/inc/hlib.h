#pragma once

#include "defs.h"

/*
	Reads from console iHex line.
	
	Parameters:
		pointer - pointer receiving buffer
		maxSize - maximum amount of bytes buffer can hold. Fails if actual data is larger than that.
		
	Return value:
		zero or positive - number of bytes correctly received
*/
#define RHL_EOF         -1 // correct end of file received
#define RHL_BADFORMAT   -2 // format error/invalid character
#define RHL_BADCHECKSUM -3 // checksum error
#define RHL_ABORT       -4 // abort symbol encountered
#define RHL_OVERFLOW    -5 // buffer overflow

#ifndef NULL
	#define NULL 0
#endif

extern const char digitsTable [16];
int readHexLine(void* pointer, int maxSize, char (*readChar)());
unsigned char lookupDigit(char digit);

struct UIntParseResult
{
	unsigned long value;
	signed short  success;
};

struct UIntParseResult parseInt(const char *ptr);

char * strtok_r (char *s, const char *delim, char **save_ptr);