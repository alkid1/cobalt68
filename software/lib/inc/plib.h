#pragma once

#include "defs.h"
#include "hlib.h"

int atoi(char *str);

void* memcpy(void* destination, void* source, size_t num);
void* memset(void*  dest, char val, size_t len);

char strcmp(const char *p1, const char *p2);
char *strcpy(char *dest, const char *src);
unsigned long strcspn(const char *s1, const char *s2);
int strlen(const char *s);
char *strncat(char *dest, const char *src, size_t n);
char * strncpy(char *dst, const char *src, size_t n);
int strspn(const char *p, const char *s);
char * strncat  (char *dest, const char *src, size_t n);

void kputchar(char c);
void kprint(const char *msg);
void kprintHexNibble(char data);
void kprintHex(char data);
void kprintHex16(unsigned int data);
void kprintHex32(unsigned long data);

/* SD *******************************************************************************************************************/

#define SD_NOERROR  0   // SD Operation OK

#define SD_TIMEOUT  1   // SD Timeout error
#define SD_ARGUMENT 2   // SD Argument error (out of range)

typedef struct
{
	unsigned int  code;
	unsigned char r1;
	unsigned char r2;
} sd_result;

sd_result sd_sendCommand(unsigned char number, unsigned long address, char crc);
sd_result sd_waitByte();

void sd_waitNotBusy();
sd_result sd_sendCommand(unsigned char number, unsigned long address, char crc);
sd_result sd_waitByte();
sd_result sd_getStatus();


void sd_spinup(char spiIndex);
unsigned int sd_writeBlock(char spiIndex, unsigned long address, void *pdata, unsigned int size);
unsigned int sd_readBlock(char spiIndex, unsigned long address, void *pdata, unsigned int size);

// Result: 
// R0 == 0 - success
// R1 == 1 - CMD0 fail
// R1 == 2 - CMD1 fail
unsigned int sd_initialize(char spiIndex);

/* SPI ******************************************************************************************************************/

void spi_selectSlave(char v);
void spi_unselectSlave();
unsigned char spi_sendByte(unsigned char v);