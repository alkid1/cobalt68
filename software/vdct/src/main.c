#include "lib_progman.h"
#include "lib_stdio.h"

#include "stdio.h"

#define UART_PORT (0xFF800)
#define BOOT_PORT (UART_PORT + 1)
#define TEST_PORT (UART_PORT + 2)

#define UART_STATUS (UART_PORT + 15)

#define USR_IBF  1

#define PORT_PS2DR   (UART_PORT + 13)  // PS2 Data Register
#define PORT_PS2SR   (UART_PORT + 14)  // PS2 Status Register

#define PS2SR_HAS_DATA 1

#define USE_6845 1
#define USE_PS2  1
#define USE_UART 1

#define PORT(P) *((volatile char*)P)

#include "ps2.c"


char stdio_default_getchar()
{
#if USE_PS2 && USE_UART
	unsigned int result = 0;
	
	while(result == 0)
	{
		if(PORT(UART_STATUS) & USR_IBF)
		{
			for(volatile char cc = 0;cc < 4; cc++);
			return PORT(UART_PORT);
		}
		else if(PORT(PORT_PS2SR) & PS2SR_HAS_DATA)
		{
			unsigned char scancode = PORT(PORT_PS2DR);
			result = ps2_decode(scancode);
		}
	}
	return result & 0xFF;	
#elif USE_PS2
	unsigned char scancode = PORT(PORT_PS2DR);
	unsigned int result = 0;
	while(result == 0) result = ps2_decode(scancode);
	return result & 0xFF;
#elif USE_UART
	return PORT(UART_PORT);
#endif
}

int main(char argc, char *argv[])
{
	stdio_print("PS2 Test routine\n");

	while(1)
	{
		stdio_putchar(stdio_default_getchar());
	}
}
