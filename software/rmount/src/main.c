#include "stdlib.h"
#include "string.h"
#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

int main(char argc, char *argv[])
{
	if(argc != 2)
	{
		stdio_print("error: missing name \n");
		return 1;
	}
	
	char *name = argv[1];
	
	int result = file_umount(name);
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}	
	
	result = file_mount(name);
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}
	return 0;
}
