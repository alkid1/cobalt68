#include "stdlib.h"
#include "string.h"

#include "lib_fileio.h"

#include "type_file_result.h"

int main(char argc, char *argv[])
{
	if(argc < 2)
	{
		stdio_print("error: missing name \n");
		return 1;
	}
	
	for(int i = 1; i < argc; i++)
	{
		char *name = argv[i];
		
		stdio_print(name);
		stdio_print(" ");
		type_file_result(file_delete(name));
	}
	return 0;
}
