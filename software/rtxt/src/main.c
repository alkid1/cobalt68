#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

extern char _free_area[];

int func_rtxt(const char *name)
{
	file_t file;
	
	long readBytes;
	
	stdio_print("Awaiting text input followed by Ctrl+C.\n>");
	
	stdio_getText(_free_area, 1024 * 128, &readBytes);
	stdio_print("Input complete.\n>");

	//kprint("Size: "); kprintHex32(readBytes); kprint("\n");
	printf("Size: %X\n", readBytes);
	
	FILE_RESULT result = file_open(name, FSYS_CREATE | FSYS_CREATENEW, &file); 
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return result;
	}
	
	result = file_write(file, _free_area, readBytes);
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return result;
	}	

	result = file_close(file);
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return result;
	}		
	
	return FSYS_OK;
}

int main(char argc, char *argv[])
{
	if(argc != 2)
	{
		stdio_print("error: missing name \n");
		return 1;
	}
	
	func_rtxt(argv[1]);
	return 0;
}
