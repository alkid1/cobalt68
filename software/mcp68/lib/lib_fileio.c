#ifndef MCP68_KERNEL
#include <string.h>
#endif

#include "lib_fileio.h"

/*
	File io library
*/


FILE_RESULT FILE_FUNCID(file_mount)(const char *name)
{
	fsys_args_t args = { .data = (char*)name, .handler = 1 };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_MOUNT, &args);
}

FILE_RESULT FILE_FUNCID(file_umount)(const char *name)
{
	fsys_args_t args = { .data = (char*)name, .handler = 0 };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_MOUNT, &args);	
}

FILE_RESULT FILE_FUNCID(file_setcwd)(const char *name)
{
	fsys_args_t args = { .data = (char*)name };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_SETCWD, &args);
}

FILE_RESULT FILE_FUNCID(file_getcwd)(const char *name, unsigned int length)
{
	fsys_args_t args = { .data = (char*)name, .size = length };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_GETCWD, &args);	
}

FILE_RESULT FILE_FUNCID(file_setdrive)(const char *name)
{
	fsys_args_t args = { .data = (char*)name };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_SETDRIVE, &args);		
}

FILE_RESULT FILE_FUNCID(file_info)(const char *name, fsys_filerec_t *rec)
{
	fsys_args_t args = { .data = (char*)name, .filerec = rec };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_INFO, &args);			
}

FILE_RESULT FILE_FUNCID(file_mkdir)(const char *name)
{
	fsys_args_t args = { .data = (char*)name };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_MAKE_DIR, &args);			
}

FILE_RESULT FILE_FUNCID(file_format)(const char *name)
{
	fsys_args_t args = { .data = (char*)name };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_FORMAT, &args);
}

FILE_RESULT FILE_FUNCID(file_open)(const char *name, unsigned long flags, file_t* pFile)
{
	if(pFile == 0) return FSYS_ARGUMENT;
	
	fsys_args_t args = {.data = (char*)name, .flags = flags };
	FILE_RESULT result = FILE_SYSCALL(MODULE_FSYS_ID | FSYS_OPEN, &args);
	
	*pFile = args.handler;
	
	return result;
}

FILE_RESULT FILE_FUNCID(file_close)(file_t file)
{
	fsys_args_t args = { .handler = file  };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_CLOSE, &args);
}

FILE_RESULT FILE_FUNCID(file_seek)(file_t file, size_t offset, int origin)
{
	fsys_args_t args = {.handler = file, .size = origin, .offset = offset };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_SEEK, &args);
}

FILE_RESULT FILE_FUNCID(file_read)(file_t file, void *data, size_t size, size_t* bytesRead)
{
	fsys_args_t args = {.handler = file, .data = data, .size = size };
	
	FILE_RESULT result = FILE_SYSCALL(MODULE_FSYS_ID | FSYS_READ, &args);
	
	if(bytesRead) *bytesRead = args.size;
	
	return result;
}

FILE_RESULT FILE_FUNCID(file_write)(file_t file, void *data, size_t size)
{
	fsys_args_t args = {.handler = file, .data = data, .size = size };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_WRITE, &args);
}

FILE_RESULT FILE_FUNCID(file_writeString)(file_t file, char * message)
{
	return FILE_FUNCID(file_write)(file, message, strlen(message));
}

FILE_RESULT FILE_FUNCID(file_delete)(const char *name)
{
	fsys_args_t args = { .data = (char*)name };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_DELETE, &args);
}

FILE_RESULT FILE_FUNCID(file_list)(const char *path, fsys_filerec_t *filerec, unsigned long *tracker)
{
	fsys_args_t args = { .data = (char*)path, .filerec = filerec, .handler = *tracker };
	
	FILE_RESULT result = FILE_SYSCALL(MODULE_FSYS_ID | FSYS_LIST, &args);
	
	*tracker = args.handler; 

	return result;
}

FILE_RESULT FILE_FUNCID(file_truncate)(file_t file, size_t size)
{
	fsys_args_t args = {.handler = file, .size = size };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_TRUNC, &args);
}

FILE_RESULT FILE_FUNCID(file_rename)(const char *oldpath, const char *newpath)
{
	fsys_args_t args = { .data = (char*)oldpath, .data2 = (char*)newpath };
	return FILE_SYSCALL(MODULE_FSYS_ID | FSYS_RENAME, &args);
}