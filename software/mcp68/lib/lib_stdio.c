#include "lib_stdio.h"
#include "kernel.h"

void stdio_putchar(char c)
{
	stdio_arg_t args = { &c, 0xFF, 0};
	_call_trap0(MODULE_STDIO_ID | STDIO_PUTCHAR, &args);
}

void stdio_putblock(const char *data, size_t size)
{
	stdio_arg_t args = { (char*)data, size, 0};
	_call_trap0(MODULE_STDIO_ID | STDIO_PUTBLOCK, &args);	
}

char stdio_getchar()
{
	char c;
	stdio_arg_t args = { &c, 0xFF , 0};
	_call_trap0(MODULE_STDIO_ID | STDIO_GETCHAR, &args);
	
	return c;
}

void stdio_print(const char *msg)
{
	stdio_arg_t args = {(char*)msg, 0xFF , 0};
	_call_trap0(MODULE_STDIO_ID | STDIO_PUTSTRING, &args);
}

STDIO_RESULT stdio_getString(char* buffer, size_t size, size_t *bytesRead)
{
	stdio_arg_t args = { .data = buffer, .size = size, .delimiter = NEWLINE_CODE };
	
	STDIO_RESULT result = _call_trap0(MODULE_STDIO_ID | STDIO_GETSTRING, &args);
	
	if(bytesRead) *bytesRead = args.size;
	
	return result;
}

STDIO_RESULT stdio_getText(char* buffer, size_t size, size_t *readBytes)
{
	stdio_arg_t args = { .data = buffer, .size = size, .delimiter = 0x3 };
	STDIO_RESULT result = _call_trap0(MODULE_STDIO_ID | STDIO_GETSTRING, &args);
	
	if(readBytes) *readBytes = args.size;
	
	return result;
}

STDIO_RESULT stdio_getConfiguration(stdio_configuration_t *data)
{
	stdio_arg_t args = { .data = (char*)data };
	return _call_trap0(MODULE_STDIO_ID | STDIO_GETCONFIGURATION, &args);
}

STDIO_RESULT stdio_setConfiguration(stdio_configuration_t *data)
{
	stdio_arg_t args = { .data = (char*)data };
	return _call_trap0(MODULE_STDIO_ID | STDIO_SETCONFIGURATION, &args);	
}



