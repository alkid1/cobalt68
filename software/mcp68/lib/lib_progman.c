#include "kernel.h"
#include "mod_progman.h"


void progman_terminate()
{
	_call_trap0(MODULE_PROGMAN_ID | PROGMAN_TERMINATE, 0);
}

unsigned int progman_run(const char *path, const char *args)
{
	progman_args_t targs;
	
	targs.path = (char *)path;
	targs.args = (char *)args;
	
	return _call_trap0(MODULE_PROGMAN_ID | PROGMAN_FILE_RUN, &targs);
}

unsigned int progman_getargs(char *args, unsigned int size)
{
	progman_args_t targs;
	
	targs.args = args;
	targs.size = size;
	
	return _call_trap0(MODULE_PROGMAN_ID | PROGMAN_GET_ARGS, &targs);
}