/*
 *  File: spi_io.c.example
 *  Author: Nelson Lombardo
 *  Year: 2015
 *  e-mail: nelson.lombardo@gmail.com
 *  License at the end of file.
 */

#include "plib.h"
#include "spi_io.h"

/******************************************************************************
 Module Public Functions - Low level SPI control functions
******************************************************************************/

void SPI_Init (void) 
{
   // Nothing
}

BYTE SPI_RW (BYTE d) 
{
	return spi_sendByte(d);
}

void SPI_Release (void) 
{
   // Nothing
   // spi_unselectSlave();
}

inline void SPI_CS_Low (BYTE slaveIndex)
{
	spi_selectSlave(slaveIndex);
}

inline void SPI_CS_High (void)
{
	spi_unselectSlave();
}

inline void SPI_Freq_High (void) 
{
   // Nothing
}

inline void SPI_Freq_Low (void) 
{
   // Nothing
}

long spi_timer_counter;

void SPI_Timer_On (WORD ms) 
{
	spi_timer_counter = ms;
}

inline BOOL SPI_Timer_Status (void) 
{
	if(spi_timer_counter) spi_timer_counter--;
    return spi_timer_counter > 0;
}

inline void SPI_Timer_Off (void) 
{
}


/*
The MIT License (MIT)

Copyright (c) 2015 Nelson Lombardo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
