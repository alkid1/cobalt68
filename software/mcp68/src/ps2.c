/* 
	PS/2 Keyboard decoder
*/

unsigned char ps2_unshifted[] = 
{
	0x0e,'`',
	0x15,'q',
	0x16,'1',
	0x1a,'z',
	0x1b,'s',
	0x1c,'a',
	0x1d,'w',
	0x1e,'2',
	0x21,'c',
	0x22,'x',
	0x23,'d',
	0x24,'e',
	0x25,'4',
	0x26,'3',
	0x29,' ',
	0x2a,'v',
	0x2b,'f',
	0x2c,'t',
	0x2d,'r',
	0x2e,'5',
	0x31,'n',
	0x32,'b',
	0x33,'h',
	0x34,'g',
	0x35,'y',
	0x36,'6',
	0x39,',',
	0x3a,'m',
	0x3b,'j',
	0x3c,'u',
	0x3d,'7',
	0x3e,'8',
	0x41,',',
	0x42,'k',
	0x43,'i',
	0x44,'o',
	0x45,'0',
	0x46,'9',
	0x49,'.',
	0x4a,'/',
	0x4b,'l',
	0x4c,';',
	0x4d,'p',
	0x4e,'-',
	0x52,'`',
	0x54,'[',
	0x55,'=',
	0x5a, 13,
	0x5b,']',
	0x5d,'/',
	0x61,'<',
	0x66, 127,   // Backspace
	0x69,'1',
	0x6b,'4',
	0x6c,'7',
	0x70,'0',
	0x71,',',
	0x72,'2',
	0x73,'5',
	0x74,'6',
	0x75,'8',
	0x79,'+',
	0x7a,'3',
	0x7b,'-',
	0x7c,'*',
	0x7d,'9',
	0,0 
};

unsigned char ps2_shifted[] = 
{
	0x0e,'`',
	0x15,'Q',
	0x16,'!',
	0x1a,'Z',
	0x1b,'S',
	0x1c,'A',
	0x1d,'W',
	0x1e,'@',
	0x21,'C',
	0x22,'X',
	0x23,'D',
	0x24,'E',
	0x25,'$',
	0x26,'#',
	0x29,' ',
	0x2a,'V',
	0x2b,'F',
	0x2c,'T',
	0x2d,'R',
	0x2e,'%',
	0x31,'N',
	0x32,'B',
	0x33,'H',
	0x34,'G',
	0x35,'Y',
	0x36,'^',
	0x39,'L',
	0x3a,'M',
	0x3b,'J',
	0x3c,'U',
	0x3d,'&',
	0x3e,'*',
	0x41,'<',
	0x42,'K',
	0x43,'I',
	0x44,'O',
	0x45,')',
	0x46,'(',
	0x49,'>',
	0x4a,'?',
	0x4b,'L',
	0x4c,':',
	0x4d,'P',
	0x4e,'_',
	0x52,'"',
	0x54,'{',
	0x55,'+',
	0x5a,13,
	0x5b,'}',
	0x5d,'|',
	0x61,'>',
	0x66, 127,   // Backspace
	0x69,'1',
	0x6b,'4',
	0x6c,'7',
	0x70,'0',
	0x71,',',
	0x72,'2',
	0x73,'5',
	0x74,'6',
	0x75,'8',
	0x79,'+',
	0x7a,'3',
	0x7b,'-',
	0x7c,'*',
	0x7d,'9',
	0,0 
};


unsigned char ps2_ctrl[] = 
{
	0x1c, 1, //'A',
	0x32, 2, //'B',	
	0x21, 3, //'C',
	0x23, 4, //'D',
	0x24, 5, //'E',
	0x2b, 6, //'F',	
	0x34, 7, //'G',	
	0x33, 8, //'H',	
	0x43, 9, //'I',
	0x3b, 10,//'J',	
	0x42, 11,//'K',
	0x4b, 12,//'L',	
	0x3a, 13,//'M',
	0x31, 14,//'N',
	0x44, 15,//'O',
	0x4d, 16,//'P',
	0x15, 17,//'Q',
	0x2d, 18,//'R',	
	0x1b, 19,//'S',
	0x2c, 20,//'T',	
	0x3c, 21, //'U',	
	0x2a, 22,//'V',	
	0x1d, 23,//'W',
	0x22, 24,//'X',
	0x35, 25,//'Y',
	0x1a, 26,//'Z',
	0,0 	
};


unsigned char ps2_CSIEscaped[] = 
{
	0x75, 'A', // UP arrow
	0x72, 'B', // Down
	0x6b, 'D', // Left
	0x74, 'C', // Right
	0,0
};

static unsigned char lookupDecodeTable(unsigned char code, unsigned char *table)
{
	while(table[1] != 0)
	{
		if(table[0] == code) return table[1];
		table += 2;
	}
	return 0;
}

enum 
{
	Immediate = 0,
	Escape    = 1,  // \x char 
	CSIPrefix = 2,  // [ char
	Stored    = 3
};

unsigned int ps2_decode(unsigned char sc) 
{
	static unsigned char is_up = 0, shift = 0, ctrl = 0, mode = Immediate, ext = 0, stored;
	unsigned char i;
	
	switch(mode)
	{
		case CSIPrefix:
			mode = Stored;
			return '[';
		case Stored: 
			ext = 0;			
			mode = Immediate;
			return stored;
	}
	
	if(sc == 0)
	{
		return 0; // special case
	}
   
	if(sc == 0xE0)
	{
		ext = 1;  // Extended code
		return 0;
	}
  
	if (!is_up) 
	{
		if(ext == 0)
		{
			switch (sc) 
			{
			case 0x14 :// Left Ctrl			
				ctrl = 1;
				break;			
			case 0xF0 :// The up-key identifier
				is_up = 1;
				break;
			case 0x12 :// Left SHIFT
			case 0x59 :// Right SHIFT		 
				shift = 1;
				break;
			default:
				{
					return lookupDecodeTable(sc, 
						ctrl 
							? ps2_ctrl
							: (shift 
								? ps2_shifted 
								: ps2_unshifted));			
				}
			}
		}
		else
		{
			if(sc == 0xF0)
			{
				is_up = 1;
				return 0;
			}
			
			stored = lookupDecodeTable(sc, ps2_CSIEscaped);
			
			if(stored == 0)
			{
				return 0;
			}
			
			mode = CSIPrefix;
			
			return '\x1b';
		}
	}
	else 
	{
		is_up = 0;// Two 0xF0 in a row not allowed
		switch (sc) 
		{
		case 0x14 :// Left Ctrl
			ctrl = 0;
			break;
		case 0x12 :// Left SHIFT
		case 0x59 :// Right SHIFT
			shift = 0;
			break;
		default:
			ext = 0;
			break;
		}
	}
	return 0;
}       