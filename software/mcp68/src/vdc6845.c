#include "plib.h"
#include "vdc6845.h"

#define VRAM 0xF8000

#define REG_ADDR 0xFE000
#define REG_DATA 0xFE001

#define HSIZE  80
#define VSIZE  25

#define VRAMSIZE  (HSIZE * VSIZE)

#define BLANK_CHAR      0
#define HARDWARE_SCROLL 1

unsigned int crtc6845_cursorAddress(VDC_t *vdc)
{
    return vdc->scroll + vdc->cursor_row * vdc->cols + vdc->cursor_col;
}

unsigned int crtc6845_rowAddress(VDC_t *vdc)
{
    return vdc->scroll + vdc->cursor_row * vdc->cols;
}

void crtc6845_writeRegister(unsigned char address, unsigned char value)
{
    volatile int c = 0;
    *((unsigned char*)REG_ADDR) = address;
    for(int i = 0; i < 8; i++) c++;
    *((unsigned char*)REG_DATA) = value;
    for(int i = 0; i < 8; i++) c++;
}

void crtc6845_updateCursor(VDC_t *vdc)
{
    unsigned int address = crtc6845_cursorAddress(vdc);
    
    crtc6845_writeRegister(14, (address >> 8) & 0xFF);  //cursor one address h byte
    crtc6845_writeRegister(15,  address       & 0xFF);  //cursor one address l byte
}

void vdc_updateScroll(VDC_t* vdc)
{
    unsigned int address = vdc->scroll;
    crtc6845_writeRegister(12, (address >> 8) & 0xFF);
    crtc6845_writeRegister(13,  address       & 0xFF);
}

void vdc_clear(VDC_t* vdc)
{
    char* ptr = (char *)vdc->vram;
    
    for(int i = 0; i < vdc->vram_size * 2;i++)
    {
        *ptr++ = BLANK_CHAR;
    }   
    
    vdc->scroll = 0;
    vdc_updateScroll(vdc);
}

void vdc_setCursor(VDC_t* vdc, char col, char row)
{
    vdc->cursor_col = col;
    vdc->cursor_row = row;
    
    crtc6845_updateCursor(vdc);
}

void crtc6845_initialize()
{
    crtc6845_writeRegister(0,99);       // H total chars
    crtc6845_writeRegister(1,HSIZE);    // H displayed chars
    crtc6845_writeRegister(2,83);       // Hsync position
    crtc6845_writeRegister(3,0x2c);     // sync width v 4 bit h 4 bit
/*  
    crtc6845_writeRegister(4,64);       // vtotal rows
    crtc6845_writeRegister(5,6);        // vtotal adjust
    crtc6845_writeRegister(6,VSIZE);    // vtotal disp
    crtc6845_writeRegister(7,61);       // vsync position
    crtc6845_writeRegister(8,0);        // interlace mode
    crtc6845_writeRegister(9,7);        // rows per char
*/

    crtc6845_writeRegister(4,32);       // vtotal rows
    crtc6845_writeRegister(5,3);        // vtotal adjust
    crtc6845_writeRegister(6,VSIZE);    // vtotal disp
    crtc6845_writeRegister(7,31);       // vsync position
    crtc6845_writeRegister(8,0);        // interlace mode
    crtc6845_writeRegister(9,15);       // rows per char

    crtc6845_writeRegister(10,14);      // cursor row start
    crtc6845_writeRegister(11,16);      // cursor row end
    crtc6845_writeRegister(12,0);       // screen one start h byte
    crtc6845_writeRegister(13,0);       // screen one start l byte
    crtc6845_writeRegister(14,0);       // cursor one address h byte
    crtc6845_writeRegister(15,0);       // cursor one address l byte
}

void vdc_scrollUp(VDC_t *vdc)
{
#ifdef HARDWARE_SCROLL
    vdc->scroll += vdc->cols;
    
    if(vdc->scroll == vdc->vram_size)
    {
        memcpy(vdc->vram, vdc->vram + vdc->vram_size, vdc->vram_size);
        vdc->scroll = 0;
    }
    
    vdc_updateScroll(vdc);
    
    // clear new line.
    for(int i = 0; i < vdc->cols; i++)
    {
        *(vdc->vram + vdc->scroll + vdc->vram_size - vdc->cols + i) = BLANK_CHAR;
    }
#else       
    for(int i = 0; i < vdc->cols * (vdc->rows - 1); i++)
    {
        *(vdc->vram + i) = *(vdc->vram + vdc->cols + i);
    }   
    
    // clear new line.
    for(int i = 0; i < vdc->cols; i++)
    {
        *(vdc->vram + vdc->vram_size - vdc->cols + i) = BLANK_CHAR;
    }
#endif      
}

void vdc_scrollDown(VDC_t *vdc)
{
#ifdef HARDWARE_SCROLL
    if(vdc->scroll == 0)
    {
        memcpy(vdc->vram + vdc->vram_size, vdc->vram, vdc->vram_size);
        vdc->scroll = vdc->vram_size;
    }
	
    vdc->scroll -= vdc->cols;	
    
    vdc_updateScroll(vdc);
    
    // clear new line.
    for(int i = 0; i < vdc->cols; i++)
    {
        *(vdc->vram + vdc->scroll - vdc->cols + i) = BLANK_CHAR;
    }
#else	
    for(int i = vdc->cols * (vdc->rows - 1); i >= 0; i--)
    {
        *(vdc->vram + vdc->cols + i) = *(vdc->vram + i);
    }   
    
    // clear new line.
    for(int i = 0; i < vdc->cols; i++)
    {
        *(vdc->vram + i) = BLANK_CHAR;
    }
#endif
}

void vdc_terminal_D(struct lw_terminal * terminal)
{
    VDC_t *vdc = (VDC_t*)terminal->user_data;
    vdc_scrollUp(vdc);
}

void vdc_terminal_M(struct lw_terminal * terminal)
{
    VDC_t *vdc = (VDC_t*)terminal->user_data;
    vdc_scrollDown(vdc);    
}

void vdc_terminal_H(struct lw_terminal * terminal)
{
    VDC_t *vdc = (VDC_t*)terminal->user_data;
    
    if(terminal->argc != 2) return;
    
    vdc_setCursor(vdc, terminal->argv[1] - 1, terminal->argv[0] - 1);
}

void vdc_terminal_J(struct lw_terminal * terminal)
{
    vdc_clear((VDC_t*)terminal->user_data);
}

void vdc_terminal_K(struct lw_terminal * terminal)
{
    VDC_t *vdc = (VDC_t*)terminal->user_data;
    
    memset((void*)(vdc->vram + crtc6845_rowAddress(vdc)), 0, vdc->cols);
}

void vdc_putNewLine(VDC_t *vdc)
{
    vdc->cursor_row++;
    
    // if last row - scroll
    if(vdc->cursor_row == vdc->rows)
    {
        vdc->cursor_row = vdc->rows - 1;
        
        vdc_scrollUp(vdc);
    }
}

void vdc_terminal_write(struct lw_terminal * terminal, char c)
{
    VDC_t * vdc = (VDC_t*)terminal->user_data;

    if(c == '\t')
    {
        for(int i = 0; i < 4; i++) vdc_terminal_write(terminal, ' ');
    }
    else  if(c == 127) // Backspace
    {
        if(vdc->cursor_col > 0)
        {
            vdc->cursor_col--;      
        }
        *(vdc->vram + crtc6845_cursorAddress(vdc)) = BLANK_CHAR;
    }
    else if(c == 0xA) // Newline
    {
        vdc_putNewLine(vdc);
    }
    else if(c == 0xD) // Carriage return
    {
        vdc->cursor_col = 0;
        crtc6845_updateCursor(vdc);
    }
    else
    {
        if(vdc->cursor_col >= 80)
        {
            vdc_putNewLine(vdc);
        }		
		
        *(vdc->vram + crtc6845_cursorAddress(vdc)) = c;
        vdc->cursor_col++;
	}
    
    crtc6845_updateCursor(vdc);     
}

void vdc_putchar(VDC_t * vdc, char c)
{
    lw_terminal_parser_read(&vdc->terminal, c);
}

void vdc_initialize(VDC_t* vdc)
{
    vdc->cols = HSIZE;
    vdc->rows = VSIZE;
    
    vdc->vram_size = vdc->cols * vdc->rows;
    vdc->vram = (char*)VRAM;
    
    vdc->cursor_col = 0;
    vdc->cursor_row = 0;
    
    vdc->scroll = 0;

    memset(vdc->vram, 0, vdc->vram_size);
    
    crtc6845_initialize();
    crtc6845_updateCursor(vdc);
    
    lw_terminal_parser_init(&vdc->terminal);
    vdc->terminal.user_data = vdc;
    
    vdc->terminal.callbacks.csi.J = vdc_terminal_J;
    vdc->terminal.callbacks.csi.H = vdc_terminal_H;
    vdc->terminal.callbacks.csi.K = vdc_terminal_K;
    
    vdc->terminal.callbacks.esc.D = vdc_terminal_D;
    vdc->terminal.callbacks.esc.M = vdc_terminal_M; 
    vdc->terminal.callbacks.esc.K = vdc_terminal_K;
    
    vdc->terminal.write = vdc_terminal_write;
}
