#pragma once

#include "mod_stdio.h"

void module_stdio_handler_init();
unsigned long module_stdio_handler(unsigned long code, void* data);