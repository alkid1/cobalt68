/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "plib.h"
#include "printf.h"
#include "ff.h"			/* Obtains integer types */
#include "diskio.h"		/* Declarations of disk functions */

#include "sd_io.h"


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

static char disk_trace = 1;

SD_DEV sd_device[2];

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
	int result = 0;
	SDRESULTS sdr = SD_Status(&sd_device[pdrv]);
	if(sdr) result = STA_NOINIT;

	return result;
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	SDRESULTS res = SD_Init(&sd_device[pdrv], pdrv);
	
	if(disk_trace == 2)
	{
		kprint("SD_Init: "); kprintHex(res); kprint("\n");
	}
	
	
	if(res) 
	{
		return RES_ERROR;
	}	

	return res  == 0 ? 0 : STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	if(count > 1)
	{
		int res;
		
		for(int j = 0; j < 3; j++)
		{
			if((res = SD_ReadMultiple(&sd_device[pdrv], buff, sector, count)) == RES_OK) break;
			disk_initialize(pdrv);
		}
		
		if(res) 
		{
			if(disk_trace == 1) printf("MCP68: Error reading multiple sectors drive = %d, sector = %d, code = %d\n", pdrv, sector, res);
			return RES_ERROR;
		}	
	}
	else
	{
		for(int i = 0; i < count; i++, sector += 1, buff += 512)
		{
			int res;
		
			for(int j = 0; j < 3; j++)
			{
				if((res = SD_Read(&sd_device[pdrv], buff, sector)) == RES_OK) break;
				disk_initialize(pdrv);
			}
			
			if(disk_trace == 2)
			{
				kprint("SD_Read: "); kprintHex(res); kprint("\n");						
			}
			
			if(res) 
			{
				if(disk_trace == 1) printf("MCP68: Error reading drive = %d, sector = %d, code = %d\n", pdrv, sector, res);
				return RES_ERROR;
			}
		}
	}
	return RES_OK;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	const BYTE *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	for(int i = 0; i < count; i++, sector += 1, buff += 512)
	{
		int res;
		
		for(int j = 0; j < 3; j++)
		{
			if((res = SD_Write(&sd_device[pdrv], (void*)buff, sector)) == RES_OK) break;
			disk_initialize(pdrv);
		}
		
		if(disk_trace == 2) 
		{
			kprint("SD_Write: "); kprintHex(res); kprint("\n");
			return RES_ERROR;
		}
		
		if(res) 
		{
			if(disk_trace == 1) printf("MCP68: Error writing drive = %d, sector = %d, code = %d\n", pdrv, sector, res);
			return RES_ERROR;
		}
	}

	return RES_OK;
}

#endif


/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
	DRESULT res;
	int result;
	
	switch(cmd)
	{
		case GET_SECTOR_COUNT:
			*(DWORD*)buff = sd_device[pdrv].last_sector;
			return RES_OK;
		case GET_SECTOR_SIZE:
			*(DWORD*)buff = 512;
			return RES_OK;
			
		case GET_BLOCK_SIZE:
			*(DWORD*)buff = 512;
			return RES_OK;		

		case CTRL_SYNC:
			return RES_OK;					
	}

	return RES_PARERR;
}

