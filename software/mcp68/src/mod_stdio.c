
#include "hlib.h"
#include "printf.h"

#include "mod_stdio.h"

#define UART_PORT (0xFF800)
#define BOOT_PORT (UART_PORT + 1)
#define TEST_PORT (UART_PORT + 2)

#define UART_STATUS (UART_PORT + 15)

#define USR_IBF  1

#define PORT_PS2DR   (UART_PORT + 13)  // PS2 Data Register
#define PORT_PS2SR   (UART_PORT + 14)  // PS2 Status Register

#define PS2SR_HAS_DATA 1

#define PREPROCESS_CRLF 1

#define USE_6845 1
#define USE_PS2  1
#define USE_UART 1

#define PORT(P) *((volatile char*)P)

#ifdef USE_6845
#include "vdc6845.h"
VDC_t vdc;
#endif

#ifdef USE_PS2
#include "ps2.c"
#endif

static stdio_configuration_t configuration = 
{   
	// Default configuration
	sizeof(stdio_configuration_t), 
	STDIO_CFG_ECHO | STDIO_CFG_PPINPUT | STDIO_CFG_PPOUTPUT
};

void module_stdio_handler_init()
{
	#ifdef USE_6845
	vdc_initialize(&vdc);
	printf("STDIO using 6845 terminal.\n");
	#endif
	
	#if USE_PS2
	printf("STDIO using PS/2 keyboard.\n");
	#endif
	
	#if USE_UART
	printf("STDIO using serial IO.\n");
	#endif	
}

static void stdio_default_putchar(char c)
{
	#if USE_UART	
		PORT(UART_PORT) = c;
	#endif
	
	#ifdef USE_6845
		vdc_putchar(&vdc, c);
	#endif
	
	#ifdef PREPROCESS_CRLF
		if((configuration.flags & STDIO_CFG_PPOUTPUT) && c == 0xA) stdio_default_putchar(0xD);
	#endif	
}

void _putchar(char character)
{
	stdio_default_putchar(character);
}

char stdio_default_getchar()
{
	char cresult;
#if USE_PS2 && USE_UART
	unsigned int result = 0;
	
	while(result == 0)
	{
		if(PORT(UART_STATUS) & USR_IBF)
		{
			for(volatile char cc = 0;cc < 4; cc++);
			result = PORT(UART_PORT);
			break;
		}
		else if((result = ps2_decode(0)) != 0)
		{
		}
		else
		{
			if(PORT(PORT_PS2SR) & PS2SR_HAS_DATA)
			{
				unsigned char scancode = PORT(PORT_PS2DR);
				result = ps2_decode(scancode);
			}
		}
	}
	cresult = result & 0xFF;	
#elif USE_PS2
	unsigned char scancode = PORT(PORT_PS2DR);
	unsigned int result = 0;
	while(result == 0) result = ps2_decode(scancode);
	cresult = result & 0xFF;
#elif USE_UART
	cresult = PORT(UART_PORT);
#endif

#ifdef PREPROCESS_CRLF
	if((configuration.flags & STDIO_CFG_PPINPUT) && cresult == 0xD) cresult = 0xA;
#endif

	if(configuration.flags & STDIO_CFG_ECHO) stdio_default_putchar(cresult);
	
	return cresult;
}

unsigned long module_stdio_handler(unsigned long code, void* data)
{
	stdio_arg_t *arg = (stdio_arg_t*)data;
	
	switch(code)
	{
	case STDIO_PUTBLOCK:
		for(unsigned int i = 0; i < arg->size; i++) 
			stdio_default_putchar(arg->data[i]);
		return STDIO_OK;
		
	case STDIO_PUTSTRING:
		for(unsigned int i = 0; (arg->data[i] != 0) && (i < arg->size); i++) 
			stdio_default_putchar(arg->data[i]);
		return STDIO_OK;
		
	case STDIO_GETBLOCK:
		for(unsigned int i = 0; i < arg->size; i++) 
			arg->data[i] = stdio_default_getchar();
		return STDIO_OK;
		
	case STDIO_GETSTRING:
		for(unsigned int i = 0; i < (arg->size - 1); i++) 
		{
			char c = stdio_default_getchar();
			if(c == arg->delimiter)
			{
				arg->data[i] = 0;
				arg->size    = i;
				return STDIO_EOL;
			}
			else
			{
				arg->data[i] = c;
			}
		}
		arg->data[arg->size - 1] = 0;
		arg->size = arg->size - 1;
		return STDIO_OK;
		
	case STDIO_PUTCHAR:
		stdio_default_putchar(*arg->data);
		return STDIO_OK;
	
	case STDIO_GETCHAR:
		*arg->data = stdio_default_getchar();
		return STDIO_OK;
		
	case STDIO_GETCONFIGURATION:
	{
		stdio_configuration_t * cfg = (stdio_configuration_t*)arg->data;
		if(cfg->size != sizeof (stdio_configuration_t)) return STDIO_EINVAL;
		*cfg = configuration;
		break;
	}
	case STDIO_SETCONFIGURATION:		
	{
		stdio_configuration_t * cfg = (stdio_configuration_t*)arg->data;
		if(cfg->size != sizeof (stdio_configuration_t)) return STDIO_EINVAL;
		configuration = *cfg ;
		break;
	}

	default: 
		return RESULT_BADCOMMAND;
	}
}