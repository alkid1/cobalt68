#define MCP68_KERNEL

#include "mcp68.h"
#include "plib.h"
#include "printf.h"

#include "klib_fileio.h"

#include "fsys_fat32.h"
#include "mod_progman_i.h"
#include "mod_stdio_i.h"


void _launch_user_code();
unsigned long trap0_handler(unsigned long code, void* data);

fat32fs_t fat32fs;

unsigned int exception_pc;

void address_exception()
{
	printf("Fatal error: misaligned memory access at 0x%x.\n", exception_pc);
	_launch_user_code();
}

unsigned long trap0_handler(unsigned long code, void* data)
{
	switch(code & MODULE_MASK)
	{
	case MODULE_STDIO_ID:
		return module_stdio_handler(code & (~MODULE_MASK), data);
	case MODULE_FSYS_ID:
		//return fsys_handler(code & (~MODULE_MASK), data);		
		return fat32_handler(&fat32fs, code & (~MODULE_MASK), data);
	case MODULE_PROGMAN_ID:
		return module_progman_handler(code & (~MODULE_MASK), data);				
	default:
		return RESULT_BADMODULE;
	}
	
	return 0;
}

char userStack[1024];

void kmain()
{
	for(int i = 0; i < 256; i++) userStack[i] = 'C';
	
	module_stdio_handler_init();
	
	printf("MCP-68\n");
	printf("Kernel Init\n");

	fat32_initInstance(&fat32fs);

	printf("Mounting 0:/, result = %x \n", kfile_mount("0:"));
	printf("Mounting 1:/, result = %x \n", kfile_mount("1:"));

	printf("Type 'help' for list of commands.\n"); 	
	
	_launch_user_code();
}

