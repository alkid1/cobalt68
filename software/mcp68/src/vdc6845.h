#pragma once

#include "lw_terminal_parser.h"

typedef struct 
{
    int    vram_size;   
    char * vram;
    
    unsigned char cols;
    unsigned char rows;
    
    unsigned char cursor_col;
    unsigned char cursor_row;
    
    unsigned int scroll;
    
    struct lw_terminal terminal;
} VDC_t;



void vdc_initialize(VDC_t* vdc);
void vdc_putchar(VDC_t * vdc, char c);