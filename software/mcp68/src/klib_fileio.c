#ifdef FILE_PREFIX 
	#undef FILE_PREFIX
#endif

#ifdef FILE_SYSCALL 
	#undef FILE_SYSCALL
#endif

#define FILE_PREFIX  k
#define FILE_SYSCALL trap0_handler

unsigned long trap0_handler(unsigned long code, void* data);

#include "lib_fileio.c"

#undef FILE_PREFIX
#undef FILE_SYSCALL