#include "plib.h"
#include "fsys_fat32.h"
#include "mod_fsys.h"
#include "ff.h"
#include "printf.h"

#define ZERO_OBJECT(o) memset(o, 0, sizeof(o));

unsigned long fat32_initInstance(fat32fs_t* fs)
{
	ZERO_OBJECT(fs->files);
	ZERO_OBJECT(fs->dirs);
	ZERO_OBJECT(fs->fs);
	
	for(int i = 0; i < FAT32FS_FILES; i++) fs->files[i].used = FALSE;
	for(int i = 0; i < FAT32FS_DIRS;  i++) fs->dirs [i].used = FALSE;
	for(int i = 0; i < FAT32FS_FSS;   i++) fs->fs   [i].used = FALSE;	
	
	return FR_OK;
}

unsigned int fat32_getFreeFile(fat32fs_t *fs)
{
	for(int i = 0; i < FAT32FS_FILES; i++) 
		if(fs->files[i].used == FALSE) return i;
	
	return -1;
}

unsigned int fat32_getFreeDir(fat32fs_t *fs)
{
	for(int i = 0; i < FAT32FS_DIRS; i++) 
		if(fs->dirs[i].used == FALSE) return i;
	
	return -1;
}

unsigned int fat32_getFreeFS(fat32fs_t *fs)
{
	for(int i = 0; i < FAT32FS_FSS; i++) 
		if(fs->fs[i].used == FALSE) return i;
	
	return -1;
}

unsigned int fat32_mapResult(unsigned int f32r)
{
	switch(f32r)
	{
	case FR_OK:			     	/* (0) Succeeded */
		return FSYS_OK;
	case FR_DISK_ERR:			/* (1) A hard error occurred in the low level disk I/O layer */
		return FSYS_IOERROR;
	case FR_INT_ERR:			/* (2) Assertion failed */
		return RESULT_GENERALERROR;
	case FR_NOT_READY:			/* (3) The physical drive cannot work */
		return FSYS_IOERROR;
	case FR_NO_FILE:			/* (4) Could not find the file */
		return FSYS_NOTFOUND;
	case FR_NO_PATH:			/* (5) Could not find the path */
		return FSYS_NOTFOUND;
	case FR_INVALID_NAME:		/* (6) The path name format is invalid */
		return FSYS_ARGUMENT;
	case FR_DENIED:				/* (7) Access denied due to prohibited access or directory full */
		return FSYS_TABLELIMIT;
	case FR_EXIST:				/* (8) Access denied due to prohibited access */
		return FSYS_IOERROR;
	case FR_INVALID_OBJECT:		/* (9) The file/directory object is invalid */
		return RESULT_GENERALERROR;
	case FR_WRITE_PROTECTED:	/* (10) The physical drive is write protected */
		return FSYS_IOERROR;
	case FR_INVALID_DRIVE:		/* (11) The logical drive number is invalid */
		return FSYS_NOTFOUND;
	case FR_NOT_ENABLED:		/* (12) The volume has no work area */
		return FSYS_NOFS;
	case FR_NO_FILESYSTEM:		/* (13) There is no valid FAT volume */
		return FSYS_NOFS;
	case FR_MKFS_ABORTED:		/* (14) The f_mkfs() aborted due to any problem */
	case FR_TIMEOUT:			/* (15) Could not get a grant to access the volume within defined period */
	case FR_LOCKED:				/* (16) The operation is rejected according to the file sharing policy */
	case FR_NOT_ENOUGH_CORE:	/* (17) LFN working buffer could not be allocated */
		return RESULT_GENERALERROR;
	case FR_TOO_MANY_OPEN_FILES:/* (18) Number of open files > FF_FS_LOCK */
		return FSYS_FILELIMIT;
	case FR_INVALID_PARAMETER:	/* (19) Given parameter is invalid */
		return FSYS_ARGUMENT;
	default: 
		return RESULT_GENERALERROR;
	}
}

#define FMAP(var, f1, f2) (var & f1 ? f2 : 0) 

unsigned long fat32_open(fat32fs_t *fs, fsys_args_t *args)
{
	int handler = fat32_getFreeFile(fs);
	if(handler == -1) return FSYS_FILELIMIT;
	
	FIL* file = &fs->files[handler];
	
	int flags =   FMAP(args->flags, FSYS_CREATE   , FA_OPEN_ALWAYS)
				+ FMAP(args->flags, FSYS_CREATENEW, FA_CREATE_ALWAYS)
				+ FMAP(args->flags, FSYS_APPEND   , FA_OPEN_APPEND)
				+ FA_READ
				+ FA_WRITE;
		
	int result = f_open(file, args->data, flags);

	
	if(result == FR_OK) file->used = 1;
	
	args->handler = handler;
	
	return fat32_mapResult(result);
}

unsigned long fat32_close(fat32fs_t *fs, fsys_args_t *args)
{
	if(args->handler >= FAT32FS_FILES)         return FSYS_ARGUMENT;
	if(fs->files[args->handler].used == FALSE) return FSYS_ARGUMENT;
	
	f_close(&fs->files[args->handler]);
	
	fs->files[args->handler].used = FALSE;
	
	return FSYS_OK;
}

unsigned long fat32_seek(fat32fs_t *fs, fsys_args_t *args)
{
	if(args->handler >= FAT32FS_FILES)         return FSYS_ARGUMENT;
	if(fs->files[args->handler].used == FALSE) return FSYS_ARGUMENT;
	if(args->size != FSYS_SEEK_BEGIN)		   return FSYS_ARGUMENT;
	
	return fat32_mapResult(f_lseek(&fs->files[args->handler], args->offset));
}

void fat32_create_filerec(fsys_filerec_t *filerec,FILINFO *fno)
{
	strncpy(filerec->name, fno->fname, sizeof(filerec->name));
	filerec->size  = fno->fsize;	
	filerec->flags = FSYS_READABLE | FSYS_WRITEABLE
					| FMAP(fno->fattrib, AM_DIR, FSYS_DIRECTORY);
		;
}

unsigned long fat32_list(fat32fs_t *fs, fsys_args_t *args)
{
	if (args->handler == FSYS_LIST_END) return FSYS_ARGUMENT;
	if (args->handler >= FAT32FS_DIRS && args->handler != FSYS_LIST_START)  return FSYS_ARGUMENT;
	
	if(args->handler == FSYS_LIST_START)
	{
		int handler = fat32_getFreeDir(fs);
		if(handler == -1) return FSYS_FILELIMIT;
		
		int res = f_opendir(&fs->dirs[handler], args->data);
		if(res) return fat32_mapResult(res);
		
		args->handler = handler;
		fs->dirs[handler].used = 1;
	}
	
	FILINFO fno;
	
	int res = f_readdir(&fs->dirs[args->handler], &fno);                   /* Read a directory item */
	
    if (res != FR_OK || fno.fname[0] == 0)
	{
		f_closedir(&fs->dirs[args->handler]);
		fs->dirs[args->handler].used = 0;
		
		args->filerec->name[0] = 0;
		args->filerec->flags   = 0;
		args->filerec->size    = 0;
		args->handler = FSYS_LIST_END;

		return FSYS_OK;
	}
	
	fat32_create_filerec(args->filerec, &fno);
	
	return FSYS_OK;	
}

unsigned long fat32_read(fat32fs_t *fs, fsys_args_t *args)
{
	if (args->handler == FSYS_LIST_END) return FSYS_ARGUMENT;
	if (args->handler >= FAT32FS_DIRS)  return FSYS_ARGUMENT;
	
	int res = f_read(&fs->files[args->handler], args->data, args->size, &args->size);
	
	return fat32_mapResult(res);
}

unsigned long fat32_write(fat32fs_t *fs, fsys_args_t *args)
{
	if (args->handler == FSYS_LIST_END) return FSYS_ARGUMENT;
	if (args->handler >= FAT32FS_DIRS)  return FSYS_ARGUMENT;
	
	int res = f_write(&fs->files[args->handler], args->data, args->size, &args->size);
	
	return fat32_mapResult(res);
}

unsigned long fat32_mount(fat32fs_t *fs, fsys_args_t *args)
{
	if (args->handler > 1) return FSYS_ARGUMENT;
	if (args->data ==   0)  return FSYS_ARGUMENT;
	
	int res;
	
	if(args->handler)
	{
		int fsi = fat32_getFreeFS(fs);
		
		if(fsi == -1) return FSYS_TABLELIMIT;
	
		// mount
		res = f_mount(&fs->fs[fsi], args->data, 1);
	}
	else
	{
		res = f_mount(0, args->data, 0);
	}
	
	return fat32_mapResult(res);
}

unsigned long fsys_info(fsys_args_t *args)		
{
	FILINFO fno;
	
	int res = f_stat(args->data, &fno);
	
	if(res == 0)
	{
		fat32_create_filerec(args->filerec, &fno);
		return FSYS_OK;
	}
	else
	{
		args->filerec->name[0] = 0;
		args->filerec->flags   = 0;
		args->filerec->size    = 0;
		return fat32_mapResult(res);
	}
}

unsigned long fat32_truncate(fat32fs_t *fs, fsys_args_t *args)		
{
	f_lseek(&fs->files[args->handler], args->size);
	return f_truncate(&fs->files[args->handler]);
}

unsigned long fat32_handler(fat32fs_t *fs, unsigned long code, void* data)
{
	fsys_args_t *args = (fsys_args_t*)data;

	switch(code)
	{
	case FSYS_LIST:		
		return fat32_list(fs, args);
	case FSYS_FORMAT:
		return RESULT_UNSUPPORTED;
	case FSYS_DELETE:
	{
		int res = f_unlink(args->data);
		return fat32_mapResult(res);
	}
	case FSYS_OPEN:
		return fat32_open(fs, args);
	case FSYS_READ:
		return fat32_read(fs, args);
	case FSYS_WRITE:
		return fat32_write(fs, args);
	case FSYS_TRUNC:
		return fat32_truncate(fs, args);
	case FSYS_SEEK:
		return fat32_seek(fs, args);
	case FSYS_CLOSE:
		return fat32_close(fs, args);
	case FSYS_MOUNT:
		return fat32_mount(fs, args);		
	case FSYS_SETCWD:
		return fat32_mapResult(f_chdir(args->data));
	case FSYS_GETCWD:
		return fat32_mapResult(f_getcwd(args->data, args->size));
	case FSYS_SETDRIVE:
		return fat32_mapResult(f_chdrive(args->data));
	case FSYS_MAKE_DIR:
		return fat32_mapResult(f_mkdir(args->data));
	case FSYS_INFO:
		return fsys_info(args);
	case FSYS_RENAME:
		return fat32_mapResult(f_rename(args->data, args->data2));
	default: 
		return RESULT_BADCOMMAND;
	}
	return 0;
}
