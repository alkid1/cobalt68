#pragma once

#include "lib_fileio.h"

#ifdef FILE_PREFIX 
	#undef FILE_PREFIX
#endif

#ifdef FILE_SYSCALL 
	#undef FILE_SYSCALL
#endif

#define FILE_PREFIX  k
#define FILE_SYSCALL trap0_handler

#include "lib_fileio_f.inc"

#undef FILE_PREFIX
#undef FILE_SYSCALL