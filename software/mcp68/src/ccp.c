#undef FILE_PREFIX
#undef FILE_SYSCALL

#include "plib.h"
#include "hlib.h"
#include "printf.h"
#include "lib_stdio.h"
#include "lib_fileio.h"
#include "lib_progman.h"

#define INPUT_BUFFER_LENGTH 64

#define RHEX_BUF_SIZE (256 * 1024)

char *parse_pointer;
char inputBuffer[INPUT_BUFFER_LENGTH];
extern char _free_area[];

typedef char* (*tokenizer_func)();
typedef void (*handler_func)(tokenizer_func tokenizer);

typedef struct
{
	const char *name;
	const char *help;
	
	handler_func handler;
} command_descriptor;

void type_error(FILE_RESULT code)
{
	const char * message = 0;
	switch(code)
	{
		case FSYS_OK:
			message = "OK\n";
			break;
		case FSYS_BADINIT    :
			message = "Filesystem not initialized\n";
			break;
		case FSYS_NOTFOUND   :
			message = "File not found\n";
			break;
		case FSYS_NOFS       :
			message = "No filesystem present\n";
			break;
		case FSYS_IOERROR    :
			message = "IO error\n";
			break;
		case FSYS_FILELIMIT  :
			message = "File limit\n";
			break;
		case FSYS_TABLELIMIT :
			message = "Table limit\n";
			break;
		case FSYS_ARGUMENT   :
			message = "Invalid argument\n";
			break;
		case FSYS_FILEINUSE  :
			message = "File in use\n";
			break;
		case FSYS_FSYSLIMIT  :		
			message = "Filesystems limit\n";
			break;
		default:
			message = "Unspecified error\n";
			break;			
	}
	
	stdio_print(message);
}

void command_dir(tokenizer_func tokenizer)
{
	unsigned long tracker = 0;
	
	char * name = tokenizer();
	
	fsys_filerec_t filerec;
	
	FILE_RESULT result = file_list(name, &filerec, &tracker);
	
	if(result)
	{
		type_error(result);
		return;
	}
	
	while(result == FSYS_OK && result != FSYS_LIST_END && filerec.name[0] != 0)
	{
		stdio_print(filerec.name);
		stdio_print("\n");
		
		result = file_list(name, &filerec, &tracker);
	}
}

void command_del(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name\n");
		return;
	}
	
	type_error(file_delete(name));
}

int func_rtxt(const char *name)
{
	file_t file;
	
	long readBytes;
	
	stdio_print("Awaiting text input followed by Ctrl+C.\n>");
	
	stdio_configuration_t defcfg =
	{
		sizeof(stdio_configuration_t),
		0
	};	
	
	stdio_configuration_t stdiocfg = 
	{
		sizeof(stdio_configuration_t),
		STDIO_CFG_PPINPUT | STDIO_CFG_PPOUTPUT
	};
	
	stdio_getConfiguration(&defcfg);
	stdio_setConfiguration(&stdiocfg);	
	
	stdio_getText(_free_area, RHEX_BUF_SIZE, &readBytes);
	stdio_print("Input complete.\n>");

	//kprint("Size: "); kprintHex32(readBytes); kprint("\n");
	printf("Size: %X\n", readBytes);
	
	FILE_RESULT result = file_open(name, FSYS_CREATE | FSYS_CREATENEW, &file); 
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		stdio_setConfiguration(&defcfg);
		return result;
	}
	
	result = file_write(file, _free_area, readBytes);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		stdio_setConfiguration(&defcfg);
		return result;
	}	

	result = file_close(file);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		stdio_setConfiguration(&defcfg);
		return result;
	}		
	
	stdio_setConfiguration(&defcfg);
	return FSYS_OK;
}

void command_rtxt(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}
	
	func_rtxt(name);
}


void command_copy(tokenizer_func tokenizer)
{
	char * sourceName      = tokenizer();
	char * destinationName = tokenizer();
	
	if(sourceName == 0 || destinationName == 0)
	{
		stdio_print("usage: copy <source> <destination>\n");
		return;
	}	
	
	file_t sourceFile, destinationFile;
	
	FILE_RESULT fresult = file_open(sourceName, 0, &sourceFile);
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
		return;
	}
	
	unsigned long bytes_read;
	
	file_read(sourceFile, _free_area, RHEX_BUF_SIZE, &bytes_read);
	file_close(sourceFile);
	
	fresult = file_open(destinationName, FSYS_CREATE | FSYS_CREATENEW, &destinationFile);
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
		return;
	}
	
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
	}
	
	fresult = file_write(destinationFile, _free_area, bytes_read);
	
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
	}
	
	file_close(destinationFile);	
	
}

char    *hex2binBffer = 0;
long int hex2binSize = 0;

char command_hex2bin_getchar()
{
	if(hex2binSize == 0)
	{
		return 0x1b; // Escape	
	}
	
	char c = *hex2binBffer;
	
	hex2binSize--;
	hex2binBffer++;

	return c;
}

int func_hex2bin(const char *sourceName, const char *destinationName)
{
	unsigned long count = 0;
	file_t sourceFile, destinationFile;
	
	hex2binBffer = (char *)(_free_area + RHEX_BUF_SIZE);
	
	FILE_RESULT fresult = file_open(sourceName, 0, &sourceFile);
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
		return fresult;
	}
	
	fresult = file_read(sourceFile, hex2binBffer, RHEX_BUF_SIZE, &hex2binSize);
	
	hex2binSize += 1;
	
	if(fresult != FSYS_OK)
	{
		file_close(sourceFile);
		type_error(fresult);
		return fresult;
	}
	
	//kprint("Hex read OK, size: "); kprintHex32(hex2binSize); kprint("\n");
	printf("Hex read OK, size:%X \n", hex2binSize);
	
	int result = 0;
	
	// Read hex as binary into memory
	while(result >= 0)
	{
		result = readHexLine(_free_area, RHEX_BUF_SIZE, command_hex2bin_getchar);
		if(result >= 0) count += result;
	}
	file_close(sourceFile);

	//kprint("Final position: "); kprintHex32((unsigned int)hex2binBffer); kprint("\n");
	printf("Final position: %X\n", (unsigned int)hex2binBffer); 
	
	switch(result)
	{
	case RHL_EOF:
		stdio_print("Hex data read OK. \n");
		break;
	case RHL_BADFORMAT:
		stdio_print("Format error.\n");
		return 10;
	case RHL_BADCHECKSUM:
		stdio_print("Checksum error.\n");
		return 10;					
	case RHL_ABORT:
		stdio_print("Aborted.\n");
		return 10;
	case RHL_OVERFLOW:
		stdio_print("Overflow\n");
		return 10;
	default:
		stdio_print("Internal error\n");
		return 10;
	}	
	
	fresult = file_open(destinationName, FSYS_CREATE | FSYS_CREATENEW, &destinationFile);
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
		return fresult;
	}
	
	fresult = file_write(destinationFile, _free_area, count);
	
	if(fresult != FSYS_OK)
	{
		type_error(fresult);
	}
	
	file_close(destinationFile);
	return FSYS_OK;
}

void command_hex2bin(tokenizer_func tokenizer)
{
	char * sourceName      = tokenizer();
	char * destinationName = tokenizer();
	
	if(sourceName == 0 || destinationName == 0)
	{
		stdio_print("usage: hex2bin <source> <destination>\n");
		return;
	}
	
	func_hex2bin(sourceName, destinationName);
}

void command_rhex(tokenizer_func tokenizer)
{
	const char *tempFileName = "rhex.tmp";
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("usage: rhex <destination>\n");
		return;		
	}
	
	file_delete(tempFileName);

	if(func_rtxt(tempFileName))
	{
		stdio_print("Error receiving ihex\n");
		return;
	}
		
	if(func_hex2bin(tempFileName, name))
	{
		stdio_print("Error decoding ihex\n");
		return;
	}
	
	stdio_print("Deleting temporary file "); stdio_print(tempFileName); stdio_print(" \n");
	type_error(file_delete(tempFileName));	
}

void command_type(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}
	
	file_t file;
	
	FILE_RESULT result = file_open(name, 0, &file); 
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}
	
	long read = 0;
	char c;
	
	result = file_read(file, &c, 1, &read);

	while(read)
	{
		if(result != FSYS_OK)
		{
			type_error(result);
			break;
		}
		stdio_putchar(c);
		
		result = file_read(file, &c, 1, &read);
	}
		
	result = file_close(file);	
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}
}

void run(const char* name, const char *args)
{
	char buf[64];
	fsys_filerec_t rec;
	
	if(file_info(name, &rec) == 0) // Lookup directly
	{
		strncpy(buf, name, sizeof(buf));
	}
	else
	{
		strncpy(buf, name, sizeof(buf));
		strncat(buf, ".com", sizeof(buf));
		if(file_info(buf, &rec) != 0) // Lookup name +".com"
		{
			strncpy(buf, "0:/sys/", sizeof(buf));
			strncat(buf, name, sizeof(buf));			
			strncat(buf, ".com", sizeof(buf));			
			
			if(file_info(buf, &rec) != 0) // Lookup name +".com" in sys
			{
				strncpy(buf, "0:/bin/", sizeof(buf));
				strncat(buf, name, sizeof(buf));			
				strncat(buf, ".com", sizeof(buf));			
				
				if(file_info(buf, &rec) != 0) // Lookup name +".com" in bin
				{
					printf("Bad command or a file name\n");
					return;
				}
			}
		}
	}
	
	switch(progman_run(buf, args))
	{
	case PROGMAN_OK: 
		stdio_print("OK\n");
		break;
	case PROGMAN_IOERROR:
		stdio_print("IO Error \n");
		break;	
	case PROGMAN_BADFORMAT:
		stdio_print("Bad format \n");
		break;		
	case PROGMAN_BADBASEADDRESS: 
		stdio_print("Bad base address \n");
		break;			
	case PROGMAN_TOOFEWBYTES:
		stdio_print("Too few bytes\n");
		break;		
	default:
		stdio_print("Unknown error\n");
		break;		
	}
}

void command_run(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}	
	
	run(name, parse_pointer);
}

void command_mount(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}	
	
	int result = file_mount(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}
}

void command_rmount(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}	
	
	int result = file_umount(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}	
	
	result = file_mount(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}
}

void command_umount(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing name \n");
		return;
	}	
	
	int result = file_umount(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}
}

void command_cd(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing path \n");
		return;
	}		
	
	int result = file_setcwd(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}	
}

void command_pwd(tokenizer_func tokenizer)
{
	char buf[64];
	
	int result = file_getcwd(buf, sizeof(buf));
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}	
	
	printf("%s", buf);
}

void command_cdr(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing path \n");
		return;
	}		
	
	int result = file_setdrive(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}	
}

void command_mkdir(tokenizer_func tokenizer)
{
	char * name = tokenizer();
	
	if(name == 0)
	{
		stdio_print("error: missing path \n");
		return;
	}		
	
	int result = file_mkdir(name);
	
	if(result !=  FSYS_OK)
	{
		type_error(result);
		return;
	}	
}


void command_help(tokenizer_func tokenizer);

command_descriptor commands[] =
{
	{"help"   , "display help message", command_help},
	{"run"    , "run program file"    , command_run},	
	{"rhex"   , "receive intel hex and save it as binary", command_rhex},	
	{"cd"     , "set working dir"     , command_cd},	
	{"cdr"    , "set working drive"   , command_cdr},	
	{"pwd"    , "print working drive" , command_pwd},	
/*	
	{"_mkdir"  , "create directory"   , command_mkdir},	
	{"_dir"   , "list files"          , command_dir},
	{"_ls"    , "list files"          , command_dir},
	{"_del"    , "delete file"         , command_del},
	{"_rtxt"   , "write text file"     , command_rtxt},
	{"_hex2bin", "convert intel hex file to binary", command_hex2bin},
	{"_type"  , "print text file"     , command_type},
	{"_copy"   , "copy file"           , command_copy},
	{"_mount"  , "mount volume"       , command_mount},
	{"_umount" , "unmount volume"     , command_umount},
	{"_rmount" , "remount volume"     , command_rmount},	
*/	
	{0, 0, 0}
};

void command_help(tokenizer_func tokenizer)
{
	// lookup for command
	command_descriptor *command_ptr = commands;
	
	while(command_ptr->name)
	{
		stdio_print(command_ptr->name);
		stdio_print(" - ");
		stdio_print(command_ptr->help);
		stdio_print("\n");
		command_ptr++;
	}
}

char * tokenizer()
{
	return strtok_r(0, " ", &parse_pointer);
}

unsigned char readLine(char * buffer, unsigned char size)
{
	unsigned char i;
	char data;
	
	size --;
	buffer[size] = 0;
	
	i = 0;
	
	while(1)
	{
		data = stdio_getchar();

		if(data == 0xA || data == 0xD) // Enter
		{ 
		    //stdio_putchar(0xA);
			buffer[i] = 0;
			return i;
		}
		else if(data == 127) // Backspace
		{
			if (i > 0) 
			{
				//stdio_putchar(data);		
				i--;
			}
		}
		else
		{
			if(size == 0) continue;
			
			//stdio_putchar(data);
			buffer[i] = data;
			
			i++;
			size--;
		}
	}
}

void umain()
{
	while(1)
	{
		command_pwd(0);
		stdio_print(">");	
		
		readLine(inputBuffer, INPUT_BUFFER_LENGTH - 1);

		//stdio_putchar('\n');
		
		//stdio_getString(inputBuffer, INPUT_BUFFER_LENGTH, 0);
		
		char * token = strtok_r(inputBuffer, " ", &parse_pointer);
		
		// Empty command - just continue
		if(token == 0) continue;
		
		// lookup for command
		command_descriptor *command_ptr = commands;
		
		while(1)
		{
			if(command_ptr->name == 0)
			{
				run(token, parse_pointer);
				printf("\n");
				break;
			}
			
			if(!strcmp(token, command_ptr->name))
			{
				command_ptr->handler(tokenizer);
				printf("\n");
				break;
			}
			
			command_ptr++;
		}
	}
}