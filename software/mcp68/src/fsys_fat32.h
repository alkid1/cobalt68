#pragma once

#include "mod_fsys.h"
#include "ff.h"

#define FAT32FS_FILES 8
#define FAT32FS_DIRS  8
#define FAT32FS_FSS   2

typedef struct
{
	FATFS fs[FAT32FS_FSS];
	
	FIL files[FAT32FS_FILES];
	DIR dirs[FAT32FS_DIRS];
} fat32fs_t;

unsigned long fat32_initInstance(fat32fs_t* fs);
unsigned long fat32_handler(fat32fs_t *fs, unsigned long code, void* data);