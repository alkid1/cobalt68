#pragma once

#include "mod_progman.h"

typedef struct mem_block_s
{
	pid_t               owner;
	struct mem_block_s* next;
} mem_block_t;

typedef struct
{
	void* start;
	void* finish;
	
	mem_block_t* first;
} mem_arena_t;

mem_arena_t mem_kernelArena;
mem_arena_t mem_programArena;

void mem_initArema(mem_arena_t* arena, void* start, void *finish)
{
	// Init fields
	arena->start  = start;
	arena->finish = finish;
	arena->first  = start;
	
	// Make first block
	arena->first->owner = 0xff; // 0xff - block is free
	arena->first->next  = 0;
}

unsigned long mem_blockSize(mem_arena_t* arena, mem_block_t* block)
{
	if(block->next == 0)
	{
		return (unsigned long)arena->finish - (unsigned long)block - sizeof(mem_block_t);
	}
	else
	{
		return (unsigned long)block->next - (unsigned long)block - sizeof(mem_block_t);
	}
}

void* mem_alloc(mem_arena_t* arena, unsigned long size, pid_t owner)
{
	// 0. Roundup to scale of 2 bytes.
	size &= ~1;
	size |=  2;
	
	// 1. Lookup for appropriate block
	struct mem_block_s* block = arena->first;
	
	while(true)
	{
		if(block == 0) return 0; // Didn't find => can't allocate
		
		if(block->owner == 0xff && mem_blockSize(arena, block) >= size)
		{
			break; // found it, break the cycle
		}
		block = block->next;
	}
	
	// 2. Split block (only if more than 4 bytes netto 
	if((mem_blockSize(arena, block) - size) > (sizeof(mem_block_t)) + 4)
	{
		// locate new block;
		struct mem_block_s* new_block = block + sizeof(block) + size;
		
		new_block->owner = 0xff;
		new_block->next  = block->next;
		
		// insert block into chain
		block->next      = new_block;
	}
	
	block->owner = owner;
	return block + sizeof(mem_block_t);
}

void* mem_allocAt(mem_arena_t* arena, void *address, unsigned long size, pid_t owner)
{
	// 0. Roundup to scale of 2 bytes.
	size &= ~1;
	size |=  2;
	
	// 1. Lookup for appropriate block
	struct mem_block_s* block = arena->first;
	
	while(true)
	{
		if(block == 0) return 0; // Didn't find => can't allocate
		
		if(block->owner == 0xff && mem_blockSize(arena, block) >= size)
		{
			break; // found it, break the cycle
		}
		block = block->next;
	}
	
	// 2. Split block (only if more than 4 bytes netto 
	if((mem_blockSize(arena, block) - size) > (sizeof(mem_block_t)) + 4)
	{
		// locate new block;
		struct mem_block_s* new_block = block + sizeof(block) + size;
		
		new_block->owner = 0xff;
		new_block->next  = block->next;
		
		// insert block into chain
		block->next      = new_block;
	}
	
	block->owner = owner;
	return block + sizeof(mem_block_t);
}

void mem_free(mem_arena_t* arena, void *data)
{
	// TODO: Check block validity.
	// Find block 
	struct mem_block_s* prev  = 0;
	struct mem_block_s* block = arena->first;
	
	while(true)
	{
		if(block == 0) return; // no such block here
		if(block + sizeof(mem_block_t) == data) break; // found it
		
		prev = block;
		block = block->next;
	}
	
	// 1. Mark block as unused
	block->owner = 0xff;
	
	// 2. Check for merge condition
	if(prev != 0 && block->next != 0 && prev->owner == 0xff && block->next->owner == 0xff) // 3-way merge
	{
		prev->next = block->next->next;
	}
	else if(prev != 0 && prev->owner == 0xff) // prev-way merge
	{
		prev->next = block->next;
	}
	else if(block->next != 0 && block->next->owner == 0xff) // next-way merge
	{
		block->next = block->next->next;
	}
}