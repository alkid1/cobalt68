#pragma once

#include "plib.h"
#include "mod_fsys.h"

#define FSYS_FSYSIDMASK 		0x00FF0000
#define FSYS_MAX_FILESYSTEMS    4
#define FSYS_PREFIX_LENGTH      4

typedef unsigned long (*fsys_handler_t)(unsigned long code, void* instance, fsys_args_t* data);

typedef struct 
{
	char           prefix;
	fsys_handler_t handler;
	void *         instance;
} fsys_filesystem_t;

fsys_filesystem_t fsys_filesystems[FSYS_MAX_FILESYSTEMS];

void fsys_init()
{
	memset(fsys_filesystems, 0, sizeof(fsys_filesystems));
}

unsigned long fsys_mount(char prefix, fsys_handler_t handler, void * instance)
{
	for(int i = 0; i < FSYS_MAX_FILESYSTEMS; i++)
	{
		if(fsys_filesystems[i].handler == 0)
		{
			fsys_filesystems[i].prefix   = prefix;
			fsys_filesystems[i].handler  = handler;
			fsys_filesystems[i].instance = instance;
			return FSYS_OK;
		}
	}
	return FSYS_FSYSLIMIT;
}

fsys_filesystem_t* fsys_resolveByPrefix(fsys_args_t* args)
{
	if(args->data[1] != '/') return 0;
	
	for(int i = 0; i < FSYS_MAX_FILESYSTEMS; i++)
	{
		if(fsys_filesystems[i].handler != 0 && fsys_filesystems[i].prefix == args->data[0])
		{
			args->data = args->data + 2; // Remove prefix from path
			return &fsys_filesystems[i];
		}
	}
	return 0;	
}

fsys_filesystem_t* fsys_resolveByHandler(fsys_args_t* args)
{
	unsigned char prefix = (args->handler & FSYS_FSYSIDMASK) >> 16;
	
	for(int i = 0; i < FSYS_MAX_FILESYSTEMS; i++)
	{
		if(fsys_filesystems[i].handler != 0 && fsys_filesystems[i].prefix == prefix)
		{
			args->handler &= ~FSYS_FSYSIDMASK; // Undecorate handler
			return &fsys_filesystems[i];
		}
	}
	
	return 0;
}

unsigned long fsys_list(fsys_args_t *args)
{
	if(args->handler >= FSYS_MAX_FILESYSTEMS)
	{
		return FSYS_ARGUMENT; // Invalid tracking number
	}
	
	while(true)
	{
		unsigned long index = args->handler++;
		
		if(index == FSYS_MAX_FILESYSTEMS)
		{
			args->handler = FSYS_LIST_END;
			return FSYS_NOTFOUND;
		}
		
		if(fsys_filesystems[index].handler != 0)
		{
			fsys_filerec_t *rec = args->filerec;
			
			// copy data to structure
			rec->name[0] = fsys_filesystems[index].prefix;
			rec->name[1] = 0;
			
			rec->size  = 0;
			rec->flags = FSYS_SPECIAL;
			
			return FSYS_OK;
		}
	}
}

unsigned long fsys_handler(unsigned long code, void* data)
{
	fsys_args_t *args = (fsys_args_t*)data;
	fsys_filesystem_t* fsys;
	unsigned long result;
		
	switch(code)
	{
	case FSYS_LIST:		
		fsys = fsys_resolveByPrefix(args);
		if(fsys == 0) return fsys_list(args);
		
		result = fsys->handler(code, fsys->instance, args);
		
		return result;

	case FSYS_FORMAT:
	case FSYS_DELETE:		
	case FSYS_OPEN:
		// Resolve
		fsys = fsys_resolveByPrefix(args);
		if(fsys == 0) return FSYS_NOTFOUND;
		
		result = fsys->handler(code, fsys->instance, args);

		// Decorate returning handler
		args->handler |= fsys->prefix << 16;
		
		return result;

	case FSYS_READ:
	case FSYS_WRITE:
	case FSYS_SEEK:
	case FSYS_CLOSE:
		// Resolve
		fsys = fsys_resolveByHandler(args);
		if(fsys == 0) return FSYS_NOTFOUND;
	
		return fsys->handler(code, fsys->instance, args);
	
	default: 
		return RESULT_BADCOMMAND;
	}
	return 0;
}