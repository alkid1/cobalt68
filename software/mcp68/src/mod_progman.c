#include "plib.h"
#include "mod_progman_i.h"
#include "klib_fileio.h"

#define PROGMAN_NAMEMAX  8
#define PROGMAN_PROGSMAX 8
#define PROGMAN_PARAMMAX 256

#define PM_ACTIVE   1

void _launch_user_code();

// Proram control block
typedef struct
{
	char arguments[PROGMAN_PARAMMAX];
} program_pcb_t;

typedef struct
{
	char            flags;
	char            name[PROGMAN_NAMEMAX];
	program_pcb_t  *pcb;
} progman_prog_t;

progman_prog_t defaultProg;
program_pcb_t  defaultPcb;

progman_prog_t *currentProg = &defaultProg;

typedef struct 
{
	long signature;
	long base_address; 
	long entrypoint;
	long load_size;
	long memory_size;
	long reserved0;
	long reserved1;
	long reserved2;
} com0_header_t;

void _call_user_program(void* entrypoint);

unsigned long progman_file_run(progman_args_t* args)
{
	char * name = args->path;
	
	com0_header_t header;
	
	file_t file;
	
	FILE_RESULT result = kfile_open(name, 0, &file);
	
	if(result !=  FSYS_OK)
	{
		args->file_result = result;
		return PROGMAN_IOERROR;
	}
	
	unsigned long bytesRead;

	result = kfile_read(file, &header, sizeof(header), &bytesRead);
	
	if(result != FSYS_OK)
	{
		kfile_close(file);
		args->file_result = result;
		return PROGMAN_IOERROR;
	}
	
	if(bytesRead != sizeof(header))
	{
		kfile_close(file);
		return PROGMAN_BADFORMAT;	
	}
	
	if(header.signature != 0x434f4d30)
	{
		kfile_close(file);
		return PROGMAN_BADFORMAT;
	}
	
	if(header.base_address != 0x10000)
	{
		kfile_close(file);
		return PROGMAN_BADBASEADDRESS;		
	}
	
	result = kfile_read(file, (void *)0x10000, header.load_size, &bytesRead);
	
	if(result !=  FSYS_OK)
	{
		kfile_close(file);
		args->file_result = result;
		return PROGMAN_IOERROR;
	}
	
	if(bytesRead != header.load_size)
	{
		kfile_close(file);
		return PROGMAN_TOOFEWBYTES;	
	}	
	
	kfile_close(file);
	
	// Init data structures
	currentProg->pcb   = &defaultPcb;
	currentProg->flags = PM_ACTIVE;
	strncpy(currentProg->pcb->arguments, args->args, PROGMAN_PARAMMAX);
	
	_call_user_program((void *)header.entrypoint);
}

unsigned long progman_get_args(progman_args_t* args)
{
	strncpy(args->args, currentProg->pcb->arguments, args->size);
	return PROGMAN_OK;
}

unsigned long module_progman_handler(unsigned long code, void* data)
{
	progman_args_t *args = (progman_args_t*)data;
	switch(code)
	{
	case PROGMAN_FILE_RUN:
		return progman_file_run(args);
	case PROGMAN_TERMINATE:
		_launch_user_code();
	case PROGMAN_GET_ARGS:
		return progman_get_args(args);
	default: 
		return RESULT_BADCOMMAND;
	}
}