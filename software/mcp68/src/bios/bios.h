#pragma once

typedef unsigned int BRESULT;

BRESULT bf_initialize(int devid);

BRESULT bf_read(int devid, ...);
BRESULT bf_write(int devid, ...);

BRESULT bf_ioctl(int devid, ...);


/* 
	BIOS Functional Requirements:
	- UART
		- Put char
		- Get char
		- Test if has char
		- set speed
		- set frame: bit, start/stop, parity, flow control
	- BLOCK DEVICES
		- Init/Reset
		- Write Block(s)
		- Read Block(s)
		- Get size (block count)
		- Get block size
		- Check status
	- Keyboard
		- Get char/ascii mode
		- Get char/raw mode (scan codes)
		- Test if has char	
	- VDC Terminal
		- Init/Rest
		- Clear
		- Put char/terminal mode
		- Put char/raw mode
		- Set Position
		- Set Cursor
		- Get Cols 
		- Get Rows
*/
