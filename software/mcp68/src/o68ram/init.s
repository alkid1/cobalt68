/* 
	M68K monitor.
	
	Initialization module: IVT, power-on code, etc.
	
*/

.global _start
.global _trap0_handler
.global _launch_user_code
.global _call_user_program


.extern main
.extern _stack
.extern trap0_handler
.extern userStack
.extern umain
.extern kprintHex32
.extern _bss_start;
.extern _bss_end;
.extern address_exception
.extern exception_pc

.section .startup, "ax", @note
.align 2

.org 0xC
   .long _address_handler
   
.org 0x80
   .long _trap0_handler

_start:	
	// Init BSS
	move.l #_bss_start, %a0

_bss_loop:
	move.b #0         , (%a0)+
	cmp.l  #_bss_end  ,  %a0
	bne    _bss_loop

	move.l #_stack, %SP // Init stack
	jsr kmain           

_trap0_handler:
	move.l %d0, -(%sp)
	move.l %d1, -(%sp)
	
	jsr trap0_handler
	
	addql #8, %sp
	rte
	
_address_handler:
    move.l  10(%ssp), (exception_pc)
    jsr address_exception
		
_launch_user_code:
	move.l #_stack, %SP             // Discard kernel stack
	andi #1, %sr                    // Switch to user mode
	move.l #(userStack + 1024), %sp // init user stack
	jsr umain
	
	
_call_user_program: // (void* entryPoint)
	move.l 4(%sp), %a0  // set a0 = entryPoint
	move.l #_stack, %SP // Discard kernel stack
	andi #1, %sr        // Switch to user mode
	jmp  (%a0)          // jump to entry point (a0) 