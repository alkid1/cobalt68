#define __STDC_WANT_LIB_EXT1__ 1

#include "stdlib.h"
#include "string.h"
#include "lib_progman.h"

#include "hlib.h"

#define ARGS_MAX_SIZE   256
#define ARGS_TABLE_SIZE 16

extern int main(int argc, char *argv[]);

static char  arguments_buffer[ARGS_MAX_SIZE];
static char *argv[ARGS_TABLE_SIZE];

char *parse_pointer;

static inline char * tokenizer()
{
	return strtok_r(0, " ", &parse_pointer);
}

void _crtmain()
{
	progman_getargs(arguments_buffer, sizeof(arguments_buffer));
	
	// Put default argment
	int argc = 1;
	argv[0] = "program";
	
	char * token = strtok_r(arguments_buffer, " ", &parse_pointer);
	
	while(token)
	{
		if(argc == (ARGS_TABLE_SIZE - 1) || token == 0)
		{
			argv[argc] = 0;
			break;
		}
		
		argv[argc] = token;
		argc++;		
		
		token = tokenizer();
	}
	
	exit(main(argc, argv));
}