NAME = elf
CC = m68k-elf-gcc
SRC = src\main.c 
LSRC1 = $(shell dir ..\lib\src\*.c /B /S)
LSRC2 = $(shell dir ..\mcp68\lib\*.* /B /S)
LSRC3 = ..\mcp68\com\init.nostdlib.S
LINKERSCRIPT = ..\mcp68\com\link.ld
INC = -I..\lib\inc -I..\mcp68\inc 
PLATFORM = mcp68
CCOPTIONS = -nostdlib -ffreestanding -march=68000 -std=c99 -Wmissing-field-initializers -Wl,--emit-relocs -Wl,--gc-sections -fdata-sections -ffunction-sections	

app: 
	$(MAKE) clean
	$(MAKE) compile
	$(MAKE) export

compile:
	-mkdir out\$(PLATFORM) 
	m68k-elf-gcc $(SRC) $(SRC1) $(LSRC1) $(LSRC2) $(LSRC3) $(INC) -lgcc -Os -Wl,-Map,out\$(PLATFORM)\$(NAME).map -T $(LINKERSCRIPT) -DPLIB -D$(PLATFORM)  $(CCOPTIONS)

export:

	m68k-elf-objdump a.out -S -d >out\$(PLATFORM)\$(NAME).lst
	m68k-elf-objcopy a.out        out\$(PLATFORM)\$(NAME).com -O binary
	m68k-elf-objcopy a.out        out\$(PLATFORM)\$(NAME).s68 -O srec
	m68k-elf-objcopy a.out        out\$(PLATFORM)\$(NAME).hex -O ihex

clean:
	-del out\$(PLATFORM)\*.* /Q