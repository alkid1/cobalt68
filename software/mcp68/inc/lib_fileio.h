#pragma once

#ifndef FILE_PREFIX
	#define FILE_PREFIX
#endif

#ifndef FILE_SYSCALL
	#define FILE_SYSCALL _call_trap0
#endif

#define FILE_CAT(i, j) i ## j
#define FILE_EVAL(i, j) FILE_CAT(i, j)
#define FILE_FUNCID(name) FILE_EVAL(FILE_PREFIX, name)



#include "kernel.h"
#include "mcp68.h"
#include "mod_fsys.h"

// FILE_RESULT values
#define FSYS_OK         0
#define FSYS_BADINIT    1
#define FSYS_NOTFOUND   2
#define FSYS_NOFS       3
#define FSYS_IOERROR    4
#define FSYS_FILELIMIT  5
#define FSYS_TABLELIMIT 6
#define FSYS_ARGUMENT   7
#define FSYS_FILEINUSE  8
#define FSYS_FSYSLIMIT  9

#define FSYS_FILENAME_LENGTH 32

// File flags
#define FSYS_DIRECTORY 1    // File is directory
#define FSYS_STREAM    2    // File can't be seeked
#define FSYS_READABLE  4    // File can be read  
#define FSYS_WRITEABLE 8    // File can be written
#define FSYS_SPECIAL   16   // File is special device file.

// File open flags
#define FSYS_CREATE     1  // Create new file if not exist, open existing.
#define FSYS_CREATENEW  2  // Create new file if not exist, if exist - truncate all content 
#define FSYS_APPEND     4  // Move pointer to end of file to append

// Seek origins
#define FSYS_SEEK_BEGIN   0
#define FSYS_SEEK_CURRENT 1
#define FSYS_SEEK_END     2

typedef struct 
{
	char          name[FSYS_FILENAME_LENGTH];
	unsigned long flags;
	unsigned long size;
} fsys_filerec_t;

typedef struct 
{
	char         *data;
	unsigned int  handler;
	unsigned long size;
	union
	{
		unsigned long   offset;
		fsys_filerec_t *filerec;
		char           *data2;
	};
	unsigned long flags;
} fsys_args_t;


// Types
typedef unsigned long file_t;
typedef unsigned long FILE_RESULT;

// Functions
#include "lib_fileio_f.inc"

