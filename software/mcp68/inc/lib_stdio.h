#pragma once

#include "mod_stdio.h"
#include "kernel.h"

typedef unsigned long STDIO_RESULT;

void stdio_putchar(char c);
void stdio_putblock(const char *data, size_t size);
char stdio_getchar();
void stdio_print(const char *msg);
STDIO_RESULT stdio_getString(char* buffer, size_t size, size_t *bytesRead);
STDIO_RESULT stdio_getText(char* buffer, size_t size, size_t *readBytes);
STDIO_RESULT stdio_getConfiguration(stdio_configuration_t *data);
STDIO_RESULT stdio_setConfiguration(stdio_configuration_t *data);
