FILE_RESULT FILE_FUNCID(file_mount)(const char *name);
FILE_RESULT FILE_FUNCID(file_umount)(const char *name);

FILE_RESULT FILE_FUNCID(file_setcwd)(const char *name);
FILE_RESULT FILE_FUNCID(file_getcwd)(const char *name, unsigned int length);
FILE_RESULT FILE_FUNCID(file_mkdir)(const char *name);
FILE_RESULT FILE_FUNCID(file_setdrive)(const char *name);

FILE_RESULT FILE_FUNCID(file_truncate)(file_t file, size_t size);

FILE_RESULT FILE_FUNCID(file_info)(const char *name, fsys_filerec_t *rec);

FILE_RESULT FILE_FUNCID(file_format)(const char *name);
FILE_RESULT FILE_FUNCID(file_open)(const char *name, unsigned long flags, file_t* pFile);
FILE_RESULT FILE_FUNCID(file_close)(file_t file);
FILE_RESULT FILE_FUNCID(file_seek)(file_t file, size_t offset, int origin);
FILE_RESULT FILE_FUNCID(file_read)(file_t file, void *data, size_t size, size_t* bytesRead);
FILE_RESULT FILE_FUNCID(file_write)(file_t file, void *data, size_t size);
FILE_RESULT FILE_FUNCID(file_writeString)(file_t file, char * message);
FILE_RESULT FILE_FUNCID(file_delete)(const char *name);
FILE_RESULT FILE_FUNCID(file_list)(const char *path, fsys_filerec_t *filerec, unsigned long *tracker);
FILE_RESULT FILE_FUNCID(file_rename)(const char *oldpath, const char *newpath);