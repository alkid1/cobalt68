#pragma once

#include "mod_progman.h"

void progman_terminate();
unsigned int progman_run(const char *path, const char *args);
unsigned int progman_getargs(char *args, unsigned int size);
