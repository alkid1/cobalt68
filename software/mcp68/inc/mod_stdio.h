#pragma once
#define MODULE_STDIO_ID MODULE_ID(0)

#include "mcp68.h"

#define STDIO_OK     0
#define STDIO_EOL    1
#define STDIO_EOF    2
#define STDIO_EINVAL 3

#define NEWLINE_CODE 0xA

#define STDIO_CFG_ECHO     1   // Echo symols entered by user
#define STDIO_CFG_PPINPUT  2   // Preprocess input entered by user
#define STDIO_CFG_PPOUTPUT 4   // Preprocess output by application

typedef struct
{
	unsigned int size;    // Size of this structure
	unsigned int flags;   // Flags
} stdio_configuration_t;

typedef struct
{
	char        * data;
	unsigned long size;
	char          delimiter;
} stdio_arg_t;

/********** operations **************/
#define STDIO_PUTBLOCK  0 
/*
Description:
	Puts block of data to output. Blocking.
Parameters:
	data - pointer to data
	size - amount of bytes to send
Return:
*/

#define STDIO_PUTSTRING 1
/*
Description:
	Puts string to output - sends bytes unless size limit is reached or zero is encountered. Blocking.
Parameters:
	data - pointer to data
	size - amount of bytes to send
Return:
*/

#define STDIO_GETBLOCK  2
/*
Description:
	Read block of data. Blocking.
Parameters:
	data - pointer to data
	size - amount of bytes to send
Return:
*/

#define STDIO_GETSTRING 3
/*
Description:
	Read bytes until LF or CR is encountered or size limit is reached. End is marked with null
Parameters:
	data - pointer to data
	size - amount of bytes to read
Return:
	size - amount of bytes read
*/

#define STDIO_PUTCHAR  4 
/*
Description:
	Puts single char
Parameters:
	data - pointer to data
Return:
*/

#define STDIO_GETCHAR 5
/*
Description:
	Gets single char
Parameters:
	data - pointer to data
Return:
*/


#define STDIO_GETCONFIGURATION 6
/*
Description:
	Returns stdio configuration 
Parameters:
	data - pointer to stdio_configuration_t to fill with data
Return:
*/

#define STDIO_SETCONFIGURATION 7
/*
Description:
	Applies stdio configuration 
Parameters:
	data - pointer to stdio_configuration_t with settings to apply
Return:
*/