#pragma once

#define MODULE_MASK  0xFF000000
#define MODULE_ID(id) (id << 24)

typedef unsigned long size_t;

unsigned int _call_trap0(unsigned int, void*);