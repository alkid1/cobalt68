#pragma once

#define RESULT_OK           0
#define RESULT_BADMODULE    1 // Missing module  code is referenced
#define RESULT_BADCOMMAND   2 // Missing command code is referenced
#define RESULT_BADINIT      3 // Target device or module was not initialized properly (or there was error).
#define RESULT_GENERALERROR 4 // General error.
#define RESULT_UNSUPPORTED  5 // This command is not supported