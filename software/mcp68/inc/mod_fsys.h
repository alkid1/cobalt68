#pragma once

#include "lib_fileio.h"

#define MODULE_FSYS_ID MODULE_ID(1)


/*
	File operations:
	
	- init filesystem (format media)
	
	//// File operations
	- list        files
	- open/create file (new, for read/write, initial position (start, end))
	- close       file
	- seek        file
	- read        data
	- write       data
	- truncate    file 
	- get file data (size, position, attributes, etc)
	- delete file
	
	//// General operations
	- get FS DATA
*/



#define FSYS_FORMAT    0
/*
Description:
	Prepares file system structure on given media disregarding any existing data on it.
	
Parameters:
	data   - path to device
	
Return:
	RESULT_OK           - Media is formatted
	RESULT_BADINIT      - Media is not initialized
	RESULT_GENERALERROR - I/O error during operation
*/


// TODO : Rework LIST function entirely
#define FSYS_LIST         1 // TODO
#define FSYS_LIST_END     0xFFFFFFFF
#define FSYS_LIST_START  (0xFFFFFFFF - 1)
/*
Description:
	Lists files according to given mask
	
Parameters:
	handler - tracking number.
				if  FSYS_LIST_START -> start search
				if FSYS_LIST_END - last result is returned
				otherwise - continue search
	data    - pointer to path string
	filerec - pointer to receiving fsys_filerec_t
	
Return:
	handler - new tracker number
	offset  - for entries returned - size of file
	
	FSYS_NOTFOUND 
*/

#define FSYS_OPEN      2
/*
Description:
	Opens file for operations
	
Parameters:
	flags - file open flags
	
Return:
	handler - opened file handler
	
	FSYS_OK       
	FSYS_NOTFOUND 
	FSYS_IOERROR  
	FSYS_FILELIMIT
	FSYS_TABLELIMIT
*/

#define FSYS_CLOSE     3
/*
Description:
	Opens file for operations
	
Parameters:
	handler - file handler
*/

/*
	TODO: Need better model of file description
	AND file iteration. Search path and pattern, etc.
*/

#define FSYS_SEEK      4

/*
Description:
	Opens file for operations
	
Parameters:
	size   - seek origin
	offset - offset to use (treated as signed long).
*/

#define FSYS_READ      5
/*
Description:
	writes data to file.
	
Parameters:
	handler - FCB handler
	data    - pointer to data
	size    - size to write
	
Returns
	offset  - number of bytes actually read
*/

#define FSYS_WRITE     6
/*
Description:
	writes data to file.
	
Parameters:
	handler - FCB handler
	data    - pointer to data
	size    - size to write
*/

#define FSYS_FILEINFO  7

#define FSYS_TRUNC     8
/*
Description:
	truncate file
	
Parameters:
	handler - FCB handler
	size    - size to write
	
Remarks:

*/


#define FSYS_DELETE    9
/*
Description:
	delete file
	
Parameters:
	data    - pointer to file name.
	
Remarks:
	file can't be deleted if it is open by any program
*/

#define FSYS_INFO      10
/*
Description:
	Get file metadata by path.
	
Parameters:
	data    - path to file
	filerec - pointer to fsys_filerec_t to fill with data
	
Return:

*/


#define FSYS_MOUNT    11
/*
Description:
	(Un)mounts filesystem designated by string id.
	
Parameters:
	data    - string id
	handler - 0 - unmount
	          1 - mount
	
Return:

*/

#define FSYS_SETCWD 12
/*
Description:
	Sets current working directory.
	
Parameters:
	data    - pointer to path
*/


#define FSYS_GETCWD 13
/*
Description:
	Gets current working directory.
	
Parameters:
	data    - pointer to buffer
	size    - size of buffer
*/

#define FSYS_SETDRIVE 14
/*
Description:
	Sets current working drive.
	
Parameters:
	data    - pointer to drive name
*/

#define FSYS_MAKE_DIR 15
/*
Description:
	Creates directory
	
Parameters:
	data    - pointer to directory path
*/

#define FSYS_RENAME 16
/*
Description:
	Rename file/directory
	
Parameters:
	data    - pointer to old path
	data2   - pointer to new path
*/