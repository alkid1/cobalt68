#pragma once

#define MODULE_PROGMAN_ID MODULE_ID(2)

typedef unsigned char pid_t;

typedef struct 
{
	char * path;
	char * args;
	int    exit_code;
	union
	{
		int          file_result;
		unsigned int size; 
	};
} progman_args_t;


#define PROGMAN_OK             0
#define PROGMAN_IOERROR        1
#define PROGMAN_BADFORMAT      2
#define PROGMAN_BADBASEADDRESS 3
#define PROGMAN_TOOFEWBYTES    4

/* Program Manager functions */
#define PROGMAN_FILE_RUN 0
/*
	Run program from file
Parameters:
	path - path to file image
	args - arguments

Return value:	
	exit_code - exit code of program that was run.
*/

#define PROGMAN_TERMINATE 1
/*
	Terminates requesting program.

Parameters:
	exit_code - code to return

Return value: 
	this call never returns.
*/


#define PROGMAN_GET_ARGS 2
/*
	Returns arguments for requesting program

Parameters:
	args - pointer to buffer
	size - size of receiving buffer

Return value: 
*/

