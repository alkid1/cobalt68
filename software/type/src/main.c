#include "stdlib.h"
#include "string.h"
#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

int main(char argc, char *argv[])
{
	if(argc != 2)
	{
		stdio_print("error: missing name \n");
		return 1;
	}
	
	char *name = argv[1];
	
	file_t file;
	
	FILE_RESULT result = file_open(name, 0, &file); 
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}
	
	long read = 0;
	char c;
	
	result = file_read(file, &c, 1, &read);

	while(read)
	{
		if(result != FSYS_OK)
		{
			type_file_result(result);
			break;
		}
		stdio_putchar(c);
		
		result = file_read(file, &c, 1, &read);
	}
		
	result = file_close(file);	
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}	
	return 0;
}
