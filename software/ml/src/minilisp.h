#pragma once

//======================================================================
// Types 
//======================================================================
// The Lisp object type
enum {
    // Regular objects visible from the user
    TINT = 1,
	TSTRING,
    TCELL,
    TSYMBOL,
    TPRIMITIVE,
    TFUNCTION,
    TMACRO,
    TENV,
    // The marker that indicates the object has been moved to other location by GC. The new location
    // can be found at the forwarding pointer. Only the functions to do garbage collection set and
    // handle the object of this type. Other functions will never see the object of this type.
    TMOVED,
    // Const objects. They are statically allocated and will never be managed by GC.
    TTRUE,
    TNIL,
    TDOT,
    TCPAREN,
};

// Typedef for the primitive function
struct Obj;
typedef struct Obj *Primitive(void *root, struct Obj **env, struct Obj **args);

// The object type
typedef struct Obj {
    // The first word of the object represents the type of the object. Any code that handles object
    // needs to check its type first, then access the following union members.
    int type;

    // The total size of the object, including "type" field, this field, the contents, and the
    // padding at the end of the object.
    int size;

    // Object values.
    union {
        // Int
        int value;
        // Cell
        struct 
		{
            struct Obj *car;
            struct Obj *cdr;
        };
        // Symbol
        char name[1];
		char string[1];
        // Primitive
        Primitive *fn;
        // Function or Macro
        struct 
		{
            struct Obj *params;
            struct Obj *body;
            struct Obj *env;
        };
        // Environment frame. This is a linked list of association lists
        // containing the mapping from symbols to their value.
        struct 
		{
            struct Obj *vars;
            struct Obj *up;
        };
        // Forwarding pointer
        void *moved;
    };
} Obj;

//======================================================================
// Constants.
//======================================================================
extern Obj *True;
extern Obj *Nil;
extern Obj *Dot;
extern Obj *Cparen;

//======================================================================
// Infrastructure functions.
//======================================================================
void error(char *fmt, ...);
void add_primitive(void *root, Obj **env, char *name, Primitive *fn);
int length(Obj *list) ;
Obj *eval(void *root, Obj **env, Obj **obj);
Obj *eval_list(void *root, Obj **env, Obj **list);


//======================================================================
// Constructors.
//======================================================================
Obj *make_int(void *root, int value);
Obj *make_string(void *root, const char * value);
Obj *cons(void *root, Obj **car, Obj **cdr);
Obj *make_symbol(void *root, char *name);
Obj *make_primitive(void *root, Primitive *fn);
Obj *make_function(void *root, Obj **env, int type, Obj **params, Obj **body);
Obj *make_env(void *root, Obj **vars, Obj **up);
Obj *acons(void *root, Obj **x, Obj **y, Obj **a); // Returns ((x . y) . a)

// Modules
void define_primitives_math(void *root, Obj **env);
