#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include "hlib.h"

#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

extern char _free_area[];


char    *hex2binBffer = 0;
long int hex2binSize = 0;

char command_hex2bin_getchar()
{
	if(hex2binSize == 0)
	{
		return 0x1b; // Escape	
	}
	
	char c = *hex2binBffer;
	
	hex2binSize--;
	hex2binBffer++;

	return c;
}

int func_hex2bin(const char *sourceName, const char *destinationName)
{
	unsigned long count = 0;
	file_t sourceFile, destinationFile;
	
	hex2binBffer = (char *)0x20000;
	
	FILE_RESULT fresult = file_open(sourceName, 0, &sourceFile);
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
		return fresult;
	}
	
	fresult = file_read(sourceFile, hex2binBffer, 1024 * 128, &hex2binSize);
	
	hex2binSize += 1;
	
	if(fresult != FSYS_OK)
	{
		file_close(sourceFile);
		type_file_result(fresult);
		return fresult;
	}
	
	//kprint("Hex read OK, size: "); kprintHex32(hex2binSize); kprint("\n");
	printf("Hex read OK, size:%X \n", hex2binSize);
	
	int result = 0;
	
	// Read hex as binary into memory
	while(result >= 0)
	{
		result = readHexLine(_free_area, 128 * 1024, command_hex2bin_getchar);
		if(result >= 0) count += result;
	}
	file_close(sourceFile);

	//kprint("Final position: "); kprintHex32((unsigned int)hex2binBffer); kprint("\n");
	printf("Final position: %X\n", (unsigned int)hex2binBffer); 
	
	switch(result)
	{
	case RHL_EOF:
		stdio_print("Hex data read OK. \n");
		break;
	case RHL_BADFORMAT:
		stdio_print("Format error.\n");
		return 10;
	case RHL_BADCHECKSUM:
		stdio_print("Checksum error.\n");
		return 10;					
	case RHL_ABORT:
		stdio_print("Aborted.\n");
		return 10;
	case RHL_OVERFLOW:
		stdio_print("Overflow\n");
		return 10;
	default:
		stdio_print("Internal error\n");
		return 10;
	}	
	
	fresult = file_open(destinationName, FSYS_CREATE | FSYS_CREATENEW, &destinationFile);
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
		return fresult;
	}
	
	fresult = file_write(destinationFile, _free_area, count);
	
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
	}
	
	file_close(destinationFile);
	return FSYS_OK;
}

char *parse_pointer;

char * tokenizer()
{
	return strtok_r(0, " ", &parse_pointer);
}


int main(char argc, char *argv[])
{
	if(args != 3)
	{
		stdio_print("usage: hex2bin <source> <destination>\n");
		return;
	}	
	
	char * sourceName      = argv[1];
	char * destinationName = argv[2];
	func_hex2bin(sourceName, destinationName);	
}
