#include "plib.h"
#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

// TODO: Implement paged copy algorithm.
#define RHEX_BUF_SIZE (128 * 1024)

extern char _free_area[];

char *parse_pointer;

char * tokenizer()
{
	return strtok_r(0, " ", &parse_pointer);
}

int main(char argc, char *argv[])
{
	if(argc < 3)
	{
		stdio_print("usage: copy <source> <destination>\n");
		return 1;
	}		
	
	char * sourceName      = argv[1];
	char * destinationName = argv[2];
	
	file_t sourceFile, destinationFile;
	
	FILE_RESULT fresult = file_open(sourceName, 0, &sourceFile);
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
		return 1;
	}
	
	unsigned long bytes_read;
	
	file_read(sourceFile, _free_area, RHEX_BUF_SIZE, &bytes_read);
	file_close(sourceFile);
	
	fresult = file_open(destinationName, FSYS_CREATE | FSYS_CREATENEW, &destinationFile);
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
		return 1;
	}
	
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
	}
	
	fresult = file_write(destinationFile, _free_area, bytes_read);
	
	if(fresult != FSYS_OK)
	{
		type_file_result(fresult);
	}
	
	file_close(destinationFile);	
	
	return 0;
}
