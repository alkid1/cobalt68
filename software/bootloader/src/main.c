#include "m68-hal.h"

void _putchar(char c)
{
	console_putchar(c);
}

#include "digits.c"
#include "lookupDigit.c"
#include "kprint.c"
#include "printf.c"
#include "spi.c"
#include "spi_io.c"
#include "sd_io.c"
#include "diskio.c"
#include "ff.c"

FATFS   fs;
FIL     file;
FILINFO fileinfo;

char * lookup[] = 
{
	"0:/sys/mcp68.sys\0",
	"0:/mcp68.sys\0",
	0
};

void wait()
{
	printf("Halting."NEWLINE);
	while(1);
}

void main()
{
	int res;
	char * path;
	
	printf("MCP68 Bootloader."NEWLINE);
	
	res = f_mount(&fs, "0:", 1);
	
	if(res != 0)
	{
		printf("Mount error: %d"NEWLINE, res);
		wait();
	}
	
	int i = 0;	
	
	while(lookup[i] != 0)
	{
		path = lookup[i];
		
		printf("Looking for OS at %s"NEWLINE, path);
		res = f_stat (path, &fileinfo);
		
		if(res == 0) break;
		i++;
	}	
	
	if(path == 0)
	{
		printf("OS Image not found."NEWLINE);
	}
	
	printf("OS Image found, size: %d bytes"NEWLINE, fileinfo.fsize);
	
	res = f_open(&file, path, FA_OPEN_EXISTING | FA_READ);
	if(res != 0)
	{
		printf("Open file error: %d"NEWLINE, res);
		wait();
	}		
	
	UINT bytesRead;
	
	printf("Reading."NEWLINE);

	res = f_read(&file, (void*)0, fileinfo.fsize, &bytesRead);

	if(res != 0)
	{
		printf("Read file error: %d"NEWLINE, res);
		wait();
	}			
	
	if(bytesRead != fileinfo.fsize)
	{
		printf("Failed to read whole fize: %d"NEWLINE);
		wait();
	}			
		
	printf("File read OK, starting operating system."NEWLINE);
	
	void (*ptr)(void) = (void (*)(void))0;
	ptr();
}
