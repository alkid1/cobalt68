#ifndef m68hal1
#define m68hal1

#define UART_PORT (0xFF800)
#define BOOT_PORT (UART_PORT + 1)
#define TEST_PORT (UART_PORT + 2)	

#define RAM_START 0x00000	
#define ROM_START 0xE0000	

#endif