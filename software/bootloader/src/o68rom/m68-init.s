/* 
	M68K monitor.
	
	Initialization module: IVT, power-on code, etc.
	
*/

.global _start
.global _version

#include "m68-hal.h"


.extern main
.extern _stack

.extern _ram_data_start
.extern _ram_data_size
.extern _ram_data_vstart

.section .startup, "ax", @note
.align 2
	.long _stack
	.long _start

_start:	
	move.b #1, (BOOT_PORT)  /* Disable boot signal */
	move.l #_stack, %SP /* is it needed? */

	/* init data in RAM from ROM image */
	move.l #_ram_data_size,   %d0
	move.l #_ram_data_start,  %a0
	move.l #_ram_data_vstart, %a1

_data_copy_loop:
	move.b (%a0)+, (%a1)+
	sub.l  #1, %d0
	cmpi.l #0, %d0
	bne    _data_copy_loop
	
	jsr main
	
_version:
	.string "ROM-Based"