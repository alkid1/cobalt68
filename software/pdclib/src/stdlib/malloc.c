/* Include the sbrk function */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

extern char _free_area[];
extern char _end_of_ram[];

char has_initialized = 0;
void *managed_memory_start = &_free_area [0];
void *last_valid_address   = &_end_of_ram[0];

typedef struct malloc_block_s 
{
	struct malloc_block_s *prev;
	struct malloc_block_s *next;
	unsigned int    info; /* bits 0-30 - size, bit 31 - used */
} malloc_block_t;

#define MALLOC_USED (1 << 31)
#define MALLOC_SIZE (0xFFFFFFFF & (~MALLOC_USED))

malloc_block_t* malloc_rootBlock = (malloc_block_t*)_free_area;

void malloc_init() 
{
	malloc_rootBlock = (malloc_block_t*)managed_memory_start;
	malloc_rootBlock->prev = 0;
	malloc_rootBlock->next = 0;
	malloc_rootBlock->info = last_valid_address - managed_memory_start - sizeof(malloc_block_t);
	has_initialized = 1;
} 

void* realloc(void *ptr, size_t numbytes)
{
/* 
     Improvement plan
	 Scenarions:
	 1. Grow
	 1.1. Next block is free and big enought
	 1.1.1. Next block is big enought to be splitted -> split.
	 1.1.2. Next block is to small to be splitted -> merge next block with used one.
	 1.2. Next block is used or too small -> malloc, copy, free.
	 2. Shrink
	 2.1. Next block is used
	 2.1.1. Delta is too small to split block -> do nothing.
	 2.1.2. Delta is big enought split block  -> split block.
	 2.2. Next block is free -> grow next free block 
*/	
	
	void * result = malloc(numbytes);
	
	memcpy(result, ptr, numbytes);
	
	free(ptr);
	
	return result;
}

void* calloc (size_t nelem, size_t elsize)
{
  void * ptr;  

  if (nelem == 0 || elsize == 0)
    nelem = elsize = 1;
 
  ptr = malloc (nelem * elsize);
  
  if (ptr) memset(ptr, 0, nelem * elsize);
  
  return ptr;
}


void *malloc(size_t numbytes) 
{
	if(!has_initialized)
	{
		malloc_init();
	}
	
	// roundup numbytes to times of 4
	if(numbytes & 0x3 != 0)
	{
		numbytes &= ~0x3;
		numbytes += 4;
	}
	
	malloc_block_t * p = malloc_rootBlock;
	unsigned int numbytesgross = numbytes + sizeof(malloc_block_t);
	
	while(p != 0)
	{
		unsigned int block_size = p->info & MALLOC_SIZE;
		
		if(((p->info & MALLOC_USED) == 0) && (block_size >= numbytesgross))
		{
			// split block only if there is at least 4 bytes net left in heap
			if((block_size - numbytesgross) >= (sizeof(malloc_block_t) + 4)) 
			{
				// allocate next block
				malloc_block_t *newblock = (malloc_block_t*)(((char*)p) + sizeof(malloc_block_t) + numbytes);
				
				newblock->info = 0;

				// init next block
				newblock->info = block_size - numbytesgross;
				newblock->prev = p;
				
				newblock->next = p->next;
				
			// insert new block 
				p->next = newblock;
				
			
				// fixup p block size
				p->info = numbytes;
				
			}
			
			p->info |= MALLOC_USED;
			
		
			return (char *)p  + sizeof(malloc_block_t);
		}
		
		p = p->next;
	}
	
	return 0;
}

void free(void *firstbyte) 
{
	if(firstbyte == 0) return;
	
	malloc_block_t* block = (malloc_block_t*)(firstbyte - sizeof(malloc_block_t));
	
	// Clear used flag
	block->info &= ~MALLOC_USED;
	if((block->prev != 0 && ((block->prev->info & MALLOC_USED) == 0)) &&(block->next != 0 && ((block->next->info & MALLOC_USED) == 0))) // 2-way merge
	{
		unsigned int block_size = block->info + block->prev->info + block->next->info;
		
		block->prev->info = block_size;        // Fixup size
		block->prev->next = block->next->next; // skip over block and block->next
	}
	else if(block->prev != 0 && ((block->prev->info & MALLOC_USED) == 0)) // prev merge
	{
		unsigned int block_size = block->info + block->prev->info;
		
		block->prev->info = block_size;  // Fixup size
		block->prev->next = block->next; // skip over block 
	}
	else if(block->next != 0 && ((block->next->info & MALLOC_USED) == 0)) // next merge
	{
		unsigned int block_size = block->info + block->next->info;
		
		block->info = block_size;        // Fixup size
		block->next = block->next->next; // and block->next
	}
}

void malloc_debug_print()
{
	printf("---\n");
	malloc_block_t * p = malloc_rootBlock;

	while(p != 0)
	{
		printf(
			"Block: size = %d, %s\n",
			p->info & MALLOC_SIZE,
			p->info & MALLOC_USED ? "used" : "free");
		
		p = p->next;
	}
}
