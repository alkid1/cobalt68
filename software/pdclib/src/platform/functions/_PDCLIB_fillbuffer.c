/* _PDCLIB_fillbuffer( struct _PDCLIB_file_t * )

   This file is part of the Public Domain C Library (PDCLib).
   Permission is granted to use, modify, and / or redistribute at will.
*/

/* This is an example implementation of _PDCLIB_fillbuffer() fit for
   use with POSIX kernels.
*/

#include <stdio.h>
#include "pdclib/_PDCLIB_glue.h"

#ifndef REGTEST

#include "lib_fileio.h"
#include "lib_stdio.h"

int _PDCLIB_fillbuffer( struct _PDCLIB_file_t * stream )
{
	size_t rc;
	int result;
	
    /* No need to handle buffers > INT_MAX, as PDCLib doesn't allow them */
	if(stream->handle == FILE_STDIN)
	{
		result = stdio_getString(stream->buffer, stream->bufsize - 1, &rc);
		stream->buffer[rc] = '\n'; // get string swallows delimiter 0xA, so insert it manually into buffer
		rc++;
	}
	else
	{
		result = file_read(stream->handle, stream->buffer, stream->bufsize, &rc);
	}
     
    if ( result == STDIO_OK || result == STDIO_EOL )
    {
        /* Reading successful. */
        if ( ! ( stream->status & _PDCLIB_FBIN ) )
        {
            /* TODO: Text stream conversion here */
        }
        stream->pos.offset += rc;
        stream->bufend = rc;
        stream->bufidx = 0;
		
		if(rc == 0)
		{
			return EOF;
		}
		
        return 0;
    }
	else
    {
        /* The 1:1 mapping done in _PDCLIB_config.h ensures
           this works.
        */
        // *_PDCLIB_errno_func() = errno;
        /* Flag the stream */
        stream->status |= _PDCLIB_ERRORFLAG;
        return EOF;
    }
    /* End-of-File */

}

#endif

#ifdef TEST

#include "_PDCLIB_test.h"

int main( void )
{
    /* Testing covered by ftell.c */
    return TEST_RESULTS;
}

#endif
