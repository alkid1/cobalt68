/* islower( int )

   This file is part of the Public Domain C Library (PDCLib).
   Permission is granted to use, modify, and / or redistribute at will.
*/

#include <ctype.h>

#ifndef REGTEST

#include <locale.h>

int islower( int c )
{
#ifdef PDCLIB_CTYPE_NOTBL
	return c >= 'a' && c <= 'z';
#else			
    return ( _PDCLIB_lc_ctype.entry[c].flags & _PDCLIB_CTYPE_LOWER );
#endif
}

#endif

#ifdef TEST

#include "_PDCLIB_test.h"

int main( void )
{
    TESTCASE( islower( 'a' ) );
    TESTCASE( islower( 'z' ) );
    TESTCASE( ! islower( 'A' ) );
    TESTCASE( ! islower( 'Z' ) );
    TESTCASE( ! islower( ' ' ) );
    TESTCASE( ! islower( '@' ) );
    return TEST_RESULTS;
}

#endif
