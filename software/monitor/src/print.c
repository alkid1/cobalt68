#pragma once

#include "plib.h"

#define MC6845_AR *((char*)0xFF000)
#define MC6845_DR *((char*)0xFF001)
#define BLANK_CHAR 0

static char cursor_col = 0;
static char cursor_row = 0;
static char vdc_echo   = 0;
static char* vram = (char *)0xF0000;

void print(const char *);

void cmdClearVram(char* inputBuffer)
{
	char* ptr = (char *)VRAM_ADDR;
	
	for(int i = 0; i < VRAM_SIZE;i++)
	{
		*ptr++ = BLANK_CHAR;
	}	
	
	cursor_col = 0;
	cursor_row = 0;
}

void cmdVdcEcho(char* inputBuffer)
{
	vdc_echo = !vdc_echo;
	
	print("Echo ");
	print(vdc_echo ? "on" : "off");
	print(NEWLINE);
}

void write6845(char address, char value)
{
	MC6845_AR = address;
	MC6845_DR = value;
}

void cmdInit6845(char* inputBuffer)
{
	write6845(0,99);    //H total chars
	write6845(1,80);    //H displayed chars
	write6845(2,83);    //Hsync position
	write6845(3,0x2c);  //sync width v 4 bit h 4 bit
	write6845(4,64);    //vtotal rows
	write6845(5,6);     //vtotal adjust
	write6845(6,60);    //vtotal disp
	write6845(7,61);    //vsync position
	write6845(8,0);     //interlace mode
	write6845(9,7);     //rows per char
	write6845(10,0);    //cursor row start
	write6845(11,7);    //cursor row end
	write6845(12,0);    //screen one start h byte
	write6845(13,0);    //screen one start l byte
	write6845(14,0);    //cursor one address h byte
	write6845(15,0);    //cursor one address l byte
}

void putcharVRAMNewLine()
{
	cursor_row++;
	
	// if last row - scroll
	if(cursor_row == 50)
	{
		cursor_row = 49;
		
		for(int i = 0; i < 80 * 49; i++)
		{
			*(vram + i) = *(vram + 80 + i);
		}
	}
	
	// clear new line.
	for(int i = 0; i < 80; i++)
	{
		*(vram + (cursor_row * 80 + i)) = BLANK_CHAR;
	}
}

void putcharVRAM(char c)
{
	if(c == 13 || c == 10)
	{
		cursor_col = 0;
		putcharVRAMNewLine();
	}
	else
	{
		*(vram + (cursor_row * 80 + cursor_col)) = c;
		cursor_col++;
		
		if(cursor_col >= 80)
		{
			cursor_col = 0;
			putcharVRAMNewLine();
		}
	}
}

void putchar(char c)
{
	if(vdc_echo)
	{	
		putcharVRAM(c);
	}
	console_putchar(c);
}

char getchar()
{
	// while(!console_ischar());
	return console_getchar();
}

void print(const char *msg)
{
	while(*msg) putchar(*(msg++));
}

void printHexNibble(char data)
{
	putchar(digitsTable[data & 0xF]);
}

void printHex(char data)
{
	printHexNibble(data >> 4);
	printHexNibble(data);
}

void printHex16(unsigned short int data)
{
	printHex(data >> 8);
	printHex(data & 0xFF);
}

void printHex32(unsigned long data)
{
	printHex16(data >> 16);
	printHex16(data & 0xFFFF);
}