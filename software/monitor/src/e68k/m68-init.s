/* 
	M68K monitor.
	
	Initialization module: IVT, power-on code, etc.
	
*/

.global _start
.global _version

.extern main
.extern _stack

.section .startup, "ax", @note
.align 2
/*
	.long _stack
	.long _start
*/	
_start:
	move.l #_stack, %SP
	
	move.l #_body_start_  , %A0
	move.l #_body_location, %A1
	
.Lcopy_loop:
	move.b (%A0)+, (%A1)+
	cmpa.l #_body_end_, %A0
	bne    .Lcopy_loop
	
	jsr main
	
_version:
	.string "E68K-Based"
	