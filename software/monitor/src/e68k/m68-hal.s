/* 
	M68K monitor.
	
	Hardware abstration layer. 
	Easy68K simulator implementation
*/

.text
.align 2

.global console_putchar
.global console_getchar
.global console_ischar
	

console_putchar:
	link    %FP, #0
	movem.l %d0-%d1, -(%SP)
	
	move.l 8(%FP), %D1
	move.l #6    , %D0
	trap   #15
	
	movem.l (%SP)+, %d0-%d1 
	unlk    %FP
	rts
	
console_getchar:
	link    %FP, #0
	move.l  %d1, -(%SP)
	
	move.l #5, %D0
	trap   #15
	move.l %D1, %D0
	
	move.l  (%SP)+, %d1
	unlk    %FP
	rts

		