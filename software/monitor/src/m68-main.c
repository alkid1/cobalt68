/* 
	M68K monitor.
	
	Main module
*/

#include "m68-hal.h"

#define bit(n) (1 << n)
#define seg_a bit(0)
#define seg_b bit(1)
#define seg_c bit(2)
#define seg_d bit(3)
#define seg_e bit(4)
#define seg_f bit(5)
#define seg_g bit(6)
#define seg_p bit(7)

#define INPUT_BUFFER_SIZE 64
#define DATA_BUFFER_SIZE 65536

#define VRAM_SIZE 4096
#define VRAM_ADDR 0xF0000

#include "print.c"
#include "readLine.c"
#include "strcmp.c"
#include "dumpMemory.c"
#include "parseInt.c"
#include "spi.c"
#include "sd.c"
#include "ihex_read.c"

#undef SD_OK

#include "spi_io.c"
#include "sd_io.c"


char inputBuffer[INPUT_BUFFER_SIZE] = "TEST_DATA";
char* data_buffer = 0;

void cmdMemoryDump(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		dumpMemory((char*)parseResult.value, 16);
	}
}

void cmdMemoryWrite(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		unsigned long ptr = parseResult.value;
		while(1)
		{
			printHex32(ptr); print(">");
			readLine(inputBuffer, INPUT_BUFFER_SIZE);
			if(inputBuffer[0] == 0) return; // empty line -> end of write
			
			parseResult = parseInt(inputBuffer);
			if(!parseResult.success)
			{
				print("Syntax error"NEWLINE);
			}
			else if(parseResult.value > 0xFF)
			{
				print("Byte overflow"NEWLINE);
			}
			else
			{
				*((unsigned char*)ptr) = (unsigned char)parseResult.value;
				ptr++;
			}
		}
	}
}

void cmdWriteString(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		print("S>");
		
		char* target = (char*)parseResult.value;
		
		while(1)
		{
			char c = getchar();
			
			putchar(c);
			
			if(c == 27) break;
			
			*target++ = c;
		}
		
		print(NEWLINE);
	}
}


void cmdReadHex(char* inputBuffer)
{
	unsigned long count = 0;
	
	char* used_data_buffer = data_buffer;
	
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(inputBuffer[1] != 0)
	{
		if(!parseResult.success)
		{
			print("Syntax error"NEWLINE);
		}
		else
		{
			used_data_buffer = (char*)parseResult.value;
			print("Buffer location redefined:"); printHex32(parseResult.value);print(NEWLINE);
		}
	}	
	
	print("Awaiting Intel-HEX input"NEWLINE);
	
	while(1)
	{
		int result = readHexLine(data_buffer, DATA_BUFFER_SIZE, getchar);
		if(result >= 0) count += result;
		
		if(result < 0)
		{
			switch(result)
			{
			case RHL_EOF:
				printHex16(count); print("h bytes received."NEWLINE);
				return;
			case RHL_BADFORMAT:
				print("Format error."NEWLINE);
				return;
 			case RHL_BADCHECKSUM:
				print("Checksum error."NEWLINE);
				return;					
 			case RHL_ABORT:
				print("Aborted."NEWLINE);
				return;
 			case RHL_OVERFLOW:
				print("Overflow"NEWLINE);
				return;
			default:
				print("Internal error"NEWLINE);
				return;
			}
		}
	}
}

extern int  _data_start;
extern int  _data_end;
extern int  _code_start;
extern int  _code_end;
extern int  _stack_start;
extern int  _stack_end;
extern char _version;

void cmdInfo(char* inputBuffer)
{
	print(&_version); print(NEWLINE);	
	print(" CODE: "); printHex32((int)&_code_start);  print(" - "); printHex32((int)&_code_end); print(NEWLINE);	
	print(" DATA: "); printHex32((int)&_data_start);  print(" - "); printHex32((int)&_data_end); print(NEWLINE);
	print("STACK: "); printHex32((int)&_stack_start); print(" - "); printHex32((int)&_stack_end); print(NEWLINE);
	print("BUFFER:"); printHex32((int)data_buffer);   print(" - "); printHex32((int)(data_buffer + DATA_BUFFER_SIZE)); print(NEWLINE);
	print("VRAM  :"); printHex32((int)VRAM_ADDR);     print(" - "); printHex32((int)(VRAM_ADDR + VRAM_SIZE)); print(NEWLINE);
}

void cmdInput(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
		return;
	}
	
	char data = *((char*)parseResult.value);
	
	printHex(data);
	print(NEWLINE);
}

void cmdOutput(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
		return;
	}
	
	unsigned long ptr = parseResult.value;
	
	while(true)
	{
		printHex32(ptr); print(">");
		readLine(inputBuffer, INPUT_BUFFER_SIZE);
		if(inputBuffer[0] == 0) return; // empty line -> end of write
		
		parseResult = parseInt(inputBuffer);
		if(!parseResult.success)
		{
			print("Syntax error"NEWLINE);
		}
		else if(parseResult.value > 0xFF)
		{
			print("Byte overflow"NEWLINE);
		}
		else
		{
			*((unsigned char*)ptr) = (unsigned char)parseResult.value;
		}	
	}
}

void cmdExecute(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		void (*ptr)(void) = (void (*)(void))parseResult.value;
		ptr();
		print(NEWLINE);
	}
}

void cmdInitSD(char* inputBuffer)
{
	print("SD Card initialization" NEWLINE);
	
	printHex(sd_initialize(0));
}

SD_DEV dev;

void cmdInitSDHC(char* inputBuffer)
{
	print("SDHC Card initialization" NEWLINE);
	printHex(SD_Init(&dev, 0));
}

void cmdWriteSPI(char* inputBuffer)
{
	spi_selectSlave(0);
	
	while(true)
	{
		print("SPI>");
		readLine(inputBuffer, INPUT_BUFFER_SIZE);
		if(inputBuffer[0] == 0) 
		{
			spi_unselectSlave();
			return; // empty line -> end of write
		}
		
		struct UIntParseResult parseResult = parseInt(inputBuffer);
		if(!parseResult.success)
		{
			print("Syntax error"NEWLINE);
		}
		else if(parseResult.value > 0xFF)
		{
			print("Byte overflow"NEWLINE);
		}
		else
		{
			printHex(spi_sendByte(parseResult.value));
			print(NEWLINE);
		}	
	}
}

void cmdSPITraceToggle(char* inputBuffer)
{
	spi_trace = !spi_trace;
	
	if(spi_trace)
	{
		print("SPI TRON\n");
	}
	else
	{
		print("SPI TROFF\n");
	}
}

void cmdSDGetStatus(char* inputBuffer)
{
	spi_selectSlave(0);
	
	sd_result result = sd_getStatus();
	
	if(result.code != SD_OK)
	{
		kprint("SD COMMAND ERROR: ");
		kprintHex(result.code);
		kprint("\n");
	}
	else
	{
		printHex16((result.r1 << 8) | result.r2);
		print("\n");	
	}
	spi_unselectSlave();
}

void cmdSDWriteBlock(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		sd_writeBlock(0, parseResult.value ,0, 512);
		print("DONE"NEWLINE);
	}
}

void cmdSDReadBlock(char* inputBuffer)
{
	struct UIntParseResult parseResult = parseInt(inputBuffer + 1);
	
	if(!parseResult.success)
	{
		print("Syntax error"NEWLINE);
	}
	else
	{
		sd_readBlock(0, parseResult.value, 0, 512);
		print("DONE"NEWLINE);
	}
}

typedef void (*CommandHandler)(char*);

struct CommandHandlerEntry
{
	CommandHandler handler;
	char           prefix;
	const char*    helpString;
};

void cmdHelp(char *);


struct CommandHandlerEntry entires[] = 
{
	{cmdMemoryDump, 'd', "dump memory contents"},
	{cmdHelp, 'h', "print this help message"},
	{cmdInfo, 'i', "print system info"},
	{cmdReadHex, 'r', "read IHEX from console"},
	{cmdMemoryWrite, 'D', "set memory contents"},
	{cmdWriteString, 'S', "Put string in memory"},
	{cmdExecute, 'E', "jump to address"},
	{cmdInput, 'o', "input one byte"},
	{cmdOutput, 'O', "output one byte"},
	{cmdClearVram, 'V', "clear VRAM (see info)"},
	// {cmdInit6845, '', "Init 6845 registers"},
	{cmdVdcEcho,  '1', "Toggle VDC echo"},
	{cmdInitSD,   '2', "Init SD Card"},
	{cmdInitSDHC,   '8', "Init SDHC Card"},
	{cmdSDWriteBlock,'3', "Block write SD"},
	{cmdSDReadBlock, '4', "Block read SD"},
	{cmdWriteSPI, '5', "Write to SPI"},
	{cmdSPITraceToggle, '6', "SPI Trace toggle"},
	{cmdSDGetStatus, '7', "SPI CMD13 (get status)"},
	{0, 0, 0}
};

void cmdHelp(char *inputBuffer)
{
	for(struct CommandHandlerEntry* ptr = entires; ptr->handler != 0; ptr++)
	{
		putchar(ptr->prefix);
		print(" - ");
		print(ptr->helpString);
		print(NEWLINE);
	}
}

void main()
{
	print("M68 Monitor."NEWLINE);
	print(&_version); print(NEWLINE);	
	print("Type 'h' for help message"NEWLINE);
	
	while(1)
	{
newLoop:	
		print(">");
		readLine(inputBuffer, INPUT_BUFFER_SIZE);
		print(inputBuffer);
		print(NEWLINE);
		
		if(inputBuffer[0] == 0) continue;
		
		for(struct CommandHandlerEntry* ptr = entires; ptr->handler != 0; ptr++)
		{
			if(inputBuffer[0] == ptr->prefix) 
			{
				ptr->handler(inputBuffer);
				goto newLoop;
			}
		}

		print("Unknown command!"NEWLINE);
	}
}


