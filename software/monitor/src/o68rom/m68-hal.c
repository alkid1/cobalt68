/*
	Onyx68-gamma
*/

#include "m68-hal.h"

void console_putchar(char c)
{
	(*((char*)UART_PORT)) = c;
}

char console_getchar()
{
	return (*((char*)UART_PORT));
}
