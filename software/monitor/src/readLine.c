#pragma once

#include "m68-hal.h"

unsigned char readLine(char * buffer, unsigned char size)
{
	unsigned char i;
	char data;
	
	size --;
	buffer[size] = 0;
	
	i = 0;
	
	while(1)
	{
		data = getchar();
		
		putchar(data);

		if(data == 0xA || data == 0xD)
		{
			buffer[i] = 0;
			return i;
		}
		
		buffer[i] = data;
		
		i++;
		size--;
		
		if(size == 0)
		{
			return i;
		}
	}
}