#pragma once

void dumpMemory(char *ptr, int size)
{
	unsigned int i, j;
	char d;

	// Print header
	print("          ");
	for(i = 0; i < 16; i++)
	{
		printHex(i);
		print(" ");
	}	
	print(NEWLINE);
	
	// Print data
	for(i = 0; i < size; i++)
	{
		printHex32((unsigned long)ptr);
		print(": ");
		
		// print hexadecimals
		for(j = 0; j < 16; j++)
		{
			printHex(*(ptr + j));
			print(" ");
		}
		
		// print ascii
		for(j = 0; j < 16; j++)
		{
			d = *(ptr + j);
			if(d > 31 && d != 0xFF)
			{
				putchar(d);
			}
			else
			{
				putchar('.');
			}
		}
		
		ptr += 16;
		
		print(NEWLINE);
	}
}