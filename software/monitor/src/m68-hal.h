#ifndef M68HAL_INCLUDED
#define M68HAL_INCLUDED

#define bool  char
#define true  1
#define false 0

extern void console_putchar(char);
extern char console_getchar();

#ifdef e68k
#define NEWLINE "\n\r"
#endif

#ifdef o68g
#define NEWLINE "\n"
#endif

#ifndef NEWLINE
#define NEWLINE "\n\r"
#endif

#endif