/* picoc main program - this varies depending on your operating system and
 * how you're using picoc */
 
/* include only picoc.h here - should be able to use it with only the external interfaces, no internals from interpreter.h */
#include "picoc.h"

/* platform-dependent code for running programs is in this file */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "lib_progman.h"

#define PICOC_STACK_SIZE (32*1024)              /* space for the the stack */

Picoc pc;

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("Format: picoc <csource.c>\n");
		return 0;
	}
	
    int ParamCount = 1;
    int DontRunMain = FALSE;
    int StackSize = PICOC_STACK_SIZE;
  
    PicocInitialise(&pc, StackSize);

/*	
	if (PicocPlatformSetExitPoint(&pc))
	{
		PicocCleanup(&pc);
		return pc.PicocExitValue;
	}
*/	
	PicocPlatformScanFile(&pc, argv[1]);
	PicocCallMain(&pc, 0, 0);
    PicocCleanup(&pc);
    return pc.PicocExitValue;
}
