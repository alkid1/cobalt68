set platform=%1
set name=app

call clean %1

m68k-elf-gcc src\main.c src\%platform%\init.S -Os -Wl,-Map,foo.map  -T src\%platform%\link.ld -nostdlib -ffreestanding -march=68000 -std=c99 -D%platform% -g -Wmissing-field-initializers 

mkdir out\%platform%

m68k-elf-objdump a.out -S -d >out\%platform%\%name%.lst
m68k-elf-objcopy a.out     out\%platform%\%name%.bin -O binary
m68k-elf-objcopy a.out     out\%platform%\%name%.s68 -O srec
m68k-elf-objcopy a.out     out\%platform%\%name%.hex -O ihex
