#include "stdlib.h"
#include "string.h"
#include "lib_stdio.h"
#include "lib_progman.h"
#include "lib_fileio.h"

#include "type_file_result.h"

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		stdio_print("error: missing path \n");
		return 1;
	}		
	
	int result = file_mkdir(argv[1]);
	
	if(result !=  FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}		
	return 0;
}
