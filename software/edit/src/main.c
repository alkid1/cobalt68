#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lib_progman.h"
#include "lib_stdio.h"

#define INP_SPECIAL 0x100  // Special input flag

#define SCREEN_ROWS 24
#define SCREEN_COLS 80

#define ROW_RESERVE 32   // Amount of character to preallocate over string length

enum KEY_ACTION{
        KEY_NULL = 0,       /* NULL */
        CTRL_C = 3,         /* Ctrl-c */
        CTRL_D = 4,         /* Ctrl-d */
        CTRL_F = 6,         /* Ctrl-f */
        CTRL_H = 8,         /* Ctrl-h */
		CTRL_R = 18,         /* Ctrl-h */
        TAB = 9,            /* Tab */
        CTRL_L = 12,        /* Ctrl+l */
        ENTER = 0xA,         /* Enter */
        CTRL_Q = 17,        /* Ctrl-q */
        CTRL_S = 19,        /* Ctrl-s */
        CTRL_U = 21,        /* Ctrl-u */
        ESC = 27,           /* Escape */
        BACKSPACE =  127,   /* Backspace */
        /* The following are just soft codes, not really reported by the
         * terminal directly. */
        ARROW_LEFT = 1000,
        ARROW_RIGHT,
        ARROW_UP,
        ARROW_DOWN,
        DEL_KEY,
        HOME_KEY,
        END_KEY,
        PAGE_UP,
        PAGE_DOWN
};

typedef struct row_s
{
    int    length;
    int    reserved;
    char * data;
    
    struct row_s * prev;
    struct row_s * next;
} row_t;

typedef struct
{
    char  * filepath;   // Dynamicaly allocated
    
    int     numrows;    // total number of rows 
    row_t * firstrow;   // Pointer to first row in linked list of rows.
    row_t * displayrow; // Pointer to first row of display windows

    int     crow;       // Cursor row position relative to start of display window
    int     ccol;       // Cursor col position relative to 0-th character of the row

    char    dirty;      // if != 0 -> there were modifications after last save
} doc_t;

typedef struct
{
    doc_t * document;     // Pointer to active document
    int     screen_rows;  // Number of rows in terminal screeen
    int     screen_cols;  // Number of cols in terminal screeen
    int     tab_length;   // Length of whitespace to render tabs
} editor_t;

stdio_configuration_t stdiocfg;

row_t * make_row(const char* data, row_t * prev, row_t *next)
{
    row_t *this = malloc(sizeof(row_t));
    int len = strlen(data);
    
    while((len > 0) && ((data[len - 1] == '\n') || (data[len - 1] == '\r'))) len --;
    
    this->length   = len;
    this->reserved = len + ROW_RESERVE;
    this->prev     = prev;
    this->next     = next;
    
    if(this->prev != 0) this->prev->next = this;
    if(this->next != 0) this->next->prev = this;
    
    this->data = malloc(this->reserved);
    
    if(this->data != 0)
    {
        memcpy(this->data, data, len);
        this->data[len] = 0;
    }
    return this;
}

void free_row(row_t *this)
{
    if(this->prev != 0) this->prev->next = this->next;
    if(this->next != 0) this->next->prev = this->prev;
    free(this->data);
}

doc_t * make_doc(const char* filepath)
{
    doc_t *this = malloc(sizeof(doc_t));
    
    this->filepath = strdup(filepath);
    
    this->ccol = 1;
    this->crow = 1;
    
    this->dirty = 0;
    
    FILE * f;
    
    f = fopen(this->filepath, "r");
    
    if(f == NULL)
    {
        this->firstrow   = make_row("", 0, 0);
        this->displayrow = this->firstrow;
        return this;
    }
    
    this->firstrow = 0;
    
    char buf[128];
    row_t * prev = 0;
    
    while(fgets(buf, sizeof(buf), f) != 0)
    {
        prev = make_row(buf, prev, 0);
        if(this->firstrow == 0) this->firstrow = prev;
    }
    
    this->displayrow = this->firstrow;
    
    fclose(f);
    
    return this;
}

editor_t * make_editor(const char* filepath)
{
    editor_t * this = malloc(sizeof(editor_t));
    
    this->document = make_doc(filepath);
    this->screen_rows = SCREEN_ROWS;
    this->screen_cols = SCREEN_COLS;
    this->tab_length  = 4;
    
    return this;
}

void free_doc(doc_t *this)
{
    free(this->filepath);
}

row_t * findCurrentRow(editor_t *editor)
{
    row_t * r = editor->document->displayrow;
    
    for(int i = 1; i < editor->document->crow; i++) 
    {
        r = r->next;
        if(r == 0) return 0; // Error - this shouldn't happen // TODO: Implement some meaningfull recovery
    }
    
    return r;
}

void place_cursor(editor_t * editor)
{
    row_t * r = findCurrentRow(editor);
    
    if(editor->document->ccol > r->length + 1)
        editor->document->ccol = r->length + 1;
    
    printf("\e[%d;%dH", editor->document->crow, editor->document->ccol);
    fflush(stdout); 
}

void render_row(editor_t * editor, row_t * row)
{
    for(int sym = 0, col = 0; (sym < row->length) & (col < editor->screen_cols); sym++, col++) 
    {
        if(row->data[sym] == '\t')
        {
            for(int i = 0; i < editor->tab_length; i++)
            {
                putchar(' ');
            }
            col += editor->tab_length - 1;
        }
        else
        {
            putchar(row->data[sym]);
        }
    }
}

void render(editor_t * editor)
{
    // CLS
    printf("\e[2J\e[1;1H");

    // Render rows
    row_t * row = editor->document->displayrow;
    
    // skip to first row to display
    for(int i = 0; i < (editor->screen_rows - 1); i++)
    {
        if(row == 0)
        {
            printf("--- EOF ------------------------");
            break;
        }
        else
        {
            render_row(editor, row);
            putchar('\n');
            row = row->next;
        }
    }
    
    place_cursor(editor);
}

int editorReadKey() 
{
    int nread;
    char c, seq[3];

    c = stdio_getchar();

    while(1) {
        switch(c) {
        case ESC:    /* escape sequence */
            /* If this is just an ESC, we'll timeout here. */
            
            seq[0] = stdio_getchar();
            seq[1] = stdio_getchar();

            /* ESC [ sequences. */
            if (seq[0] == '[') {
                if (seq[1] >= '0' && seq[1] <= '9') {
                    /* Extended escape, read additional byte. */
                    
                    seq[2] = stdio_getchar();
                    
                    // if (read(fd,seq+2,1) == 0) return ESC;
                
                    if (seq[2] == '~') {
                        switch(seq[1]) {
                        case '3': return DEL_KEY;
                        case '5': return PAGE_UP;
                        case '6': return PAGE_DOWN;
                        }
                    }
                } else {
                    switch(seq[1]) {
                    case 'A': return ARROW_UP;
                    case 'B': return ARROW_DOWN;
                    case 'C': return ARROW_RIGHT;
                    case 'D': return ARROW_LEFT;
                    case 'H': return HOME_KEY;
                    case 'F': return END_KEY;
                    }
                }
            }

            /* ESC O sequences. */
            else if (seq[0] == 'O') {
                switch(seq[1]) {
                case 'H': return HOME_KEY;
                case 'F': return END_KEY;
                }
            }
            break;
        default:
            return c;
        }
    }
}

void processScrolling(editor_t *editor, int key)
{
    switch(key)
    {
        case ARROW_LEFT:
            if(editor->document->ccol > 1) 
            {
                editor->document->ccol--;
                place_cursor(editor);
            }
            break;
        case ARROW_RIGHT:
            {
                row_t * cr = findCurrentRow(editor);
            
                if(editor->document->ccol <= cr->length) editor->document->ccol++;
                place_cursor(editor);
            }
            break;      
        case ARROW_UP:
            if(editor->document->crow > 1) 
            {
                editor->document->crow--;
                place_cursor(editor);
            }
            else
            {
                if(editor->document->displayrow->prev != 0)
                {
                    editor->document->displayrow = editor->document->displayrow->prev;
                    
                    //scroll-down
                    printf("\e[1;1H");                  
                    printf("\eM");
                    
                    // redraw first string
                    render_row(editor, editor->document->displayrow);
                    
                    // fix cursor position
                    place_cursor(editor);
                }
            }
            
            break;      
        case ARROW_DOWN:
            if(findCurrentRow(editor)->next == 0) break; // if it is last line in document - abort, can't move down
        
            if(editor->document->crow < (editor->screen_rows - 1)) 
            {
				// Simple case - move down within screen, no scrolling
                editor->document->crow++;
                place_cursor(editor);
            }
            else
            {
				// Complex case - need to scroll
                row_t * r = editor->document->displayrow;
                
				// Find last displayed row
                for(int i = 0; i < editor->screen_rows; i++) 
                {
                    r = r->next;
                    if(r == 0) return;
                }
                
				// Implement scrolling
                if(r->next != 0)
                {
					// move display windows one row down
                    editor->document->displayrow = editor->document->displayrow->next;
                    
                    //scroll-down screen
                    printf("\e[%d;1H", editor->screen_rows);
                    printf("\eD");
                    
                    // redraw last string
                    printf("\e[%d;1H", editor->screen_rows - 1);
                    render_row(editor, r->next);
                    fflush(stdout);
                    
                    // fix cursor position
                    place_cursor(editor);
                }
            }
            break;      
        case PAGE_UP: // TODO : Implement
        case PAGE_DOWN: // TODO : Implement
            break;
        case HOME_KEY:
            editor->document->ccol = 1;
            place_cursor(editor);
            break;
        case END_KEY:
            editor->document->ccol = findCurrentRow(editor)->length + 1;
            place_cursor(editor);
            break;      
    };  
}

void insertCharacter(editor_t *editor, int key)
{
    // find current row
    row_t * r = findCurrentRow(editor);
    
    // realloc if needed
    if(r->reserved < (r->length + 2))
    {
        r->data = realloc(r->data, r->reserved + ROW_RESERVE);
        if(r->data == 0) 
        {
            printf("Fatal error\n");
            exit(1);
        }
        r->reserved += ROW_RESERVE;
    }
    
    // insert character 
    memmove(&r->data[editor->document->ccol], &r->data[editor->document->ccol - 1], r->length - (editor->document->ccol - 1));
    r->data[editor->document->ccol - 1] = key;
    
    editor->document->ccol++;
    r->length++;
    
    // erase row
    printf("\e[2K\r");
    
    // render row
    render_row(editor, r);
    place_cursor(editor);
}

void insertNewline(editor_t *editor)
{
    // find current row
    row_t * r = findCurrentRow(editor); 
    
    if(editor->document->ccol >= r->length + 1) // Insert new line at end of current line
    { 
       row_t * nr = make_row("", r, r->next);
    }
    else // Insert newline in middle of current line
    {
        row_t * nr = make_row(&r->data[editor->document->ccol - 1], r, r->next);
        
        r->data[editor->document->ccol - 1] = 0;
        r->length = strlen(r->data);
    }

    // return carriage and place cursor down
    editor->document->ccol = 1; 
    processScrolling(editor, ARROW_DOWN);
    render(editor);
    place_cursor(editor);   
}

void deleteCharacter(editor_t *editor, int key)
{
    row_t * r = findCurrentRow(editor); 
    
    if(key == BACKSPACE)
    {
        if(editor->document->ccol == 1)
        {
            if(findCurrentRow(editor)->prev == 0) return;
            
            editor->document->crow--;
            
            r = findCurrentRow(editor); 
            
            editor->document->ccol = r->length + 1;
        }
        else
        {
            editor->document->ccol--;
        }
    }
    
    if((r->next) && (editor->document->ccol >= r->length + 1)) // Delete newline character - concatenate rows
    { 
       r->data = realloc(r->data, r->length + r->next->length + ROW_RESERVE);
       strncat(r->data, r->next->data, r->length + r->next->length + ROW_RESERVE);
       r->length = r->length + r->next->length;
       
       free_row(r->next);
       
       render(editor);
       place_cursor(editor);    
    }
    else // Delete character in middle of current line
    {
        memmove(&r->data[editor->document->ccol - 1], &r->data[editor->document->ccol], r->length - (editor->document->ccol - 1));
        r->length--;
        
        // erase row
        printf("\e[2K\r");
        
        // render row
        render_row(editor, r);
        place_cursor(editor);       
    }
}

void saveDocument(editor_t * editor)
{
    FILE * f = fopen(editor->document->filepath, "w+");
    
    for(row_t * r = editor->document->firstrow; r != 0; r = r->next)
    {
        r->data[r->length] = 0; 
        
        if(r->next)
        {
            fprintf(f, "%s\n", r->data);
        }
        else
        {
            fprintf(f, "%s", r->data);
        }
    }
    
    fclose(f);
}

void processInput(editor_t *editor, int key)
{
    /* Main cycle:
       read key
       process input:
         - newline
         - delete/backspace
         - special commands:
           - save
           - quit
           - refresh
    */  
    
    switch(key)
    {
        case ARROW_LEFT:
        case ARROW_RIGHT:
        case ARROW_UP:
        case ARROW_DOWN:
        case HOME_KEY:
        case END_KEY:
        case PAGE_UP:
        case PAGE_DOWN: // TODO : Implement
            processScrolling(editor, key);
            break;      
        case ENTER:
            insertNewline(editor);
            break;
        case CTRL_Q:
            printf("\e[2J\e[1;1H");
			stdio_setConfiguration(&stdiocfg);
            exit(0);
            break;
        case CTRL_S: 
            saveDocument(editor);
            break;
		case CTRL_R: 
			render(editor);
			break;
        case DEL_KEY:
        case BACKSPACE:
            deleteCharacter(editor, key);
            break;
        default:
            if((key >= 32) && (key < 127)) insertCharacter(editor, key);
    };
}

void setStdioConfiguration()
{
	stdio_getConfiguration(&stdiocfg);
	stdio_configuration_t newConfig = { sizeof(newConfig), STDIO_CFG_PPINPUT | STDIO_CFG_PPOUTPUT };
	stdio_setConfiguration(&newConfig);
}

int main(int argc, char *argv[])
{
	if(argc != 2)
	{
		printf("Usage: edit <filename>\n");
	}
	printf("Loading document...\n");

	setStdioConfiguration();
    
    editor_t * editor = make_editor(argv[1]);
    
    render(editor);

    while(1)
    {
        int key = editorReadKey();
        processInput(editor, key);
    }
	
	return 0;
}
