#include <stdint.h>
#include <stdio.h>

#include "elf.h"
#include "lib_fileio.h"
#include "lib_stdio.h"

void _putchar(char character)
{
	//kputchar(character);
	stdio_putchar(character);
}

Elf32_Ehdr ehdr;

int load_image(file_t *file, Elf32_Ehdr *ehdr, void* targetAddress)
{
	Elf32_Phdr phdr;
	
	printf("Loading image.\n");
	printf("PHDR offset: %d.\n", ehdr->e_phoff);
	printf("PHDR count: %d.\n" , ehdr->e_phnum);
	
	int res;
	
	for(int i = 0; i < ehdr->e_phnum; i++)
	{
		if(res = file_seek(*file, ehdr->e_phoff + sizeof(Elf32_Phdr) * i, FSYS_SEEK_BEGIN))
		{
			printf("Error seeking to phdr table\n");
			return res;
		}
		
		if (res = file_read(*file, &phdr, sizeof(phdr), 0))
		{
			printf("Error loading phdr table\n");
			return res;			
		}
		
		printf("PHDR %d.\n", i);
		printf("PHDR type : %d  \n", phdr.p_type);
		printf("PHDR vaddr: %xh \n", phdr.p_vaddr);
		printf("PHDR size:  %xh \n", phdr.p_filesz);
		
		if(phdr.p_type == PT_LOAD && phdr.p_vaddr == 0x10000)
		{
			printf("Loading:  \n", phdr.p_filesz);
			if(res = file_seek(*file, phdr.p_offset, FSYS_SEEK_BEGIN))
			{
				printf("Error seeking to phdr data\n");
				return res;
			}
			
			if(res = file_read(*file, targetAddress, phdr.p_filesz, 0))
			{
				printf("Error loading phdr data\n");
				return res;
			}
			return 0;
		}
	}
}

int read_symtab(file_t *file, Elf32_Ehdr *ehdr, void *targetAddress)
{
	Elf32_Shdr shdr;
	
	Elf32_Shdr symtab;
	Elf32_Shdr strtab;
	Elf32_Shdr rela;
	
	
	// Search for sections
	for(int i = 0; i < ehdr->e_shnum; i++)
	{
		file_seek(*file, ehdr->e_shoff + sizeof(Elf32_Shdr) * i, FSYS_SEEK_BEGIN);
		file_read(*file, &shdr, sizeof(shdr), 0);
		
		if(shdr.sh_type == SHT_SYMTAB)
		{
			symtab = shdr;
		}			
		
		if(shdr.sh_type == SHT_STRTAB)
		{
			strtab = shdr;
		}			
		
		if(shdr.sh_type == SHT_RELA)
		{
			rela = shdr;
		}					
		
	}
	
	
	Elf32_Sym sym;
	
	char buf[32];
/*
	printf("Symbols:\n");
	for(int i = 0; i < symtab.sh_size / sizeof(Elf32_Sym); i++)
	{
		file_seek(*file, symtab.sh_offset + sizeof(Elf32_Sym) * i, FSYS_SEEK_BEGIN);
		file_read(*file, &sym, sizeof(sym), 0);
		
		file_seek(*file, strtab.sh_offset + sym.st_name, FSYS_SEEK_BEGIN);
		file_read(*file, &buf, sizeof(buf), 0);
		
		printf("Symbol: %s, value: %x, size: %x \n", buf, sym.st_value, sym.st_size);
	}
*/	
	Elf32_Rela rel;
	
	unsigned int offset = (unsigned int)targetAddress - 0x10000;
	
	printf(" Base   address: %x\n", 0x10000);
	printf(" Target address: %x\n", (unsigned int)targetAddress);
	printf(" Target offset : %x\n", offset);
	
	printf("Relocations:\n");
	for(int i = 0; i < rela.sh_size / sizeof(Elf32_Rela); i++)
	{
		file_seek(*file, rela.sh_offset + sizeof(Elf32_Rela) * i, FSYS_SEEK_BEGIN);
		file_read(*file, &rel, sizeof(rel), 0);		
		
		file_seek(*file, symtab.sh_offset + sizeof(Elf32_Sym) * ELF32_R_SYM(rel.r_info), FSYS_SEEK_BEGIN);
		file_read(*file, &sym, sizeof(sym), 0);
		
		file_seek(*file, strtab.sh_offset + sym.st_name, FSYS_SEEK_BEGIN);
		file_read(*file, &buf, sizeof(buf), 0);		
		
		// Fixup address
		*((unsigned int*)(rel.r_offset + offset)) += offset;
		
		printf("Symbol: %s: %x + %x, type: %d \n", buf, sym.st_value, rel.r_offset, ELF32_R_TYPE(rel.r_info));
	}
}

int relocate_image(file_t *file, Elf32_Ehdr *ehdr, void* targetAddress)
{
	
}

int main(int argc, char *argv[])
{
	printf("ELF testbed \n");
	file_t file;
	int res;
	
	void* targetAddress = (void *)0x20000;
	
	res = file_open("a.out", 0, &file);
	
	if(res)
	{
		printf("Open error: %d", res);
		return 1;
	}
	
	file_read(file, &ehdr, sizeof(ehdr), 0);
	
	load_image(&file, &ehdr, targetAddress);
/*	read_symtab(&file, &ehdr, targetAddress);
	relocate_image(&file, &ehdr, targetAddress);*/
	
	printf("Running loaded image\n");
	
	void (*ptr)(void) = (void (*)(void))targetAddress;
	ptr();
	
/*
	Function plan:
	1. Iterate through program headers, load relevant into memory.
	2. Iterate through sections.
		2.1 Find rela sections, apply relocations.
*/	
	file_close(file);
	
	printf("Finished.\n");
	return 0;
}
