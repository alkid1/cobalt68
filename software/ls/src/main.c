#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "lib_stdio.h"
#include "lib_fileio.h"

#include "type_file_result.h"

int main(int argc, char *argv[])
{
	char * name = argc >= 2
			? argv[1]
			: "/";
	
	fsys_filerec_t filerec;
	unsigned long tracker = FSYS_LIST_START;
	
	FILE_RESULT result = file_list(name, &filerec, &tracker);
	
	if(result != FSYS_OK)
	{
		type_file_result(result);
		return 1;
	}
	
	while(result == FSYS_OK && result != FSYS_LIST_END && filerec.name[0] != 0)
	{
		if(filerec.flags & FSYS_DIRECTORY) 
			printf("%s\\\n", filerec.name);
		else
			printf("%-12s : %d bytes.\n", filerec.name, filerec.size);
		
		result = file_list(name, &filerec, &tracker);
	}	
	return 0;
}
