
	.equ UART_PORT,  0xFF800
	.equ BOOT_PORT,  0xFF801
	
	.equ RAM_START, 0x00000	
	.equ ROM_START, 0xE0000		

.section .ivt, "a", @note
	.long 0
	.long _start

	.text	
	.global _start

_start:	
	move.b #1, (BOOT_PORT)  /* Disable boot signal */
	
	move.b #0xAA, (0)
	move.b (0)  , %D0
	
	move.b #0xAA, (0x7ffff)
	move.b (0x7ffff), %D0	
	
_endloop:
		jmp _endloop
/*	
	move.l #_message, %A0
	
_loop:	
	move.b (%A0)+, (UART_PORT)
	cmpi.b #0, (%A0)
	bne  _loop


_message_ok:	
	.string "OK"
	
_message_fail:	
	.string "FAIL"	
*/