	.equ UART_PORT, 0xFF800

	.text	
	.global _start

_start:	
	move.l #_message, %a0
	
_loop:
	move.b (%A0)+, (UART_PORT)
	cmpi.b #0, (%A0)
	bne  _loop
	
_endloop:
	jmp _endloop
	
_message:	
	.string "Hello, loader!"
	