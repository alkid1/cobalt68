	.equ MCU_PORT, 0xFF800

.section .ivt, "a", @note
	.long 0
	.long _start

	.text	
	.global _start

_start:	
	move.l #_message, %A0
	
_loop:	
	move.b (%A0)+, (MCU_PORT)
	cmpi.b #0, (%A0)
	bne  _loop

_echo_loop:	
	move.b (MCU_PORT), %D0
	move.b %D0, (MCU_PORT) 
	jmp _echo_loop

_message:	
	.string "PROMPT>"
