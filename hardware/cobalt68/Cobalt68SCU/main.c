/*
 * Z80SystemController.c
 *
 * Created: 15.06.2017 23:06:31
 * Author : Alex
 */ 

#include "defs.h"


#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "..\..\..\..\avr\common\ports.h"

#include "uart.h"
#include "spi.h"
#include "ps2.h"
#include "dma.h"

volatile uint8_t testRegister = 0x0;
volatile uint8_t intRegister  = 0;

static inline void trace_signal(const char* message)
{
	#ifdef SIGNAL_TRACING
	if(getv(TRACE_PORT) == 0)
	{
		usart_pstr(message);		
	}
	#endif
}

void RaiseInterrupt(uint8_t intNumber)
{
	intRegister = intNumber;
	
	// Assert INT#
	unset(CTRL2_INT);
}

void scuMainRoutine();

int main( void )
{
	// Powerup system reset
	PORT_OUT(CTRL_RST);		
	setv(CTRL_RST, 0);    // enable RST#
	
	// Address bus:
	DDR(ADDR_PORT)  = 0x0;
	PORT(ADDR_PORT) = ADDRESS_MASK; // Enable pullup

	// Default for data port - input, pullup
	DDR(DATA_PORT)  = 0x0;
	PORT(DATA_PORT) = 0xFF; 
	
	// Control
	PORT_IN_PULLUP(CTRL_CE);
	PORT_IN_PULLUP(CTRL_WE);
	PORT_IN_PULLUP(CTRL_IACK);
	PORT_IN_PULLUP(TRACE_PORT);
	
	PORT_OUT(CTRL_BR);
	PORT_OUT(CTRL_IPL0);
	PORT_OUT(CTRL_IPL1);

	set(CTRL_BR);
	set(CTRL_IPL0);
	set(CTRL_IPL1);	
	
	PORT_OUT(CTRL_SCUDTACK);
	PORT_OUT(CTRL_BOOT);
	
	PORT_OUT(CTRL2_INT);
	
	setv(CTRL_SCUDTACK, !SCUDTACK_TRUE);  // init  DTACK# to deasserted value
	setv(CTRL_BOOT , 0);                  // init  BOOT#
	setv(CTRL2_INT , 1);                  // init  INT#
	

	
	// Debug
	PORT_OUT(DBG_LED_0);
	
	m_uart_init();
	m_spi_init();
	m_ps2_init();
	
	usart_pstr("\n\rRESET\n\r");
	_delay_ms(200);		

	setv(CTRL_RST, 1);   // disable RST#
	
	sei();
	
	while (1)
	{
		if(getv(CTRL_CE) == 0)
		{
			scuMainRoutine();	
		}
		SPI_DMA_MAIN;
	}	

	return 0;
}

char itoab[16];

//Interrupt Service Routine for INT0
void scuMainRoutine()
{
	uint8_t ioAddress;

	ioAddress = PIN(ADDR_PORT) & ADDRESS_MASK; // Input only five bits.

#ifdef SIGNAL_TRACING
	if(getv(TRACE_PORT) == 0)
	{
		trace_signal("CS#\n");
		usart_pstr("A:");
		itoa(ioAddress, itoab, 16);
		usart_pstr(itoab);
		usart_putchar('\n');
	}
#endif	

	// it is read
	if(getv(CTRL_WE) == 1)
	{
		trace_signal("RE#\n");
		uint8_t outputData;

		if(!getv(CTRL_IACK))
		{
			set(CTRL2_INT); // Deassert INT#
			outputData = intRegister;
		}
		else
		{
			switch(ioAddress)
			{
			case PORT_TTY:
			case PORT_UCR:
			case PORT_USR:
				outputData = m_uart_read(ioAddress);
				break;
				
			case PORT_BOOT:
				outputData = getv(CTRL_BOOT);
				break;
			case PORT_TESTREG:
				outputData = testRegister;
				break;

			case PORT_SPISR:
			case PORT_SPIDATA:
			case PORT_SPIDMAS:
				outputData = m_spi_read(ioAddress);
				break;				
			case PORT_PS2DR:
			case PORT_PS2SR:
				outputData = m_ps2_read(ioAddress);
				break;
			default:				
				usart_pstr("E!"); 
				itoa(ioAddress, itoab, 10);
				usart_pstr(itoab);				
				usart_putchar('\n');
				outputData = 0;
			}
		}
				
		// Output data
		DDR(DATA_PORT) = 0xFF;
		PORT(DATA_PORT) = outputData;
		
		#ifdef SIGNAL_TRACING
		if(getv(TRACE_PORT) == 0)
		{
			usart_pstr("D:");
			itoa(outputData, itoab, 16);
			usart_pstr(itoab);
			usart_putchar('\n');
		}
		#endif		
			
		// Assert DTACK#
		trace_signal("DTACK#\n");		
		setv(CTRL_SCUDTACK, SCUDTACK_TRUE);


		// Wait while CE is low.
		while(getv(CTRL_CE) == 0);
			
		// Tristate data port
		DDR(DATA_PORT)  = 0x0;
		PORT(DATA_PORT) = 0x0;
		
		// Deassert DTACK#
		setv(CTRL_SCUDTACK, !SCUDTACK_TRUE);		

		trace_signal("!DTACK#\n");
	}
	else
	{
		trace_signal("WE#\n");
		uint8_t inputData = PIN(DATA_PORT);
		
		#ifdef SIGNAL_TRACING
		if(getv(TRACE_PORT) == 0)
		{
			usart_pstr("D:");
			itoa(inputData, itoab, 16);
			usart_pstr(itoab);
			usart_putchar('\n');
		}
		#endif		

		switch(ioAddress)
		{
		case PORT_TTY:
		case PORT_UCR:
			m_uart_write(ioAddress, inputData);
			break;
			
		case PORT_BOOT:
			setv(CTRL_BOOT, inputData);
			break;
		case PORT_TESTREG:
			testRegister = inputData;
			break;
		case PORT_INTREG:
			RaiseInterrupt(inputData);
			break;
		case PORT_SPISS:
		case PORT_SPIDATA:
		case PORT_SPISR:
		case PORT_SPIDMA + 0:
		case PORT_SPIDMA + 1:
		case PORT_SPIDMA + 2:
		case PORT_SPIDMA + 3:		
		case PORT_SPIDMAS:
			m_spi_write(ioAddress,inputData);
			break;
		default:
			usart_pstr("E!"); 
			itoa(ioAddress, itoab, 10);
			usart_pstr(itoab);
			usart_putchar('\n');
			break;
		}

		// Release WAIT trigger
		trace_signal("DTACK#\n");		
		setv(CTRL_SCUDTACK, SCUDTACK_TRUE);
		setv(CTRL_SCUDTACK, !SCUDTACK_TRUE);
		trace_signal("!DTACK#\n");
	}
	trace_signal("---\n");
}