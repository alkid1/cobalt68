/*
 * dma.c
 *
 * Created: 20.11.2020 18:34:24
 *  Author: Alex
 */ 

#include "dma.h"

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "..\..\..\..\avr\common\ports.h"

#include "spi.h"


static inline void dma_setAddress(unsigned long address)
{
	PORT(ADDR_PORT )  =  address;
	PORT(ADDR_PORT1) &= ~ADDR_PORT1_MASK;  
	PORT(ADDR_PORT1) |=  ((address >> 4) & ADDR_PORT1_MASK);
	PORT(ADDR_PORT2)  =  address >> (8 + 4);	
}

void writeByte(unsigned long address, uint8_t data)
{
	dma_setAddress(address);
	
	DDR(DATA_PORT) = 0xFF;
	PORT(DATA_PORT) = data;
	
	const uint8_t csigmask = bit(g_sig(CTRL_AS))
							| bit(g_sig(CTRL_DS))
							| bit(g_sig(CTRL_W));
	
	
//	unset(CTRL_AS);
//	unset(CTRL_DS);
//	unset(CTRL_W);	
	
	PORT(CONTROl_PORT) &= ~csigmask;
	
	//while(getv(CTRL_DTACK) == 1);
	
	PORT(CONTROl_PORT) |= csigmask;
	
//	set(CTRL_W);
//	set(CTRL_DS);
//	set(CTRL_AS);
	
	
	DDR(DATA_PORT) = 0x0;
	//PORT(DATA_PORT) = 0x0;
}

uint8_t readByte(unsigned long address)
{
	dma_setAddress(address);
	
	unset(CTRL_AS);
	unset(CTRL_DS);
	
	while(getv(CTRL_DTACK) == 1);
	
	uint8_t result = PIN(DATA_PORT);
	
	set(CTRL_W);
	set(CTRL_DS);
	
	return result;
}

bool AquireBus()
{
	unset(CTRL_BR); // Assert BR#
	
	while(getv(CTRL_BG) == 1)
	{
		#ifndef SCUMAIN_INT0
		if(getv(CTRL_CE) == 0)
		{
			scuMainRoutine();
		}
		#endif		
	};
	
	// Control lines to output
	set(CTRL_W);
	set(CTRL_AS);
	set(CTRL_DS);
	
	PORT_OUT(CTRL_WE);
	PORT_OUT(CTRL_AS);
	PORT_OUT(CTRL_DS);
	
	// Address lines to output
	DDR(ADDR_PORT) = 0xFF;
	DDR(ADDR_PORT1) |= ADDR_PORT1_MASK;
	DDR(ADDR_PORT2) = 0xFF;
	return true;
}

void ReleaseBus()
{
	// Control lines to input
	PORT_IN(CTRL_WE);
	PORT_IN(CTRL_AS);
	PORT_IN(CTRL_DS);
	
	unset(CTRL_WE);
	unset(CTRL_AS);
	unset(CTRL_DS);
	
	// Address lines to input
	DDR(ADDR_PORT) = 0x00;
	DDR(ADDR_PORT1) &= ~ADDR_PORT1_MASK;
	DDR(ADDR_PORT2) = 0x00;
	
	set(CTRL_BR); // Up BR#
}




