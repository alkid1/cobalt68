/*
 * ps2.c
 *
 * Created: 20.11.2020 18:42:49
 *  Author: Alex
 */ 

#include "ps2.h"

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "..\..\..\..\avr\common\ports.h"
#include "queue.h"

DECLARE_QUEUE(ps2buf);



void m_ps2_init()
{
	// PS2 Port
	PORT_IN_PULLUP(PS2CLOCK);
	PORT_IN_PULLUP(PS2DATA);	
	
	// Enable INT1
	EIMSK |=   bit(INT1);	// Enable INT1
	EICRA |=   bit(ISC11);  // Trigger INT1 on falling edge	
}

uint8_t m_ps2_read(uint8_t address)
{
	switch(address)
	{
	case PORT_PS2DR:
		while(QUEUE_COUNT(ps2buf) == 0);
		uint8_t outputData;
		
		QUEUE_POP(ps2buf, outputData);
		
		return outputData;
	case PORT_PS2SR:
		return QUEUE_COUNT(ps2buf) != 0;
	default:				
		return 0;
	}	
}

void m_ps2_write(uint8_t address, uint8_t value)
{
	
}

ISR(INT1_vect)
{
	static unsigned char bitcount = 11;   // holds the received scan code
	static uint8_t ps2dr;
	// Place your code here
	// function entered at falling edge of the kbd clock signal
	// if data bit is the next bit to be read
	// (bit 3 to 10 is data, start, stop & parity bis are ignored
	if((bitcount < 11) && (bitcount > 2)) {
		ps2dr = (ps2dr >> 1);
		if (getv(PS2DATA) == 1)         // if next bit is 1
		ps2dr = ps2dr | 0x80;   // store a '1'
		else
		ps2dr = ps2dr & 0x7f; // else store a '0'
	}
	if(--bitcount == 0) {        // all bits received ?
		QUEUE_PUSH(ps2buf, ps2dr);
		bitcount = 11;           // reset bit counter
	}
}