/*
 * uart.c
 *
 * Created: 20.11.2020 17:36:58
 *  Author: Alex
 */ 

#include "uart.h"

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "..\..\..\..\avr\common\ports.h"

#include "queue.h"

// *** DATA *************************************************************************************************************
volatile uint8_t reg_ucr = 0;

DECLARE_QUEUE(ubuf);


// *** MODULE API *******************************************************************************************************
void m_uart_init()
{
	usart_init(MYUBRR);		
}

uint8_t m_uart_read(uint8_t address)
{
	switch(address)
	{
	case PORT_TTY:
		{
			while(QUEUE_COUNT(ubuf) == 0);
	
			uint8_t d ;
			
			QUEUE_POP(ubuf, d);
			
			if(QUEUE_COUNT(ubuf) == 0) unset(UART_CTS);
			return d;
		}
	case PORT_UCR:
		return reg_ucr;
	case PORT_USR:
		return FMAP(UCSR3A, bit(UDRE3), USR_OBE) |
			   FMAP((QUEUE_COUNT(ubuf) > 0), 1, USR_IBF);
	default:				
		return 0;
	}
}

void m_uart_write(uint8_t address, uint8_t inputData)
{
	switch(address)
	{
	case PORT_TTY:
		usart_putchar(inputData);
		break;
	case PORT_UCR:
		reg_ucr = inputData;
		break;
	}	
}

// *** IMPLEMENTATION DETAILS *******************************************************************************************
volatile uint8_t d;

ISR(USART3_RX_vect)
{
	// Forbid receiving data
	set(UART_CTS);
	
	set(DBG_LED_0);
	unset(DBG_LED_0);

	QUEUE_PUSH(ubuf, UDR3);
		
	if(reg_ucr & UCR_RX_INTERRUPT_ENABLED)
	{
		RaiseInterrupt(UART_RXC_INT);
	}
}

ISR(USART3_TX_vect)
{
	if(reg_ucr & UCR_TX_INTERRUPT_ENABLED)
	{
		RaiseInterrupt(UART_TXC_INT);
	}
}

// ********************************************************************************
// usart Related
// ********************************************************************************
void usart_init( uint16_t ubrr) {
	// Set baud rate
	UBRR3H = (uint8_t)(ubrr>>8);
	UBRR3L = (uint8_t)ubrr;
	
	// Enable receiver and transmitter
	UCSR3B = bit(TXEN3) | bit(RXEN3);
	
	// Set frame format: 8data, 1stop bit
	UCSR3C = bit(UCSZ31) | bit(UCSZ30) ;
	
	// Enable UART Interrupts
	UCSR3B |= bit(RXCIE3) | bit(TXCIE3); // Enable the USART Recieve Complete interrupt (USART_RXC)
	
	PORT_OUT(UART_CTS);
	unset(UART_CTS);
}

void usart_putchar(char data) {
	// Wait for empty transmit buffer
	while ( !(UCSR3A & (bit(UDRE3))) );
	
	// Start transmission
	UDR3 = data;
}

char usart_getchar(void)
{
	// Wait for incoming data
	while ( !(UCSR3A & (bit(RXC3))) ) unset(UART_CTS);
	unset(UART_CTS);
	
	// Return the data
	return UDR3;
}
unsigned char usart_kbhit(void) {
	//return nonzero if char waiting polled version
	unsigned char b;
	b=0;
	if(UCSR3A & (1<<RXC3)) b=1;
	return b;
}

void usart_pstr(const char *s) {
	// loop through entire string
	while (*s) {
		usart_putchar(*s);
		s++;
	}
}