/*
 * spi.c
 *
 * Created: 20.11.2020 18:23:00
 *  Author: Alex
 */ 
#include "spi.h"

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include <stdbool.h>
#include "..\..\..\..\avr\common\ports.h"

#include "dma.h"
#include "uart.h"

volatile DMABlock_t dma = {0, {0}, 0};

static inline char SPI_Transmit(char cData)
{
	SPDR = cData;
	
	while(!(SPSR & bit(SPIF)));
	
	return SPDR;
}

void m_spi_init()
{
	// SPI
	PORT_OUT(SPI_SS);
	PORT_OUT(SPI_MOSI);
	PORT_OUT(SPI_SCK);
	PORT_OUT(SPI_SSA);
	PORT_OUT(SPI_SSB);
	PORT_OUT(SPI_SSC);
	PORT_OUT(SPI_SSD);
	
	set(SPI_SS); // SPI SS# is high by default
	
	SPCR = bit(SPE) | bit (MSTR);// | bit(SPR0) | bit(SPR1); // | bit(CPOL) | bit(CPHA); // SPI enable, Master, f/128	
}

uint8_t m_spi_read(uint8_t address)
{
	switch(address)
	{
	case PORT_SPIDATA:
		return SPDR;
	
	case PORT_SPISR:
		{
			const uint8_t spr_mask = bit(SPR0) | bit(SPR1);
			return (SPCR & spr_mask);
		}
	case PORT_SPIDMAS:
		return dma.flags;
	default:				
		return 0;
	}
}

void m_spi_write(uint8_t ioAddress, uint8_t inputData)
{
	switch(ioAddress)
	{
	case PORT_SPISS:
		if(inputData == 0xff)
		{
			setv(SPI_SS, 1);
		}
		else
		{
			PORT(SPI_ADDR_PORT)	&= 0xF0;
			PORT(SPI_ADDR_PORT)	|= (inputData & 0xF);
			setv(SPI_SS, 0);
		}
			
		break;
	case PORT_SPIDATA:
		SPI_Transmit(inputData);
		break;
	case PORT_SPISR:
		{
			const uint8_t spr_mask = bit(SPR0) | bit(SPR1);
			SPCR = (SPCR & (~spr_mask)) | (inputData & spr_mask);
			break;			
		}
	case PORT_SPIDMA + 0:
	case PORT_SPIDMA + 1:
	case PORT_SPIDMA + 2:
	case PORT_SPIDMA + 3:
		{
			dma.destinationByte[3 - (ioAddress - PORT_SPIDMA)] = inputData;
			
			break;
		}
	case PORT_SPIDMAS:
		dma.flags = inputData;

		if(dma.flags & DMA_ACTIVE)
		{
			dma.size  = 512; // Preset default transfer block
		}

		break;
	}	
}


//#define DMA_CYCLE_STEALING 1
static inline void m_spi_DmaTransferByte()
{
	if(dma.flags & DMA_WRITE)
	{
		SPI_Transmit(readByte(dma.desination++));
	}
	else
	{
		SPI_Transmit(0xFF);
		writeByte(dma.desination++, SPDR);
	}
}

void m_spi_DmaMain()
{
	if(!AquireBus()) return;
#ifdef DMA_CYCLE_STEALING
	m_spi_DmaTransferByte();
	dma.size--;
	
	if(dma.size == 0)
	{
		dma.flags = 0;
	}
#else

	SPDR = 0xFF; // Initial byte transfer
	_delay_us(3); // Why the hell this delay is needed is mystery for me. But without is one extra byte us read

	while(dma.size)
	{
		while(!(SPSR & bit(SPIF))); // Wait for byte from SPI.
		uint8_t data = SPDR;        // Store byte
		
		SPDR = 0xFF; 
		
		writeByte(dma.desination++, data);
		
		dma.size--;
	}
	
	dma.flags = 0;
#endif
	ReleaseBus();
}
