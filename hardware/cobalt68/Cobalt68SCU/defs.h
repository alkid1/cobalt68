/*
 * defs.h
 *
 * Created: 20.11.2020 17:27:20
 *  Author: Alex
 */ 

#ifndef DEFS_H_
#define DEFS_H_

#define F_CPU 8000000UL

#define ADDRESS_MASK 0x1F

#define RESET_TRUE    0
#define SCUDTACK_TRUE 1

//#define SIGNAL_TRACING

#define DMA_IS_DEFINED 

#define TRACE_PORT (L, 5)

#define ADDR_PORT A
#define ADDR_A8   (B, 4) // 23
#define ADDR_A9   (B, 5) // 24
#define ADDR_A10  (B, 6) // 25
#define ADDR_A11  (B, 7) // 26

#define ADDR_A12  (C, 0) // 53
#define ADDR_A13  (C, 1) // 54
#define ADDR_A14  (C, 2) // 55
#define ADDR_A15  (C, 3) // 56
#define ADDR_A16  (C, 4) // 57
#define ADDR_A17  (C, 5) // 58
#define ADDR_A18  (C, 6) // 59
#define ADDR_A19  (C, 7) // 60

#define ADDR_PORT1 B
#define ADDR_PORT1_MASK (bit(4) | bit(5) | bit (6) | bit (7))
#define ADDR_PORT2 C

#define DATA_PORT E

#define PS2DATA  (F ,1)
#define PS2CLOCK (D ,1)   // INT1

// System and bus control signals
#define CTRL_CE       (D, 0) // SCUCS#
#define CTRL_BOOT     (F, 7) // BOOT#
#define CTRL_IACK     (F, 4) // IACK# Active LOW
#define CTRL_SCUDTACK (F, 5) // SCUDTACK#
#define CTRL_RST      (F, 6) // CM-RESET#

#define CONTROl_PORT G
#define CTRL_BG       (G, 0) // BG#
#define CTRL_BR       (G, 1) // BR#
#define CTRL_W        CTRL_WE
#define CTRL_DTACK    (G, 2)
#define CTRL_AS       (G, 3)
#define CTRL_DS       (G, 4)
#define CTRL_WE       (G, 5) // W#


#define CTRL_IPL0     (F, 2) // IPL0,2#
#define CTRL_IPL1     (F, 3) // IPL1#

#define CTRL2_INT   (F, 2)

#define SPI_SS    (B, 0)
#define SPI_MOSI  (B, 2)
#define SPI_MISO  (B, 3)
#define SPI_SCK   (B, 1)

#define SPI_ADDR_PORT L       // L0-L3 - spi decoder address
#define SPI_SSA       (L, 0)
#define SPI_SSB       (L, 1)
#define SPI_SSC       (L, 2)
#define SPI_SSD       (L, 3)

// UART signals
#define UART_RXD      (J, 0) // RXD
#define UART_TXD      (J, 1) // TXD
#define UART_CTS      (F, 0)


// SCU Ports
#define PORT_TTY     0
#define PORT_BOOT    1
#define PORT_TESTREG 2
#define PORT_INTREG  3
#define PORT_UCR     4
#define PORT_SPIDATA 5
#define PORT_SPISS   6
#define PORT_SPISR   7
#define PORT_SPIDMA  8   // + 4
#define PORT_SPIDMAS 12
#define PORT_PS2DR   13  // PS2 Data Register
#define PORT_PS2SR   14  // PS2 Status Register
#define PORT_USR     15  // UART Status register

#define DBG_LED_0    (J,2)

#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// ********************************************************************************
// Macros and Defines
// ********************************************************************************
#define BAUD 14400
#define MYUBRR F_CPU/16/BAUD-1

// USR - UART Status register
#define USR_IBF bit(0)   // Input buffer full
#define USR_OBE bit(1)   // Output buffer empty

// UCR - UART Control Register
#define UCR_RX_INTERRUPT_ENABLED bit(0)
#define UCR_TX_INTERRUPT_ENABLED bit(1)

#define UART_RXC_INT 64
#define UART_TXC_INT 65

// PS2SR - PS2 Status Register
#define PS2SR_HAS_DATA bit(0)


// DMA
#define DMA_ACTIVE 1
#define DMA_WRITE  2


#define FMAP(value, flag1, flag2) (value & flag1 ? flag2 : 0)

// Functions
void RaiseInterrupt(uint8_t intNumber);
void scuMainRoutine();

extern char itoab[16];

#endif /* DEFS_H_ */