/*
 * spi.h
 *
 * Created: 20.11.2020 18:22:51
 *  Author: Alex
 */ 


#ifndef SPI_H_
#define SPI_H_

#include "defs.h"
#include <stdlib.h>

typedef struct
{
	unsigned char flags;
	union
	{
		unsigned long desination;
		unsigned char destinationByte[4];
	};
	unsigned int  size;
} DMABlock_t;

extern volatile DMABlock_t dma;

void m_spi_init();
uint8_t m_spi_read(uint8_t address);
void m_spi_write(uint8_t address, uint8_t value);
void m_spi_DmaMain();


#define SPI_DMA_MAIN if(dma.flags & DMA_ACTIVE) m_spi_DmaMain();

#endif /* SPI_H_ */