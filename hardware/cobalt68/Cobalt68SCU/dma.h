/*
 * IncFile1.h
 *
 * Created: 30.12.2020 16:15:38
 *  Author: Alex
 */ 


#ifndef INCFILE1_H_
#define INCFILE1_H_

#include "defs.h"
#include <stdbool.h>

void writeByte(unsigned long address, uint8_t data);
uint8_t readByte(unsigned long address);
bool AquireBus();
void ReleaseBus();


#endif /* INCFILE1_H_ */