/*
 * ps2.h
 *
 * Created: 20.11.2020 18:42:36
 *  Author: Alex
 */ 


#ifndef PS2_H_
#define PS2_H_

#include "defs.h"

void m_ps2_init();
uint8_t m_ps2_read(uint8_t address);
void m_ps2_write(uint8_t address, uint8_t value);



#endif /* PS2_H_ */