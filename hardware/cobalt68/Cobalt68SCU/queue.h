/*
 * queue.h
 *
 * Created: 10.01.2021 16:42:52
 *  Author: Alex
 */ 


#ifndef QUEUE_H_
#define QUEUE_H_

#define BUFSIZE 16

typedef struct 
{
	uint8_t buffer[BUFSIZE];
	uint8_t b_widx;
	uint8_t b_ridx;
	uint8_t b_count;
} queue_t;

#define DECLARE_QUEUE(a) volatile queue_t a = { .b_widx = 0, .b_ridx = 0, .b_count = 0}

#define QUEUE_PUSH(q, data) \
			{ \
				q.buffer[q.b_widx] = data; \
				q.b_widx = (q.b_widx + 1) % BUFSIZE; \
				q.b_count++; \
			}

#define QUEUE_POP(q, var) \
	{\
		var = q.buffer[q.b_ridx];\
		q.b_ridx = (q.b_ridx + 1) % BUFSIZE;\
		q.b_count--;\
	}

#define QUEUE_COUNT(q) q.b_count



#endif /* QUEUE_H_ */