/*
 * UART.h
 *
 * Created: 20.11.2020 17:36:50
 *  Author: Alex
 */ 

#ifndef UART_H_
#define UART_H_

#include "defs.h"

// Module API
void m_uart_init();
uint8_t m_uart_read(uint8_t address);
void m_uart_write(uint8_t address, uint8_t value);

// USART utilities
// ********************************************************************************
// Function Prototypes
// ********************************************************************************
void usart_init(uint16_t ubrr);
char usart_getchar( void );
void usart_putchar( char data );
void usart_pstr (const char *s);
unsigned char usart_kbhit(void);

#endif /* UART_H_ */