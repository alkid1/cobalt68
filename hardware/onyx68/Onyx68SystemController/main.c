/*
 * Z80SystemController.c
 *
 * Created: 15.06.2017 23:06:31
 * Author : Alex
 */ 

#define F_CPU 8000000UL
//#define SIGNAL_TRACING

#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include "..\..\..\..\avr\common\common.h"

#define ADDRESS_MASK 0x1F

#define RESET_TRUE 0

#define ADDR_PORT C
#define DATA_PORT A

// System and bus control signals
#define CTRL_PORT  D
#define CTRL_CE    PD2
#define CTRL_BOOT  PD3
#define CTRL_WE    PD4
#define CTRL_IACK  PD5   // Active HIGH!
#define CTRL_DTACK PD6   // Wait release
#define CTRL_RST   PD7

#define CTRL2_PORT B
#define CTRL2_INT   PB0

#define SPI_PORT  B
#define SPI_SS    PB4
#define SPI_MOSI  PB5
#define SPI_MISO  PB6
#define SPI_SCK   PB7


/*
#define DBG_PORT B
#define DBG_BIT0 PB0
#define DBG_BIT1 PB1
*/

// UART signals
#define UART_PORT D
#define UART_RXD  PD0
#define UART_TXD  PD1

#define PORT_TTY     0
#define PORT_BOOT    1
#define PORT_TESTREG 2
#define PORT_INTREG  3
#define PORT_UCR     4
#define PORT_SPIDATA 5
#define PORT_SPISS   6
#define PORT_SPISR   7


#include <stdio.h>
#include <avr/io.h>
#include <avr/interrupt.h>

// ********************************************************************************
// Macros and Defines
// ********************************************************************************
#define BAUD 14400
#define MYUBRR F_CPU/16/BAUD-1


// ********************************************************************************
// Function Prototypes
// ********************************************************************************
void usart_init(uint16_t ubrr);
char usart_getchar( void );
void usart_putchar( char data );
void usart_pstr (const char *s);
unsigned char usart_kbhit(void);

volatile uint8_t testRegister = 0xB;
volatile uint8_t intRegister  = 0;

// UCR - UART Control Register 
#define UCR_RX_INTERRUPT_ENABLED bit(0)
#define UCR_TX_INTERRUPT_ENABLED bit(1)

#define UART_RXC_INT 64
#define UART_TXC_INT 65

volatile uint8_t reg_ucr = 0;

void trace_signal(const char* message)
{
	#ifdef SIGNAL_TRACING
	usart_pstr(message);
	#endif
}


void RaiseInterrupt(uint8_t intNumber)
{
	intRegister = intNumber;
	
	// Assert INT#
	unset(CTRL2_PORT, CTRL2_INT);
}


char SPI_Transmit(char cData){

	SPDR = cData;
	
	while(!(SPSR & bit(SPIF)));
	
	return SPDR;
}


int main( void )
{
	// Powerup system reset
	setv(CTRL_PORT, CTRL_RST, RESET_TRUE);    // enable RST#
	PORT_OUT(CTRL_PORT, CTRL_RST);		
	
	// Address bus:
	DDR(ADDR_PORT) = 0x0;
	PORT(ADDR_PORT) = ADDRESS_MASK; // Enable pullup

	// Default - input
	DDR(DATA_PORT)  = 0x0;
	PORT(DATA_PORT) = 0x0;
	
	// SPI
	PORT_OUT(SPI_PORT, SPI_SS);
	PORT_OUT(SPI_PORT, SPI_MOSI);
	PORT_OUT(SPI_PORT, SPI_SCK);
	
	set(SPI_PORT, SPI_SS); // SPI SS# is high by default
	
	SPCR = bit(SPE) | bit (MSTR) | bit(SPR0) | bit(SPR1) | bit(CPOL) | bit(CPHA); // SPI enable, Master, f/128

	
	// Control
	PORT_IN_PULLUP(CTRL_PORT, CTRL_CE);
	PORT_IN_PULLUP(CTRL_PORT, CTRL_WE);
	PORT_IN_PULLUP(CTRL_PORT, CTRL_IACK);
	
	PORT_OUT(CTRL_PORT, CTRL_DTACK);
	PORT_OUT(CTRL_PORT, CTRL_BOOT);
	
	PORT_OUT(CTRL2_PORT, CTRL2_INT);
	
	setv(CTRL_PORT , CTRL_DTACK, 1);  // init  DTACK#
	setv(CTRL_PORT , CTRL_BOOT , 0);  // init  BOOT#
	setv(CTRL2_PORT, CTRL2_INT , 1);  // init  INT#
	
	usart_init(MYUBRR);	
	
	usart_pstr("\n\rRESET\n\r");
	_delay_ms(200);
	setv(CTRL_PORT, CTRL_RST, !RESET_TRUE);   // disable RST#
	
	// Enable INT0
	GICR = 1<<INT0;	  // Enable INT0
	
	MCUCR &= ~(bit(ISC01) | bit(ISC00)); // Trigger INT0 on low level
	//MCUCR |= bit(ISC01); // Trigger INT0 on falling edge	
	
	sei();
	
	while (1)
	{

	}	

	return 0;
}

char itoab[4];

//Interrupt Service Routine for INT0
ISR(INT0_vect)
{
	uint8_t ioAddress;

	ioAddress = PIN(ADDR_PORT) & ADDRESS_MASK; // Input only five bits.

#ifdef SIGNAL_TRACING
	trace_signal("CS#\n");
	usart_pstr("A:");
	itoa(ioAddress, itoab, 16);
	usart_pstr(itoab);
	usart_putchar('\n');
#endif	
	
	// it is read
	if(getv(CTRL_PORT, CTRL_WE) == 1)
	{
		trace_signal("RE#\n");
		uint8_t outputData = 0;
		
		if(getv(CTRL_PORT, CTRL_IACK))
		{
			set(CTRL2_PORT, CTRL2_INT); // Deassert INT#
			outputData = intRegister;
		}
		else
		{
			switch(ioAddress)
			{
			case PORT_TTY:
				outputData = usart_getchar();
				break;
			case PORT_BOOT:
				outputData = getv(CTRL_PORT, CTRL_BOOT);
				break;
			case PORT_TESTREG:
				outputData = testRegister;
				break;
			case PORT_UCR:
				outputData = reg_ucr;
				break;
			case PORT_SPIDATA:
				outputData = SPDR;
				break;				
			case PORT_SPISR:
			{
				const uint8_t spr_mask = bit(SPR0) | bit(SPR1);
				outputData = (SPCR & spr_mask);
				break;
			}				
			default:				
					usart_pstr("E!"); 
					itoa(ioAddress, itoab, 10);
					usart_pstr(itoab);				
					usart_putchar('\n');
			}
		}
				
		// Output data
		DDR(DATA_PORT) = 0xFF;
		PORT(DATA_PORT) = outputData;
		
		#ifdef SIGNAL_TRACING
			usart_pstr("D:");
			itoa(outputData, itoab, 16);
			usart_pstr(itoab);
			usart_putchar('\n');
		#endif		
			
		// Assert DTACK#
		trace_signal("DTACK#\n");		
		setv(CTRL_PORT, CTRL_DTACK, 0);
		
		// Wait while CE is low.
		while(getv(CTRL_PORT, CTRL_CE) == 0);
			
		// Tristate data port
		DDR(DATA_PORT)  = 0x0;
		PORT(DATA_PORT) = 0x0;
			
		// Deassert DTACK#
		setv(CTRL_PORT, CTRL_DTACK, 1);
		trace_signal("!DTACK#\n");
	}
	else
	{
		trace_signal("WE#\n");
		uint8_t inputData = PIN(DATA_PORT);
		
		#ifdef SIGNAL_TRACING
			usart_pstr("D:");
			itoa(inputData, itoab, 16);
			usart_pstr(itoab);
			usart_putchar('\n');
		#endif		

		switch(ioAddress)
		{
		case PORT_TTY:
			usart_putchar(inputData);
			break;
		case PORT_BOOT:
			setv(CTRL_PORT, CTRL_BOOT, inputData);
			break;
		case PORT_TESTREG:
			testRegister = inputData;
			break;
		case PORT_INTREG:
			RaiseInterrupt(inputData);
			break;
		case PORT_UCR:
			reg_ucr = inputData;
			break;
		case PORT_SPISS:
			setv(SPI_PORT, SPI_SS, inputData);
			break;
		case PORT_SPIDATA:
			SPI_Transmit(inputData);
			break;
		case PORT_SPISR:
			{
				const uint8_t spr_mask = bit(SPR0) | bit(SPR1);
				SPCR = (SPCR & (~spr_mask)) | (inputData & spr_mask);
				break;			
			}

		default:
			usart_pstr("E!"); 
			itoa(ioAddress, itoab, 10);
			usart_pstr(itoab);
			usart_putchar('\n');
			break;
		}

		// Release WAIT trigger
		trace_signal("DTACK#\n");		
		setv(CTRL_PORT, CTRL_DTACK, 0);
		while(getv(CTRL_PORT, CTRL_CE) == 0);
		setv(CTRL_PORT, CTRL_DTACK, 1);
		trace_signal("!DTACK#\n");
	}
	trace_signal("---\n");
}

// ********************************************************************************
// usart Related
// ********************************************************************************
void usart_init( uint16_t ubrr) {
	// Set baud rate
	UBRRH = (uint8_t)(ubrr>>8);
	UBRRL = (uint8_t)ubrr;
	
	// Enable receiver and transmitter
	UCSRB = bit(TXEN) | bit(RXEN);
	
	// Set frame format: 8data, 1stop bit
	UCSRC = bit(URSEL) | bit(UCSZ1) | bit(UCSZ0) ;
	
	// Enable UART Interrupts
    UCSRB |= bit(RXCIE); // | bit(TXCIE); // Enable the USART Recieve Complete interrupt (USART_RXC)
}

void usart_putchar(char data) {
	// Wait for empty transmit buffer
	while ( !(UCSRA & (_BV(UDRE))) );
		
	// Start transmission
	UDR = data;
}
char usart_getchar(void) {
	// Wait for incoming data
	while ( !(UCSRA & (_BV(RXC))) );
	
	// Return the data
	return UDR;
}
unsigned char usart_kbhit(void) {
	//return nonzero if char waiting polled version
	unsigned char b;
	b=0;
	if(UCSRA & (1<<RXC)) b=1;
	return b;
}

void usart_pstr(const char *s) {
	// loop through entire string
	while (*s) {
		usart_putchar(*s);
		s++;
	}
}

ISR(USART_RXC_vect)
{
	if(reg_ucr & UCR_RX_INTERRUPT_ENABLED)
	{
		RaiseInterrupt(UART_RXC_INT);
	}
}

ISR(USART_TXC_vect)
{
	if(reg_ucr & UCR_TX_INTERRUPT_ENABLED)
	{
		RaiseInterrupt(UART_TXC_INT);
	}
}
